<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CMSFeatureList extends Model
{
    //

    protected $table = "cms_feature_lists";

    protected $guarded = [];
}
