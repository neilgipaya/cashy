<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class Members extends Model
{
    //

    protected $table = "members";

    protected $guarded = [];

    public function user(){
        return $this->hasOne('App\User', 'user_id');
    }

    public static function getMembers($status, $scope = array(), $id = null){

        $query = DB::table('users as u')
            ->select('u.id as user_id', 'u.profile','u.fullname', 'u.firstname', 'u.middlename', 'u.lastname', 'u.email',
            'm.id as member_id', 'm.user_id as member_user_id', 'm.activation_id as member_activation_id', 'm.gender',
                'm.contact_no', 'm.referral_code', 'm.parent_member', 'm.status', 'm.about_me', 'u.username',
                'ac.id as activation_id', 'ac.activation_code', 'ac.status as activation_status', 'ac.assigned_to')
            ->join('members as m', 'm..user_id', '=', 'u.id')
            ->leftJoin('activation_codes as ac', 'ac.id', '=', 'm.activation_id')
            ->where('m.status', $status)
            ->when(
                in_array("Referral", $scope), function ($query){
                    $query->where('m.parent_member', User::getUserMember()->member_id);
                }
            )
            ->get();

        return $query;
    }


}
