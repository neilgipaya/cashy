<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class ActivationCode extends Model
{
    //

    protected $table = "activation_codes";

    protected $guarded = [];

    /**
     * @return \Illuminate\Support\Collection
     */
    public static function getActivationList(){

        return DB::table('users as u')
            ->select('ac.id as activation_id', 'ac.activation_code', 'ac.assigned_to', 'u.id as user_id', 'u.fullname',
                'ac.status')
            ->join('activation_codes as ac', 'ac.assigned_to', '=', 'u.id')
            ->groupBy('ac.activation_code')
            ->get();

    }

    public static function getAssignedActivation($member_id){


        return DB::table('activation_codes')
            ->where('assigned_to', $member_id)
            ->where('status', 0)
            ->get();

    }
}
