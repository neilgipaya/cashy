<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use App\Traits\Scope;

class Referral extends Model
{
    //

    public static function getDirectReferrals(){

        return Members::getMembers('Active', [ 'Referral' ], Auth::id());

    }

}
