<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CMSAboutList extends Model
{
    //

    protected $table = "cms_about_lists";

    protected $guarded = [];

}
