<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CashySolving extends Model
{
    //

    protected $table = "user_solvings";

    protected $guarded = [];
}
