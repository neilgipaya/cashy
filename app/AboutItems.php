<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AboutItems extends Model
{
    //

    protected $table = "about_items";
    protected  $guarded = [];
}
