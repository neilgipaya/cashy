<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CMSFeatures extends Model
{
    //

    protected $table = "cms_features";

    protected $guarded = [];
}
