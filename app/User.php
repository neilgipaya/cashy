<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Spatie\Permission\Traits\HasRoles;

class User extends Authenticatable
{
    use Notifiable;
    use HasRoles;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'username', 'email', 'password','firstname', 'middlename', 'lastname', 'suffix', 'activation_status', 'fullname',
        'profile'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function setPasswordAttribute($password)
    {
        $this->attributes['password'] = bcrypt($password);
    }

    public static function getUserMember($id = null){

        $query = DB::table('users as u')
            ->select('u.id as user_id', 'u.profile', 'u.fullname', 'u.firstname', 'u.middlename', 'u.lastname', 'u.email',
                'm.id as member_id', 'm.user_id as member_user_id', 'm.activation_id as member_activation_id', 'm.gender',
                'm.contact_no', 'm.referral_code', 'm.parent_member', 'm.status',  'ac.id as activation_id', 'ac.activation_code', 'm.about_me',
                'ac.status as activation_status', 'ac.assigned_to')
            ->join('members as m', 'm..user_id', '=', 'u.id')
            ->leftJoin('activation_codes as ac', 'ac.id', '=', 'm.activation_id')
            ->where('user_id', '=',empty($id) ? Auth::id() : $id)
            ->where('m.status', 'Active')
            ->first();

        return $query;
    }

}
