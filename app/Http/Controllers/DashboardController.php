<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class DashboardController extends Controller
{
    //

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(){


        $user = Auth::user();

        if ($user->can('admin-dashboard')){
            return view('Dashboard.index');
        }

        return redirect('/dashboard/member');

    }

    public function memberDashboard(){


        if (Auth::user()->activation_status == 'Activated'){
            return view('Dashboard.index-member');
        }

        return view('Dashboard.index-member-inactive');


    }
}
