<?php

namespace App\Http\Controllers;

use App\LoadTransactions;
use Illuminate\Http\Request;
use Yajra\DataTables\DataTables;


class LoadTransactionsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        return view('Eloading.console', [
            'data'=>LoadTransactions::all()->sortByDesc('id')
        ]);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\LoadTransactions  $loadTransactions
     * @return \Illuminate\Http\Response
     */
    public function show(LoadTransactions $loadTransactions)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\LoadTransactions  $loadTransactions
     * @return \Illuminate\Http\Response
     */
    public function edit(LoadTransactions $loadTransactions)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\LoadTransactions  $loadTransactions
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, LoadTransactions $loadTransactions)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\LoadTransactions  $loadTransactions
     * @return \Illuminate\Http\Response
     */
    public function destroy(LoadTransactions $loadTransactions)
    {
        //
    }

    public function transactions(Request $request){

        if ($request->ajax()){

            $transactions = LoadTransactions::all()->sortByDesc('id');
            return DataTables::of($transactions)
                ->addIndexColumn()
                ->make(true);
        }
    }
}
