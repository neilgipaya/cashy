<?php

namespace App\Http\Controllers;

use GuzzleHttp\Client;
use Illuminate\Http\Request;


class APIController extends Controller
{

    /*
     * var client
     */
    private $client;


    public function __construct()
    {
        $this->client = new Client([
            'headers'=>[
                'Content-Type' => 'application/json'
            ]
        ]);
    }

    public function requestLoad($mobile_number, $promo){
        $url = "https://devapi.globelabs.com.ph/rewards/v1/transactions/send";

        try {

            $request = $this->client->post(
                $url,
                [
                    'verify' => false,
                    'json' => [
                        'outboundRewardRequest' => [
                            'app_id' => env('GLOBE_APP_ID', null),
                            'app_secret' => env('GLOBE_APP_SECRET', null),
                            'rewards_token' => env('GLOBE_REWARDS_TOKEN', null),
                            'address' => $mobile_number,
                            'promo' => $promo
                        ]
                    ]
                ]
            );

            return $request->getStatusCode();

        } catch (\Exception $e){
            return $e->getCode();
        }

    }
}
