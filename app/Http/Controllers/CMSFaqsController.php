<?php

namespace App\Http\Controllers;

use App\CMSFaq;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class CMSFaqsController extends Controller
{
    //

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //

        try{

            CMSFaq::create($request->all());

            $notification = array(
                'message' => 'Content Created Successfully',
                'alert-type' => 'success'
            );


            return redirect()->back()->with($notification);
        }
        catch (\Exception $e){

            Log::channel('system_error')->critical("Error: " . $e->getMessage() . " Line: " . $e->getLine() . " File: " . $e->getFile());
            $notification = array(
                'message' => 'Fatal Error, Please Contact your Administrator',
                'alert-type' => 'error'
            );


            return redirect()->back()->with($notification);

        }
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\AboutItems  $aboutItems
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //

        try{

            CMSFaq::find($id)->update($request->all());

            $notification = array(
                'message' => 'Content Updated Successfully',
                'alert-type' => 'info'
            );


            return redirect()->back()->with($notification);
        }
        catch (\Exception $e){

            Log::channel('system_error')->critical("Error: " . $e->getMessage() . " Line: " . $e->getLine() . " File: " . $e->getFile());
            $notification = array(
                'message' => 'Fatal Error, Please Contact your Administrator',
                'alert-type' => 'error'
            );


            return redirect()->back()->with($notification);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\AboutItems  $aboutItems
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //

        try{

            CMSFaq::find($id)->delte();

            $notification = array(
                'message' => 'Content Deleted Successfully',
                'alert-type' => 'warning'
            );


            return redirect()->back()->with($notification);
        }
        catch (\Exception $e){

            Log::channel('system_error')->critical("Error: " . $e->getMessage() . " Line: " . $e->getLine() . " File: " . $e->getFile());
            $notification = array(
                'message' => 'Fatal Error, Please Contact your Administrator',
                'alert-type' => 'error'
            );


            return redirect()->back()->with($notification);
        }
    }
}
