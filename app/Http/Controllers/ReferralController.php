<?php

namespace App\Http\Controllers;

use App\ActivationCode;
use App\Referral;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ReferralController extends Controller
{
    /**
     * @var FormatUtil
     */
    private $formatUtil;

    /**
     * ReferralController constructor.
     * @param FormatUtil $
     */
    public function __construct(FormatUtil $formatUtil)
    {
        $this->formatUtil = $formatUtil;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //

        $data['breadcrumbs'] = $this->formatUtil->formatBreadCrumbs('Cashy CMS', 'CMS', '#', '/homeCMS', 'Home Header');
        $data['referral_lists'] = Referral::getDirectReferrals();
        $data['referral_code'] = User::getUserMember()->referral_code;
        $data['activation_codes'] = ActivationCode::all()->where('assigned_to', User::getUserMember()->member_id);

        return view('Members.members-referral', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Referral  $referral
     * @return \Illuminate\Http\Response
     */
    public function show(Referral $referral)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Referral  $referral
     * @return \Illuminate\Http\Response
     */
    public function edit(Referral $referral)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Referral  $referral
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Referral $referral)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Referral  $referral
     * @return \Illuminate\Http\Response
     */
    public function destroy(Referral $referral)
    {
        //
    }
}
