<?php

namespace App\Http\Controllers;

use App\ActivationCode;
use App\CMSFeatures;
use App\Members;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;

class MembersController extends Controller
{
    /**
     * @var FormatUtil
     */
    private $formatUtil;

    /**
     * MembersController constructor.
     * @param FormatUtil $formatUtil
     */
    public function __construct(FormatUtil $formatUtil)
    {
        $this->formatUtil = $formatUtil;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //

        $data['breadcrumbs'] = $this->formatUtil->formatBreadCrumbs('Cashy CMS', 'CMS', '#', '/aboutCMS', 'About Cashy');

        $data['members'] = Members::getMembers('Active');

        $data['activation_codes'] = ActivationCode::getActivationList();

        return view('Members.members', $data);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $data['breadcrumbs'] = $this->formatUtil->formatBreadCrumbs('Cashy CMS', 'CMS', '#', '/aboutCMS', 'About Cashy');

        return view('Members.members-create', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //

        try{

            $request->validate([
               'email' =>'unique:users',
               'username'=>'unique:users',
               'referral_code'=>'unique:members'
            ]);

            $request->merge([
                'referral_code' => $this->formatUtil->formatReferralCode($request->post('firstname'), $request->post('lastname'),
                    $request->post('middlename'), $request->post('username'))
            ]);

            if (!empty($request->post('activation_code'))){
                $check_activation = $this->formatUtil->checkActivationCode($request->post('activation_code'));

                switch ($check_activation){

                    case "Success":
                        //register the member with activation code
                        $request->merge([
                           'activation_id'=> $this->formatUtil->getActivationCode($request->post('activation_code')),
                            'parent_member' => $this->formatUtil->checkParentMember($request->post('parent_member'))
                        ]);

                        $user = User::create($request->only('username', 'password', 'firstname', 'middlename', 'lastname', 'suffix', 'email'));

                        $request->merge([
                            'user_id'=> $user->id
                        ]);
                        Members::create($request->only('user_id', 'profile', 'address', 'gender', 'contact_no', 'referral_code', 'activation_id'));

                        //assign role

                        $user->assignRole('Member');

                        $this->formatUtil->updateActivationCode($request->post('activation_code'));

                        $notification = array(
                            'message' => 'Member Registered Successfully',
                            'alert-type' => 'success'
                        );


                        return redirect()->back()->with($notification);
                        break;

                    case "Expired":
                        $notification = array(
                            'message' => 'Activation Code Already Taken',
                            'alert-type' => 'error'
                        );


                        return redirect()->back()->with($notification)->withInput(Input::all());
                        break;

                    case "Invalid":
                        $notification = array(
                            'message' => 'Activation Code is Invalid',
                            'alert-type' => 'error'
                        );

                        return redirect()->back()->with($notification)->withInput(Input::all());
                        break;
                    default:
                        $notification = array(
                            'message' => 'Fatal Error, Please Contact your Administrator',
                            'alert-type' => 'error'
                        );


                        return redirect()->back()->with($notification);
                }
            } else {
                //empty activation code


                $request->merge([
                    'activation_status' => 'Inactive'
                ]);

                $user = User::create($request->only('username', 'password', 'firstname', 'middlename', 'lastname', 'suffix', 'email', 'activation_status'));

                $request->merge([
                    'user_id'=> $user->id,
                    'parent_member' => $this->formatUtil->checkParentMember($request->post('parent_member')),


                ]);
                Members::create($request->only('user_id', 'address', 'gender', 'contact_no', 'referral_code'));
                $user->assignRole('Member');

                $notification = array(
                    'message' => 'Member Registered Successfully',
                    'alert-type' => 'success'
                );


                return redirect()->back()->with($notification);

            }



        }
        catch (\Exception $e){
            Log::channel('system_error')->critical("Error: " . $e->getMessage() . " Line: " . $e->getLine() . " File: " . $e->getFile());
            $notification = array(
                'message' => 'Fatal Error, Please Contact your Administrator',
                'alert-type' => 'error'
            );


            return redirect()->back()->with($notification);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param $id
     * @return void
     */
    public function show($id)
    {
        //

        $data['info'] = User::getUserMember($id);


        return view('Personal.profile', $data);


    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Members  $members
     * @return \Illuminate\Http\Response
     */
    public function edit(Members $members)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param $id
     * @return void
     */
    public function update(Request $request, $id)
    {
        //

//        try{

            if ($request->has('update-profile')){

                User::find($id)->update($request->only('fullname', 'email'));
                Members::find($id)->update($request->only('gender', 'contact_no'));

                $notification = array(
                    'message' => 'Profile Updated Successfully',
                    'alert-type' => 'info'
                );
            } else {

                Members::find($id)->update($request->except('files'));

                $notification = array(
                    'message' => 'Member Updated Successfully',
                    'alert-type' => 'info'
                );
            }

            return redirect()->back()->with($notification);
//        }
//        catch (\Exception $e){
//
//            Log::channel('system_error')->critical("Error: " . $e->getMessage() . " Line: " . $e->getLine() . " File: " . $e->getFile());
//        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Members  $members
     * @return \Illuminate\Http\Response
     */
    public function destroy(Members $members)
    {
        //
    }

    public function activate(Request $request)
    {
        try{


            if (!empty($request->post('activation_code'))){
                $check_activation = $this->formatUtil->checkActivationCode($request->post('activation_code'));

                switch ($check_activation){

                    case "Success":
                        //register the member with activation code
                        $request->merge([
                            'activation_id'=> $this->formatUtil->getActivationCode($request->post('activation_code')),
                            'parent_member' => $this->formatUtil->checkParentMember($request->post('parent_member'))
                        ]);

                        User::find(Auth::user()->id)->update([
                            'activation_status' => 'Activated'
                        ]);

                        $this->formatUtil->updateActivationCode($request->post('activation_code'));

                        $notification = array(
                            'message' => 'Member Registered Successfully',
                            'alert-type' => 'success'
                        );


                        return redirect()->back()->with($notification);
                        break;

                    case "Expired":
                        $notification = array(
                            'message' => 'Activation Code Already Taken',
                            'alert-type' => 'error'
                        );


                        return redirect()->back()->with($notification)->withInput(Input::all());
                        break;

                    case "Invalid":
                        $notification = array(
                            'message' => 'Activation Code is Invalid',
                            'alert-type' => 'error'
                        );


                        return redirect()->back()->with($notification)->withInput(Input::all());
                        break;
                    default:
                        $notification = array(
                            'message' => 'Fatal Error, Please Contact your Administrator',
                            'alert-type' => 'error'
                        );


                        return redirect()->back()->with($notification);
                }
            }
        }
        catch (\Exception $e){
            Log::channel('system_error')->critical("Error: " . $e->getMessage() . " Line: " . $e->getLine() . " File: " . $e->getFile());
            $notification = array(
                'message' => 'Fatal Error, Please Contact your Administrator',
                'alert-type' => 'error'
            );


            return redirect()->back()->with($notification);
        }
    }

    public function changeProfilePicture(Request $request, $id){


        $filename = time().$request->file('picture')->getClientOriginalName();

        if ($request->hasFile('picture')) {


            Storage::disk('public_uploads')->putFileAs(
                'profile/',
                $request->file('picture'),
                $filename
            );
        }

        User::find($id)->update([
           'profile' => $filename
        ]);


        $notification = array(
            'message' => 'Profile Updated Successfully',
            'alert-type' => 'info'
        );


        return redirect()->back()->with($notification);


    }

    public function activateMember(Request $request, $id)
    {

        try{


            DB::table('members')
                ->where('user_id', $id)
                ->update([
                    'activation_id'=> $this->formatUtil->getActivationCode($request->post('activation_code'))
                ]);

            DB::table('users')
                ->where('id', $id)
                ->update([
                    'activation_status' => 'Activated'
                ]);


            $this->formatUtil->updateActivationCode($request->post('activation_code'));

            $notification = array(
                'message' => 'Member Activated Successfully',
                'alert-type' => 'success'
            );


            return redirect()->back()->with($notification);
        }
        catch (\Exception $exception){
            Log::channel('system_error')->critical("Error: " . $e->getMessage() . " Line: " . $e->getLine() . " File: " . $e->getFile());
            $notification = array(
                'message' => 'Fatal Error, Please Contact your Administrator',
                'alert-type' => 'error'
            );


            return redirect()->back()->with($notification);
        }

    }

}
