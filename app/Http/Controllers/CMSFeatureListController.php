<?php

namespace App\Http\Controllers;

use App\AboutItems;
use App\CMSFeatureList;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class CMSFeatureListController extends Controller
{

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //

        try{

            CMSFeatureList::create($request->all());

            $notification = array(
                'message' => 'Content Created Successfully',
                'alert-type' => 'success'
            );


            return redirect()->back()->with($notification);
        }
        catch (\Exception $e){

            Log::channel('system_error')->critical("Error: " . $e->getMessage() . " Line: " . $e->getLine() . " File: " . $e->getFile());
            $notification = array(
                'message' => 'Fatal Error, Please Contact your Administrator',
                'alert-type' => 'error'
            );


            return redirect()->back()->with($notification);

        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\AboutItems  $aboutItems
     * @return \Illuminate\Http\Response
     */
    public function show(AboutItems $aboutItems)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\AboutItems  $aboutItems
     * @return \Illuminate\Http\Response
     */
    public function edit(AboutItems $aboutItems)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\AboutItems  $aboutItems
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //

        try{

            CMSFeatureList::find($id)->update($request->all());

            $notification = array(
                'message' => 'Content Updated Successfully',
                'alert-type' => 'info'
            );


            return redirect()->back()->with($notification);
        }
        catch (\Exception $e){

            Log::channel('system_error')->critical("Error: " . $e->getMessage() . " Line: " . $e->getLine() . " File: " . $e->getFile());
            $notification = array(
                'message' => 'Fatal Error, Please Contact your Administrator',
                'alert-type' => 'error'
            );


            return redirect()->back()->with($notification);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\AboutItems  $aboutItems
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //

        try{

            CMSFeatureList::find($id)->delte();

            $notification = array(
                'message' => 'Content Deleted Successfully',
                'alert-type' => 'warning'
            );


            return redirect()->back()->with($notification);
        }
        catch (\Exception $e){

            Log::channel('system_error')->critical("Error: " . $e->getMessage() . " Line: " . $e->getLine() . " File: " . $e->getFile());
            $notification = array(
                'message' => 'Fatal Error, Please Contact your Administrator',
                'alert-type' => 'error'
            );


            return redirect()->back()->with($notification);
        }
    }
}
