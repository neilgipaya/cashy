<?php

namespace App\Http\Controllers;

use App\AboutItems;
use App\CMSAboutList;
use App\CMSFaq;
use App\CMSFeatureList;
use App\CMSFeatures;
use App\CMSHome;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Monolog\Logger;

class CMSController extends Controller
{
    //
    /**
     * @var FormatUtil
     */
    private $formatUtil;

    /**
     * CMSController constructor.
     * @param FormatUtil $formatUtil
     */
    public function __construct(FormatUtil $formatUtil)
    {
        $this->formatUtil = $formatUtil;
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function homeCMS(){


        $data['breadcrumbs'] = $this->formatUtil->formatBreadCrumbs('Cashy CMS', 'CMS', '#', '/homeCMS', 'Home Header');

        $data['cms'] = CMSHome::findOrFail(1);

        return view('CMS.cms-home', $data);

    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function homeCMSPost(Request $request){

        try{

            CMSHome::find(1)->update($request->except('files'));

            $notification = array(
                'message' => 'Content Updated Successfully',
                'alert-type' => 'info'
            );

            return redirect()->back()->with($notification);
        }
        catch (\Exception $e){

            Log::channel('system_error')->critical("Error: " . $e->getMessage() . " Line: " . $e->getLine() . " File: " . $e->getFile());
            $notification = array(
                'message' => 'Fatal Error, Please Contact your Administrator',
                'alert-type' => 'error'
            );


            return redirect()->back()->with($notification);
        }
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function aboutCMS(){


        $data['breadcrumbs'] = $this->formatUtil->formatBreadCrumbs('Cashy CMS', 'CMS', '#', '/aboutCMS', 'About Cashy');

        $data['cms'] = CMSAboutList::findOrFail(1);
        $data['about_items'] = AboutItems::all();

        return view('CMS.cms-about', $data);

    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function aboutCMSPost(Request $request){

        try{

            CMSAboutList::find(1)->update($request->except('files'));

            $notification = array(
                'message' => 'Content Updated Successfully',
                'alert-type' => 'info'
            );


            return redirect()->back()->with($notification);
        }
        catch (\Exception $e){

            Log::channel('system_error')->critical("Error: " . $e->getMessage() . " Line: " . $e->getLine() . " File: " . $e->getFile());
            $notification = array(
                'message' => 'Fatal Error, Please Contact your Administrator',
                'alert-type' => 'error'
            );


            return redirect()->back()->with($notification);
        }
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function featuresCMS(){


        $data['breadcrumbs'] = $this->formatUtil->formatBreadCrumbs('Cashy CMS', 'CMS', '#', '/aboutCMS', 'About Cashy');

        $data['cms'] = CMSFeatures::findOrFail(1);
        $data['feature_lists'] = CMSFeatureList::all();

        return view('CMS.cms-feature', $data);

    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function featuresCMSPost(Request $request){

        try{

            CMSFeatures::find(1)->update($request->except('files'));

            $notification = array(
                'message' => 'Content Updated Successfully',
                'alert-type' => 'info'
            );


            return redirect()->back()->with($notification);
        }
        catch (\Exception $e){

            Log::channel('system_error')->critical("Error: " . $e->getMessage() . " Line: " . $e->getLine() . " File: " . $e->getFile());
            $notification = array(
                'message' => 'Fatal Error, Please Contact your Administrator',
                'alert-type' => 'error'
            );


            return redirect()->back()->with($notification);
        }
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function faqsCMS(){


        $data['breadcrumbs'] = $this->formatUtil->formatBreadCrumbs('Cashy CMS', 'CMS', '#', '/aboutCMS', 'About Cashy');

        $data['faq_lists'] = CMSFaq::all();

        return view('CMS.cms-faqs', $data);

    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function faqsCMSPost(Request $request){

        try{

            CMSFeatures::find(1)->update($request->except('files'));

            $notification = array(
                'message' => 'Content Updated Successfully',
                'alert-type' => 'info'
            );


            return redirect()->back()->with($notification);
        }
        catch (\Exception $e){

            Log::channel('system_error')->critical("Error: " . $e->getMessage() . " Line: " . $e->getLine() . " File: " . $e->getFile());
            $notification = array(
                'message' => 'Fatal Error, Please Contact your Administrator',
                'alert-type' => 'error'
            );


            return redirect()->back()->with($notification);
        }
    }
}
