<?php

namespace App\Http\Controllers;

use App\Members;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Session;

class AuthenticationController extends Controller
{


    /**
     * @var FormatUtil
     */
    private $formatUtil;

    /**
     * AuthenticationController constructor.
     * @param FormatUtil $formatUtil
     */
    public function __construct(FormatUtil $formatUtil)
    {
        $this->middleware('guest');
        $this->formatUtil = $formatUtil;
    }


    public function showLogin(){

        return view('login');

    }

    public function login(Request $request){

        $credentials = $request->only('username', 'password');
        $remember = $request->post('remember');

        if(Auth::attempt($credentials, $remember)){

            $notification = array(
                'success' => true,
                'message' => 'Login Success, Redirecting to Dashboard',
                'type' => 'success'
            );

            return response()->json($notification);
        }

        $notification = array(
            'success' => false,
            'message' => 'Invalid Username / Password',
            'type' => 'error'
        );

    return response()->json($notification);

    }

    public function showRegister(){

        $data['referral_code'] = \request()->get('r');

        return view('register', $data);
    }

    public function registerMember(Request $request){

//        DB::beginTransaction();

        try{


            $request->validate([
                'email' =>'unique:users',
                'username'=>'unique:users',
                'referral_code'=>'unique:members'
            ]);

            $request->merge([
            'referral_code' => $this->formatUtil->formatReferralCode(
                $request->post('username'),
                $request->post('fullname'),
                $request->post('email'),
                $request->post('username')
            )
        ]);

            if (!empty($request->post('activation_code'))){
                $check_activation = $this->formatUtil->checkActivationCode($request->post('activation_code'));

                switch ($check_activation){

                    case "Success":
                        //register the member with activation code
                        $request->merge([
                            'activation_id'=> $this->formatUtil->getActivationCode($request->post('activation_code')),
                            'parent_member' => $this->formatUtil->checkParentMember($request->post('parent_member'))
                        ]);



                        $user = User::create($request->only('username', 'password', 'fullname','email'));

                        $request->merge([
                            'user_id'=> $user->id
                        ]);
                        Members::create($request->only('user_id', 'referral_code', 'activation_id', 'parent_member'));

                        $this->formatUtil->updateActivationCode($request->post('activation_code'));

                        $user->assignRole('Member');



                        Auth::loginUsingId($user->id);

                        if (Auth::check()){
                            return redirect('dashboard');
                        }

//                        $notification = array(
//                            'message' => 'Registered Successfully',
//                            'alert-type' => 'success'
//                        );
//
//
//                        return redirect()->back()->with($notification);
                        break;

                    case "Expired":
                        $notification = array(
                            'message' => 'Activation Code Already Taken',
                            'alert-type' => 'error'
                        );


                        return redirect()->back()->with($notification)->withInput(Input::all());
                        break;

                    case "Invalid":
                        $notification = array(
                            'message' => 'Activation Code is Invalid',
                            'alert-type' => 'error'
                        );


                        return redirect()->back()->with($notification)->withInput(Input::all());
                        break;
                    default:
                        $notification = array(
                            'message' => 'Fatal Error, Please Contact your Administrator',
                            'alert-type' => 'error'
                        );


                        return redirect()->back()->with($notification);
                }
            } else {
                //empty activation code

                $parent = $this->formatUtil->checkParentMember($request->post('parent_member'));

                $request->merge([
                    'activation_status' => 'Inactive'
                ]);

                $user = User::create($request->only('username', 'password', 'fullname', 'email', 'activation_status'));


                $request->merge([
                    'user_id'=> $user->id,
                    'parent_member' => empty($parent) ? '' : $parent[0],


                ]);
                Members::create($request->only('user_id', 'referral_code', 'parent_member'));
                $user->assignRole('Member');


//                DB::commit();

                Auth::loginUsingId($user->id);

                //user is not activated
                if (Auth::check()){
                    Session::put('activated', false);
                    return redirect('dashboard');
                }

//                $notification = array(
//                    'message' => 'Member Registered Successfully',
//                    'alert-type' => 'success'
//                );
//
//
//                return redirect()->back()->with($notification);

            }



        }
        catch (\Exception $e){
//            DB::rollBack();
            Log::channel('system_error')->critical("Error: " . $e->getMessage() . " Line: " . $e->getLine() . " File: " . $e->getFile());
            $notification = array(
                'message' => 'Fatal Error, Please Contact your Administrator',
                'alert-type' => 'error'
            );


            return redirect()->back()->with($notification);
        }
    }
}
