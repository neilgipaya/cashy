<?php

namespace App\Http\Controllers;

use App\CashySolving;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Yajra\DataTables\DataTables;

class CashySolvingController extends Controller
{
    /**
     * @var FormatUtil
     */
    private $formatUtil;


    /**
     * CashySolvingController constructor.
     * @param FormatUtil $formatUtil
     */
    public function __construct(FormatUtil $formatUtil)
    {
        $this->formatUtil = $formatUtil;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //

    }

    public function solveAddition(){


        return view('CashySolving.solve-addition');
    }


    public function solveSubtraction(){


        return view('CashySolving.solve-subtraction');
    }

    public function solveMultiplication(){


        return view('CashySolving.solve-multiplication');
    }

    public function solveDivision(){


        return view('CashySolving.solve-division');
    }


    public function fetchSolvingDetails(Request $request, $operation){

        if ($request->ajax()){

            $data = DB::table('users')
                ->join('user_solvings', 'users.id', '=', 'user_solvings.user_id')
                ->where('users.id', Auth::user()->id)
                ->where('user_solvings.operation', $operation)
                ->whereNull('user_solvings.deleted_at')
                ->get();


            return DataTables::of($data)
                ->addIndexColumn()
                ->make(true);

        }
    }

    public function getAnalytics()
    {

        $data['points'] = DB::table('users')
            ->join('user_solvings', 'users.id', '=', 'user_solvings.user_id')
            ->where('users.id', Auth::user()->id)
            ->whereNull('user_solvings.deleted_at')
            ->whereDate('user_solvings.created_at', Carbon::today())
            ->sum('user_solvings.points');

        $data['correct'] = DB::table('users')
            ->join('user_solvings', 'users.id', '=', 'user_solvings.user_id')
            ->where('users.id', Auth::user()->id)
            ->whereNull('user_solvings.deleted_at')
            ->where('correction', 'correct')
            ->whereDate('user_solvings.created_at', Carbon::today())
            ->count();

        $data['incorrect'] = DB::table('users')
            ->join('user_solvings', 'users.id', '=', 'user_solvings.user_id')
            ->where('users.id', Auth::user()->id)
            ->whereNull('user_solvings.deleted_at')
            ->where('correction', 'incorrect')
            ->whereDate('user_solvings.created_at', Carbon::today())
            ->count();

        $response = array(
            "points" => $data['points'],
            'correct' => $data['correct'],
            'incorrect' => $data['incorrect']
        );

        return response()->json($response);
    }

    public function getTotalSolvingHistory()
    {

        $data['addition'] = CashySolving::all()
            ->where('operation', 'Addition')
            ->where('user_id', Auth::user()->id)
            ->sum('points');

        $data['subtraction'] = CashySolving::all()
            ->where('operation', 'Subtraction')
            ->where('user_id', Auth::user()->id)
            ->sum('points');

        $data['division'] = CashySolving::all()
            ->where('operation', 'Division')
            ->where('user_id', Auth::user()->id)
            ->sum('points');

        $data['multiplication'] = CashySolving::all()
            ->where('operation', 'Multiplication')
            ->where('user_id', Auth::user()->id)
            ->sum('points');

        $data['addition-today'] = CashySolving::all()
            ->where('operation', 'Addition')
            ->where('user_id', Auth::user()->id)
            ->sum('points');

        $data['subtraction-today'] = CashySolving::all()
            ->where('operation', 'Subtraction')
            ->where('user_id', Auth::user()->id)
            ->sum('points');

        $data['division-today'] = CashySolving::all()
            ->where('operation', 'Division')
            ->where('user_id', Auth::user()->id)
            ->sum('points');

        $data['multiplication-today'] = CashySolving::all()
            ->where('operation', 'Multiplication')
            ->where('user_id', Auth::user()->id)
            ->whereDate('created_at')
            ->sum('points');    1111111111111111111111111111111111Z

        $data['correct'] = CashySolving::all()
            ->where('correction', 'correct')
            ->where('user_id', Auth::user()->id)
            ->count();


        $data['incorrect'] = CashySolving::all()
            ->where('correction', 'incorrect')
            ->where('user_id', Auth::user()->id)
            ->count();


        $response = array(
            'correct' => $data['correct'],
            'incorrect' => $data['incorrect'],
            'addition' => $data['addition'],
            'multiplication' => $data['multiplication'],
            'division' => $data['division'],
            'subtraction' => $data['subtraction']
        );

        return response()->json($response);
    }
}
