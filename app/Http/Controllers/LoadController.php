<?php

namespace App\Http\Controllers;

use App\LoadTransactions;
use App\Uid;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class LoadController extends Controller
{
    //
    /**
     * @var APIController
     */
    private $APIController;
    /**
     * @var FormatUtil
     */
    private $formatUtil;

    /**
     * LoadController constructor.
     * @param APIController $APIController
     * @param FormatUtil $formatUtil
     */
    public function __construct(APIController $APIController, FormatUtil $formatUtil)
    {
        $this->APIController = $APIController;
        $this->formatUtil = $formatUtil;
    }

    /**
     *
     */
    public function index(){

    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function rewards(Request $request){

        try{

            $mobile_number = $request->post('mobile_number');
            $promo = $request->post('promo');
            $cashy_token = $request->post('cashy_token');
            $uid = $request->post('uid');

            if ($this->verifyUID($uid)){

                return response()->json(
                    [
                        'status'=>'error',
                        'content'=>'Request has been expired'
                    ],
                    419
                );

            }


            //insert UID to database
            Uid::create([
                'generated_uid'=>$uid
            ]);

            if ($this->verifyMobileNumber($mobile_number)){

                if ($this->verifyToken($cashy_token)){

                    $request_load = $this->APIController->requestLoad($mobile_number, $promo);



                    if ($request_load === 200){
                        LoadTransactions::create(
                            [
                                'transaction_code'=>$this->generateTransactionCode($promo, $mobile_number),
                                'mobile_number'=>$mobile_number,
                                'promo_code'=>$promo,
                                'api_response'=>"Ok 200",
                                'transaction_date_time'=>date('Y-m-d h:i: a')
                            ]
                        );
                        return response()->json(
                            [
                                'status'=>'success',
                                'content'=>"Top-up success"
                            ],
                            $request_load
                        );
                    }

                    LoadTransactions::create(
                        [
                            'transaction_code'=>$this->generateTransactionCode($promo, $mobile_number),
                            'mobile_number'=>$mobile_number,
                            'promo_code'=>$promo,
                            'api_response'=>"$request_load API Problem",
                            'transaction_date_time'=>date('Y-m-d h:i: a')
                        ]
                    );

                    return response()->json(
                        [
                            'status'=>'error',
                            'content'=>"$request_load API Problem"
                        ],
                        $request_load
                    );
                }

                LoadTransactions::create(
                    [
                        'transaction_code'=>$this->generateTransactionCode($promo, $mobile_number),
                        'mobile_number'=>$mobile_number,
                        'promo_code'=>$promo,
                        'api_response'=> "Cashy Rewards Token Not Verified",
                        'transaction_date_time'=>date('Y-m-d h:i: a')
                    ]
                );

                return response()->json(
                    [
                        'status'=>'error',
                        'content'=>'Cashy Rewards Token Not Verified'
                    ],
                    400
                );

            }

            LoadTransactions::create(
                [
                    'transaction_code'=>$this->generateTransactionCode($promo, $mobile_number),
                    'mobile_number'=>$mobile_number,
                    'promo_code'=>$promo,
                    'api_response'=>"Invalid Mobile Number",
                    'transaction_date_time'=>date('Y-m-d h:i: a')
                ]
            );

            return response()->json(
                [
                    'status'=>'error',
                    'content'=>'Invalid Mobile Number'
                ],
                400
            );

        } catch (\Exception $e){

            return response()->json(
                [
                    'status'=> 'error',
                    'content'=> $e->getMessage()
                ],
                500
            );
        }

    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function generateUID(){

        $uid = "CASHY".time();

        return  response()->json(
            [
                "status"=>"success",
                "UID"=>Hash::make($uid)
            ]
        );

    }


    /**
     * @param $token
     * @return bool
     */
    private function verifyToken($token){

        $cashy_token = env('CASHY_REWARDS_TOKEN');

       if ($token === $cashy_token){
           return true;
       }

       return false;

    }

    /**
     * @param $rewards
     * @param $mobile_number
     * @return string
     */
    private function generateTransactionCode($rewards, $mobile_number){

        $transaction_code = time().$rewards.$mobile_number;

        return $transaction_code;

    }

    /**
     * @param $mobile_number
     * @return bool|int|mixed|string|string[]|null
     */
    private function verifyMobileNumber($mobile_number){

        $mobile_number = str_replace(' ', '', $mobile_number);

        if (substr($mobile_number, 0, 1) == 9){
            if (strlen($mobile_number) === 10){
                return $mobile_number;
            }

            return false;
        }


        $mobile_header = substr($mobile_number, 0, 2);

        switch ($mobile_header){

            case "+6":
                $phone_number = str_replace('+639','9', $mobile_number);
                break;
            case "63":
                $phone_number = preg_replace('/^639/','9', $mobile_number);
                break;
            case "09":
                $phone_number = preg_replace('/^09/','9', $mobile_number);
                break;
            case "9":
                $phone_number = str_replace('9','9', $mobile_number);
                break;
            default:
                $phone_number = 0;
        }

        if (strlen($phone_number) === 10){
            return $phone_number;
        }

        return false;

    }


    /**
     * @param $uid
     * @return bool
     */
    private function verifyUID($uid){

        $select = DB::table('uids')
            ->where('generated_uid', $uid)
            ->first();

        if (!empty($select)){
            return true;
        }

        return false;

    }




}
