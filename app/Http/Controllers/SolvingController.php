<?php

namespace App\Http\Controllers;

use App\CashySolving;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;

class SolvingController extends Controller
{

    public function solveNumber(Request $request){

        try {
            $operation = $request->post('operation');
            $user_answer = (float) $request->post('answer');

            $system_answer = (float) Session::get('system_answer') ?? 0;

            if (isset($operation)){

                switch ($operation){

                    case "Addition":

                        if ($system_answer === $user_answer){

                            $result =[
                                'error'=>false,
                                'correct' => true
                            ];


                            $points = $this->getPoints("addition");

                            CashySolving::create([
                                'user_id' => Auth::user()->id,
                                'operation' => $operation,
                                'formula' => Session::get('formula'),
                                'answer' => $user_answer,
                                'points' => $points,
                                'correction' => 'correct'
                            ]);

                            $this->updatePoints($points);

                        } else{

                            $result =[
                                'error'=>true,
                                'correct' => false
                            ];

                            CashySolving::create([
                                'user_id' => Auth::user()->id,
                                'operation' => $operation,
                                'formula' => Session::get('formula'),
                                'answer' => $user_answer,
                                'points' => 0,
                                'correction' => 'incorrect'
                            ]);
                        }

                        return response()->json($result);

                        break;
                    case "Subtraction":

                        if ($system_answer === $user_answer){

                            $result =[
                                'error'=>false,
                                'correct' => true
                            ];
                            $points = $this->getPoints("subtraction");

                            CashySolving::create([
                                'user_id' => Auth::user()->id,
                                'operation' => $operation,
                                'formula' => Session::get('formula'),
                                'answer' => $user_answer,
                                'points' => $points,
                                'correction' => 'correct'
                            ]);

                            $this->updatePoints($points);

                        } else{

                            $result =[
                                'error'=>true,
                                'correct' => false
                            ];

                            CashySolving::create([
                                'user_id' => Auth::user()->id,
                                'operation' => $operation,
                                'formula' => Session::get('formula'),
                                'answer' => $user_answer,
                                'points' => 0,
                                'correction' => 'incorrect'
                            ]);
                        }

                        return response()->json($result);
                        break;
                    case "Multiplication":

                        if ($system_answer === $user_answer){

                            $result =[
                                'error'=>false,
                                'correct' => true
                            ];


                            $points = $this->getPoints('multiplication');

                            CashySolving::create([
                                'user_id' => Auth::user()->id,
                                'operation' => $operation,
                                'formula' => Session::get('formula'),
                                'answer' => $user_answer,
                                'points' => $points,
                                'correction' => 'correct'
                            ]);

                            $this->updatePoints($points);

                        } else{

                            $result =[
                                'error'=>true,
                                'correct' => false
                            ];

                            CashySolving::create([
                                'user_id' => Auth::user()->id,
                                'operation' => $operation,
                                'formula' => Session::get('formula'),
                                'answer' => $user_answer,
                                'points' => 0,
                                'correction' => 'incorrect'
                            ]);
                        }

                        return response()->json($result);

                        break;
                    case "Division":

                        if ($system_answer === $user_answer){

                            $result =[
                                'error'=>false,
                                'correct' => true
                            ];

                            $points = $this->getPoints('division');

                            CashySolving::create([
                                'user_id' => Auth::user()->id,
                                'operation' => $operation,
                                'formula' => Session::get('formula'),
                                'answer' => $user_answer,
                                'points' => $points,
                                'correction' => 'correct'
                            ]);

                            $this->updatePoints($points);

                        } else{

                            $result =[
                                'error'=>true,
                                'correct' => false
                            ];

                            CashySolving::create([
                                'user_id' => Auth::user()->id,
                                'operation' => $operation,
                                'formula' => Session::get('formula'),
                                'answer' => $user_answer,
                                'points' => 0,
                                'correction' => 'incorrect'
                            ]);

                        }

                        return response()->json($result);
                        break;

                    default:
                        $result = [
                            'error'=>false,
                            'correct' => false
                        ];
                        return response()->json($result);
                        break;

                }

            }

            Session::forget([
                'system_answer',
                'formula'
            ]);

        } catch (\Exception $e){
            $result = [
                'error'=>true,
                'message' => $e->getMessage() . $e->getLine()

            ];

            return response()->json($result);
        }


    }

    public function generateNumber($operation){

        try{

            $num1 = 0;
            $num2 = 0;

            switch ($operation){

                case "Addition":
                    $num1 = rand(100, 999);
                    $num2 = rand(0, 99);
                    $system_answer = $num1 + $num2;
                    Session::put('system_answer', $system_answer);
                    Session::put('formula', $num1 ." + " . $num2);

                    $result = [
                        'error'=>false,
                        'num1' => $num1,
                        'num2' => $num2,
                        'operation'=>$operation
                    ];

                    return response()->json($result);
                    break;

                case "Subtraction":
                    $num1 = rand(100, 999);
                    $num2 = rand(0, 99);
                    $system_answer = $num1 - $num2;
                    Session::put('system_answer', $system_answer);
                    Session::put('formula', $num1 ." - " . $num2);

                    $result = [
                        'error'=>false,
                        'num1' => $num1,
                        'num2' => $num2,
                        'operation'=>$operation
                    ];

                    return response()->json($result);
                    break;

                case "Division":
                    $num1 = rand(100, 999);
                    $num2 = rand(0, 99);
                    $system_answer = $num1 / $num2;
                    Session::put('system_answer', $system_answer);
                    Session::put('formula', $num1 ." / " . $num2);

                    $result = [
                        'error'=>false,
                        'num1' => $num1,
                        'num2' => $num2,
                        'operation'=>$operation
                    ];

                    return response()->json($result);
                    break;

                case "Multiplication":
                    $num1 = rand(100, 999);
                    $num2 = rand(0, 99);
                    $system_answer = $num1 * $num2;
                    Session::put('system_answer', $system_answer);
                    Session::put('formula', $num1 ." * " . $num2);

                    $result = [
                        'error'=>false,
                        'num1' => $num1,
                        'num2' => $num2,
                        'operation'=>$operation
                    ];

                    return response()->json($result);
                    break;

                default:
                    $result = [
                        'error'=>true,
                    ];

                    return response()->json($result);
                    break;
            }

        } catch (\Exception $e){

            $result = [
                'error'=>true,

            ];

            return response()->json($result);

        }

    }


    private function getPoints($column){
        $query = DB::table('solving_configurations')
            ->pluck($column)
            ->first();

        if (isset($query)){
            return $query;
        }

        return $query;

    }

    private function updatePoints($points){

        DB::table('users')
            ->where('id', Auth::user()->id)
            ->increment('points', $points);

    }
}
