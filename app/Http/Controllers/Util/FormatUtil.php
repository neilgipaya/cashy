<?php

namespace App\Http\Controllers;

use App\ActivationCode;
use App\Members;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class FormatUtil extends Controller
{
    //


    public function formatBreadCrumbs($render, $parent, $parent_url, $url, $description){

        $html = "<div class='card-block'>
                    <h5 class='m-b-10'>$render</h5>
                    <ul class='breadcrumb-title b-t-default p-t-10'>
                        <li class='breadcrumb-item'>
                            <a href='/dashboard'> <i class='fa fa-home'></i> </a>
                        </li>
                        <li class='breadcrumb-item'>
                            <a href='$parent_url'>$parent</a>
                        </li>
                        <li class='breadcrumb-item'>
                            <a href='$url'>$description</a>
                        </li>
                    </ul>
                </div>";

        return $html;


    }

    public function checkActivationCode($activation_code){
        $query = ActivationCode::all()->where('activation_code', $activation_code)->first();

        if (isset($query)){
            if ($query->status == 1){
                return "Expired";
            }

            return "Success";
        }

        return "Invalid";
    }

    public function getActivationCode($activation_code){

        $query = ActivationCode::all()->where('activation_code', $activation_code)->pluck('id')->first();

        if (isset($query)){
            return $query;
        }

        return null;
    }

    public function updateActivationCode($activation_code){
        $query = DB::table('activation_codes')
            ->where('activation_code', $activation_code)
            ->update([
                'status'=>'1'
            ]);

        if (isset($query)){
            return true;
        }

        return null;
    }

    public function formatReferralCode($firstname, $lastname, $middlename, $username){

        $code = str_shuffle(substr(time(), '0', '5').$firstname[1].$lastname[0].$middlename[0].$username[0]);

        return $code;

    }

    public function checkParentMember($referral_code){

        $query = Members::all()->where('referral_code', $referral_code)->pluck('id');

        if (isset($query)){
            return $query;
        }

        return null;

    }

}
