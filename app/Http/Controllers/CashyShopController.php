<?php

namespace App\Http\Controllers;

use App\AboutItems;
use App\CMSAboutList;
use App\CMSFaq;
use App\CMSFeatureList;
use App\CMSFeatures;
use App\CMSHome;
use Illuminate\Http\Request;

class CashyShopController extends Controller
{
    //

    public function index(){

        $data['cms_header'] = CMSHome::find(1);
        $data['about'] = CMSAboutList::find(1);
        $data['about_lists'] = AboutItems::all();
        $data['features'] = CMSFeatures::find(1);
        $data['feature_lists'] = CMSFeatureList::all();
        $data['faqs'] = CMSFaq::all();


        return view('welcome', $data);

    }

    public function shop(){

        return view('Shop.shop');
    }

    public function productDetails($product_id){

        return view('Shop.details');
    }


}
