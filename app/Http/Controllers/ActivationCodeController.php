<?php

namespace App\Http\Controllers;

use App\ActivationCode;
use App\Http\Requests\ActivationCodeRequest;
use App\LoadTransactions;
use App\Members;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Yajra\DataTables\DataTables;

class ActivationCodeController extends Controller
{
    /**
     * @var FormatUtil
     */
    private $formatUtil;


    /**
     * ActivationCodeController constructor.
     * @param FormatUtil $formatUtil
     */
    public function __construct(FormatUtil $formatUtil)
    {
        $this->formatUtil = $formatUtil;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //

        $data['breadcrumbs'] = $this->formatUtil->formatBreadCrumbs('Members Panel', 'Activation Code', '#', '/activation', 'Activation');

        $data['members'] = Members::getMembers('Active');

        return view('Members.activation', $data);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ActivationCodeRequest $request)
    {
        //

        if ($request->ajax()){
            ActivationCode::create($request->all());

            $notification = array(
                'message' => 'Content Updated Successfully',
                'type' => 'info'
            );

            return response()->json($notification);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\ActivationCode  $activationCode
     * @return \Illuminate\Http\Response
     */
    public function show(ActivationCode $activationCode)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\ActivationCode  $activationCode
     * @return \Illuminate\Http\Response
     */
    public function edit(ActivationCode $activationCode)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\ActivationCode  $activationCode
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ActivationCode $activationCode)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\ActivationCode  $activationCode
     * @return \Illuminate\Http\Response
     */
    public function destroy(ActivationCode $activationCode)
    {
        //
    }

    public function getActivationCode(Request $request){
        if ($request->ajax()){

            $transactions = ActivationCode::getActivationList();

            return DataTables::of($transactions)
                ->addIndexColumn()
                ->make(true);
        }
    }

    public function generateCode(){
        return $this->generateRandomString();
    }

    private function  generateRandomString($length = 10) {

        return substr(str_shuffle(str_repeat($x = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ', ceil($length/strlen($x)) )),1,$length);
    }
}
