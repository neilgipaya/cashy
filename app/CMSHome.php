<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CMSHome extends Model
{
    //

    protected $table = "cms_homes";

    protected $guarded = [];
}
