<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CMSFaq extends Model
{
    //

    protected $table = "cms_faqs";
    protected $guarded = [];
}
