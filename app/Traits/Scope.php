<?php


namespace App\Traits;


class Scope
{

    public function scopeGetSpecificMember($query, $member_id){
        return $query->where('id', $member_id);
    }

    public function scopeToday($query)
    {
        return $query->whereDate('created_at', now()->today());
    }

    public function scopeYesterday($query)
    {
        return $query->whereDate('created_at', now()->yesterday());
    }
}
