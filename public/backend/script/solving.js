"use strict";

let solving = function(){


    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });


    let gameForm = $("#gameForm");

    let operations = gameForm.attr('data-operation');

    let getAnalytics = function () {

        $(".analyticsLoader").css('display', 'block');
        $.ajax({
            type: "GET",
            url: "/getAnalytics/",
            success: function (data) {
                $(".analyticsLoader").css('display', 'none');
                $("#points").html(data['points']);
                $("#correct").html(data['correct']);
                $("#incorrect").html(data['incorrect']);
            },
            error: function (data) {
                console.log('Error:', data);
            }
        });
    };

    let generateNumber = function () {
        $("#solvingLoader").css('display', 'block');
        $("#game").css("display", "none");

        $.ajax({
            type: "GET",
            url: "/generateNumber/" + operations,
            success: function (data) {
                console.log(data);
                $("#solvingLoader").css('display', 'none');
                $("#textStart").html('');
                $("#game").css("display", "block");
                $("#num1").html(data['num1']);
                $("#num2").html(data['num2']);
                $(".answer").val('');
            },
            error: function (data) {
                console.log('Error:', data);
            }
        });
    };

    let formHandler = function () {
        gameForm.on('submit', function (e) {
            e.preventDefault();

            // $("#calculating").modal('show');

            let form_data = $("#gameForm").serialize();

            $.ajax({
                type: "POST",
                url: "/cashySolve",
                data: form_data + '&operation=' + operations,
                success: function (data) {
                    // $("#calculating").modal('hide');

                    if (data['error'] === true){
                        $('#error').modal('show');

                        getAnalytics();

                        setTimeout(function () {
                            generateNumber(operations);
                        }, 5000);

                    }
                    if (data['correct'] === true){
                        $('#success').modal('show');

                        getAnalytics();
                        setTimeout(function () {
                            generateNumber(operations);
                        }, 5000);
                    }

                    table.fnDraw(false);
                },
                error: function (data) {
                    $("#calculating").modal('hide');
                    console.log('Error:', data);
                }
            });
        });
    }


    return {
        init: function () {

            getAnalytics();
            generateNumber();
            formHandler();
        }
    }

}();


jQuery(document).ready(function () {

    solving.init();

});


