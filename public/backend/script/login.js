"use strict";

let login = function() {

    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    let form = $("#login_form");


    let toastMessage = function(response){

        toastr.options = {
            "closeButton": true,
            "debug": false,
            "newestOnTop": true,
            "progressBar": true,
            "positionClass": "toast-top-center",
            "preventDuplicates": true,
            "onclick": null,
            "showDuration": "300",
            "hideDuration": "1000",
            "timeOut": "5000",
            "extendedTimeOut": "1000",
            "showEasing": "swing",
            "hideEasing": "linear",
            "showMethod": "fadeIn",
            "hideMethod": "fadeOut"
        };

        switch (response.type) {
            case 'info':
                toastr.info(response.message);
                break;

            case 'warning':
                toastr.warning(response.message);
                break;

            case 'success':
                toastr.success(response.message);
                break;

            case 'error':
                toastr.error(response.message);
                break;
        }
    };
    
    let handleSubmit = function(){




        form.on("submit", function (e) {
            e.preventDefault();


            let options = {
                url: '/authenticate',
                data: form.serialize(),
                type: 'POST',
                success: function (response) {

                    if (response.success === true){
                        toastMessage(response);
                        setTimeout(function () {
                            window.open('/dashboard', '_self')
                        })
                    }

                    toastMessage(response);
                },
                error: function (response) {
                    new PNotify( {
                            text: response.message,  type: response.type
                        }
                    );
                }
            };

            $.ajax(options)

        })

};
    


    return {
        init: function () {

            handleSubmit();
        }
    }
}();

// Class Initialization
jQuery(document).ready(function () {

    login.init();

});
