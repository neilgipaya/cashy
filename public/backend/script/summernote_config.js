"use strict";

let config = function(){



    let summernoteConfig = function(){

        $(".summernote").summernote(
            {
                placeholder: 'Enter Content',
                tabsize: 2,
                height: 100
            }
        );
    };


    return {
        init: function () {

            summernoteConfig();
        }
    }
}();


// Class Initialization
jQuery(document).ready(function () {

    config.init();

});

