@extends('layout.app')

@section('content')

    <div class="main-body">
        <div class="page-wrapper">

            <div class="page-header card">
{{--                {!! $breadcrumbs !!}--}}
            </div>


            <div class="page-body">

                <div class="row">
                    <div class="col-lg-12">
                        <div class="cover-profile">
                            <div class="profile-bg-img">
                                <img class="profile-bg-img img-fluid"
                                     src="{{ asset('frontend/img/core-img/logo-cover.png') }}"
                                     alt="bg-img">
                                <div class="card-block user-info">
                                    <div class="col-md-12">
                                        <div class="media-left">
                                            <a href="#" class="profile-image">
                                                <img class="user-img img-radius"
                                                     src="{{ empty($info->profile) ? asset('catchy/wow.png') : asset('storage/profile/' .$info->profile)  }}"
                                                     alt="user-img" style="max-height: 120px; max-width: 120px">
                                            </a>
                                        </div>
                                        <div class="media-body row">
                                            <div class="col-lg-12">
                                                <div class="user-title">
                                                    <h2>{{ $info->fullname }}</h2>
                                                </div>
                                            </div>
                                            <div>
                                                <div class="pull-right cover-btn">
                                                    <button type="button"
                                                            class="btn btn-primary m-r-10 m-b-5" data-toggle="modal" data-target="#profile-picture{{ $info->user_id }}"><i
                                                            class="icofont icofont-plus"></i>
                                                        Change Profile Picture</button>
{{--                                                    <button type="button"--}}
{{--                                                            class="btn btn-primary" data-toggle="modal" data-target="#change-cover"><i--}}
{{--                                                            class="icofont icofont-ui-messaging"></i>--}}
{{--                                                        Change Cover</button>--}}
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>


                <div class="row">
                    <div class="col-lg-12">

                        <div class="tab-header card">
                            <ul class="nav nav-tabs md-tabs tab-timeline" role="tablist">
                                <li class="nav-item">
                                    <a class="nav-link active" data-toggle="tab" href="#personal" role="tab" aria-expanded="true">Personal</a>
                                    <div class="slide"></div>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" data-toggle="tab" href="#history" role="tab" aria-expanded="false">Solving History</a>
                                    <div class="slide"></div>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" data-toggle="tab" href="#payout" role="tab" aria-expanded="false">Payout</a>
                                    <div class="slide"></div>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" data-toggle="tab" href="#codes" role="tab" aria-expanded="false">Activation Codes</a>
                                    <div class="slide"></div>
                                </li>
                            </ul>
                        </div>


                        <div class="tab-content card-block">
                            <div class="tab-pane active" id="personal" role="tabpanel" aria-expanded="true">
                                @include('Personal.Profile.personal')
                            </div>
                            <div class="tab-pane" id="history" role="tabpanel" aria-expanded="false">
                                @include('Personal.Profile.solving-history')
                            </div>
                            <div class="tab-pane" id="payout" role="tabpanel" aria-expanded="false">
                                @include('Personal.Profile.payouts')
                            </div>
                            <div class="tab-pane" id="codes" role="tabpanel" aria-expanded="false">
                                @include('Personal.Profile.codes')
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>

    <!--modals -->

    <div class="modal fade" id="edit{{ $info->member_id }}" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <form action="/members/{{ $info->member_id }}" method="POST">
                @csrf
                @method('PATCH')
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title">Update Profile</h4>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <input type="hidden" name="update-profile" />
                       <div class="form-group">
                           <label>Full Name</label>
                           <input type="text" class="form-control" name="fullname" value="{{ $info->fullname }}" />
                       </div>
                        <div class="form-group">
                            <label>Email</label>
                            <input type="text" class="form-control" name="email" value="{{ $info->email }}" />
                        </div>
                        <div class="form-group">
                            <label>Gender</label>
                            <select name="gender" class="form-control" required>
                                <option value="Male" {{ $info->gender == "Male" ? "Selected" : "" }}>Male</option>
                                <option value="Female" {{ $info->gender == "Female" ? "Selected" : "" }}>Female</option>
                                <option value="Transgender" {{ $info->gender == "Transgender" ? "Selected" : "" }}>Transgender</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label>Contact No</label>
                            <input type="tel" class="form-control" name="contact_no" required value="{{ $info->contact_no }}" />
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button"
                                class="btn btn-default waves-effect"
                                data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-success waves-effect waves-light ">Update Record</button>
                    </div>
                </div>
            </form>
        </div>
    </div>

    <div class="modal fade" id="about{{ $info->member_id }}" tabindex="-1" role="dialog">
        <div class="modal-dialog modal-lg" role="document">
            <form action="/members/{{ $info->member_id }}" method="POST">
                @csrf
                @method('PATCH')
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title">About Me</h4>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="form-group">
                            <label>About Me</label>
                            <textarea class="summernote" name="about_me" required>{!! $info->about_me  !!} </textarea>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button"
                                class="btn btn-default waves-effect"
                                data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-success waves-effect waves-light ">Update Record</button>
                    </div>
                </div>
            </form>
        </div>
    </div>

    <div class="modal fade" id="profile-picture{{ $info->member_id }}" tabindex="-1" role="dialog">
        <div class="modal-dialog modal-sm" role="document">
            <form action="/change-profile/{{ $info->member_id }}" method="POST" enctype="multipart/form-data">
                @csrf
                @method('PATCH')
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title">Change Profile Picture</h4>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="form-group">
                            <label class="col-form-label">Profile</label>
                            <input type="file" accept="image/*" name="picture">
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button"
                                class="btn btn-default waves-effect"
                                data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-success waves-effect waves-light ">Update Profile</button>
                    </div>
                </div>
            </form>
        </div>
    </div>

    <div class="modal fade" id="requestPayout" tabindex="-1" role="dialog">
        <div class="modal-dialog modal-sm" role="document">
            <form action="/payouts/{{ $info->user_id }}" method="POST" enctype="multipart/form-data">
                @csrf
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title">Request for Payout</h4>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="form-group">
                            <label class="col-form-label">Profile</label>
                            <input type="file" accept="image/*" name="picture">
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button"
                                class="btn btn-default waves-effect"
                                data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-success waves-effect waves-light ">Update Profile</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection

@section('scripts')


    <script>
        let getAnalytics = function () {

            $(".analyticsLoader").css('display', 'block');
            $.ajax({
                type: "GET",
                url: "/getTotalSolvingHistory/",
                success: function (data) {
                    $(".analyticsLoader").css('display', 'none');
                    console.log(data);
                    // $("#points").html(data['points']);
                    // $("#correct").html(data['correct']);
                    // $("#incorrect").html(data['incorrect']);
                },
                error: function (data) {
                    console.log('Error:', data);
                }
            });
        };

        getAnalytics();
    </script>

@endsection
