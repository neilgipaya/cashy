<div class="card">
    <div class="card-header">
        <h5 class="card-header-text">Solving Precision</h5>
    </div>
    <div class="card-block">
        <div class="row">
            <div class="col-md-6 col-lg-3">
                <div class="card statustic-card">
                    <div class="card-header">
                        <h5>Addition</h5>
                    </div>
                    <div class="card-block text-center">
                        <span class="d-block text-c-blue f-36">
                            <div class="preloader3 loader-block analyticsLoader" style="height:0; align-items: center; display: block">
                                <div class="circ1"></div>
                                <div class="circ2"></div>
                                <div class="circ3"></div>
                                <div class="circ4"></div>
                            </div>
                            <span id="addition-total"></span>
                        </span>
                        <p class="m-b-0">Total</p>
                        <div class="progress">
                            <div class="progress-bar bg-c-blue" style="width: 56%"></div>
                        </div>
                    </div>
                    <div class="card-footer bg-c-blue">
                        <h6 class="text-white m-b-0">Today: <span id="addition-today"></span></h6>
                    </div>
                </div>
            </div>

            <div class="col-md-6 col-lg-3">
                <div class="card statustic-card">
                    <div class="card-header">
                        <h5>Subtraction</h5>
                    </div>
                    <div class="card-block text-center">
                        <span class="d-block text-c-blue f-36">
                            <div class="preloader3 loader-block analyticsLoader" style="height:0; align-items: center; display: block">
                                <div class="circ1"></div>
                                <div class="circ2"></div>
                                <div class="circ3"></div>
                                <div class="circ4"></div>
                            </div>
                            <span id="subtraction-total"></span>
                        </span>
                        <p class="m-b-0">Total</p>
                        <div class="progress">
                            <div class="progress-bar bg-c-pink" style="width:56%"></div>
                        </div>
                    </div>
                    <div class="card-footer bg-c-pink">
                        <h6 class="text-white m-b-0">Today: <span id="subtraction-today"></span></h6>
                    </div>
                </div>
            </div>

            <div class="col-md-6 col-lg-3">
                <div class="card statustic-card">
                    <div class="card-header">
                        <h5>Division</h5>
                    </div>
                    <div class="card-block text-center">
                        <span class="d-block text-c-blue f-36">
                              <div class="preloader3 loader-block analyticsLoader" style="height:0; align-items: center; display: block">
                                <div class="circ1"></div>
                                <div class="circ2"></div>
                                <div class="circ3"></div>
                                <div class="circ4"></div>
                            </div>
                            <span id="division-total"></span>
                        </span>
                        <p class="m-b-0">Total</p>
                        <div class="progress">
                            <div class="progress-bar bg-c-yellow" style="width:56%"></div>
                        </div>
                    </div>
                    <div class="card-footer bg-c-yellow">
                        <h6 class="text-white m-b-0">Today: <span id="division-today"></span> </h6>
                    </div>
                </div>
            </div>

            <div class="col-md-6 col-lg-3">
                <div class="card statustic-card">
                    <div class="card-header">
                        <h5>Multiplication</h5>
                    </div>
                    <div class="card-block text-center">
                        <span class="d-block text-c-blue f-36">
                            <div class="preloader3 loader-block analyticsLoader" style="height:0; align-items: center; display: block">
                                <div class="circ1"></div>
                                <div class="circ2"></div>
                                <div class="circ3"></div>
                                <div class="circ4"></div>
                            </div>
                            <span id="multiplication-total"></span> </span>
                        <p class="m-b-0">Total</p>
                        <div class="progress">
                            <div class="progress-bar bg-c-green" style="width:56%"></div>
                        </div>
                    </div>
                    <div class="card-footer bg-c-green">
                        <h6 class="text-white m-b-0">Today: <span id="multiplication-today"></span> </h6>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-lg-12">
        <div class="card">
            <div class="card-header">
                <h5 class="card-header-text">Profit</h5>
            </div>
            <div class="card-block">
                <div id="main"
                     style="height:300px;width: 100%;"></div>
            </div>
        </div>
    </div>
</div>
