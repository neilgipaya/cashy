<div class="card">
    <div class="card-header">
        <h5 class="card-header-text">About Me</h5>
        <button type="button"
                class="btn btn-sm btn-primary waves-effect waves-light f-right" data-toggle="modal" data-target="#edit{{ $info->user_id }}">
            <i class="fa fa-edit"></i>
        </button>
    </div>
    <div class="card-block">
        <div class="view-info">
            <div class="row">
                <div class="col-lg-12">
                    <div class="general-info">
                        <div class="row">
                            <div class="col-lg-12 col-xl-6">
                                <div class="table-responsive">
                                    <table class="table m-0">
                                        <tbody>
                                        <tr>
                                            <th scope="row">
                                                Full Name
                                            </th>
                                            <td>{{ $info->fullname }}</td>
                                        </tr>
                                        <tr>
                                            <th scope="row">Gender</th>
                                            <td>{{ $info->gender }}</td>
                                        </tr>
                                        <tr>
                                            <th scope="row">Contact No</th>
                                            <td>{{ $info->contact_no }}</td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>

                            <div class="col-lg-12 col-xl-6">
                                <div class="table-responsive">
                                    <table class="table">
                                        <tbody>
                                        <tr>
                                            <th scope="row">
                                                Email</th>
                                            <td>
                                               {{ $info->email }}
                                            </td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>

                        </div>

                    </div>

                </div>

            </div>

        </div>
    </div>

</div>
<div class="row">
    <div class="col-lg-12">
        <div class="card">
            <div class="card-header">
                <h5 class="card-header-text">Description About
                    Me</h5>
                <button id="edit-info-btn" type="button"
                        class="btn btn-sm btn-primary waves-effect waves-light f-right" data-target="#about{{ $info->user_id }}" data-toggle="modal">
                    <i class="fa fa-edit"></i>
                </button>
            </div>
            <div class="card-block user-desc">
                <div class="view-desc">
                    <p>{!! $info->about_me !!}</p>
                </div>
            </div>
        </div>
    </div>
</div>
