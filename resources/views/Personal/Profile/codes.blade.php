<div class="row">
    <div class="col-xl-3">

        <div class="card">
            <div class="card-header contact-user">
                <img class="img-radius img-40"
                     src="{{ asset('backend') }}/assets/images/avatar-4.jpg"
                     alt="contact-user">
                <h5 class="m-l-10">John Doe</h5>
            </div>
            <div class="card-block">
                <ul class="list-group list-contacts">
                    <li class="list-group-item active"><a
                            href="#">All Contacts</a></li>
                    <li class="list-group-item"><a
                            href="#">Recent Contacts</a></li>
                    <li class="list-group-item"><a
                            href="#">Favourite Contacts</a></li>
                </ul>
            </div>
            <div class="card-block groups-contact">
                <h4>Groups</h4>
                <ul class="list-group">
                    <li
                        class="list-group-item justify-content-between">
                        Project
                        <span
                            class="badge badge-primary badge-pill">30</span>
                    </li>
                    <li
                        class="list-group-item justify-content-between">
                        Notes
                        <span
                            class="badge badge-success badge-pill">20</span>
                    </li>
                    <li
                        class="list-group-item justify-content-between">
                        Activity
                        <span
                            class="badge badge-info badge-pill">100</span>
                    </li>
                    <li
                        class="list-group-item justify-content-between">
                        Schedule
                        <span
                            class="badge badge-danger badge-pill">50</span>
                    </li>
                </ul>
            </div>
        </div>
        <div class="card">
            <div class="card-header">
                <h4 class="card-title">Contacts<span
                        class="f-15"> (100)</span></h4>
            </div>
            <div class="card-block">
                <div class="connection-list">
                    <a href="#"><img
                            class="img-fluid img-radius"
                            src="{{ asset('backend') }}/assets/images/user-profile/follower/f-1.jpg"
                            alt="f-1" data-toggle="tooltip"
                            data-placement="top"
                            data-original-title="Airi Satou">
                    </a>
                    <a href="#"><img
                            class="img-fluid img-radius"
                            src="{{ asset('backend') }}/assets/images/user-profile/follower/f-2.jpg"
                            alt="f-2" data-toggle="tooltip"
                            data-placement="top"
                            data-original-title="Angelica Ramos">
                    </a>
                    <a href="#"><img
                            class="img-fluid img-radius"
                            src="{{ asset('backend') }}/assets/images/user-profile/follower/f-3.jpg"
                            alt="f-3" data-toggle="tooltip"
                            data-placement="top"
                            data-original-title="Ashton Cox">
                    </a>
                    <a href="#"><img
                            class="img-fluid img-radius"
                            src="{{ asset('backend') }}/assets/images/user-profile/follower/f-4.jpg"
                            alt="f-4" data-toggle="tooltip"
                            data-placement="top"
                            data-original-title="Cara Stevens">
                    </a>
                    <a href="#"><img
                            class="img-fluid img-radius"
                            src="{{ asset('backend') }}/assets/images/user-profile/follower/f-5.jpg"
                            alt="f-5" data-toggle="tooltip"
                            data-placement="top"
                            data-original-title="Garrett Winters">
                    </a>
                    <a href="#"><img
                            class="img-fluid img-radius"
                            src="{{ asset('backend') }}/assets/images/user-profile/follower/f-1.jpg"
                            alt="f-6" data-toggle="tooltip"
                            data-placement="top"
                            data-original-title="Cedric Kelly">
                    </a>
                    <a href="#"><img
                            class="img-fluid img-radius"
                            src="{{ asset('backend') }}/assets/images/user-profile/follower/f-3.jpg"
                            alt="f-7" data-toggle="tooltip"
                            data-placement="top"
                            data-original-title="Brielle Williamson">
                    </a>
                    <a href="#"><img
                            class="img-fluid img-radius"
                            src="{{ asset('backend') }}/assets/images/user-profile/follower/f-5.jpg"
                            alt="f-8" data-toggle="tooltip"
                            data-placement="top"
                            data-original-title="Jena Gaines">
                    </a>
                </div>
            </div>
        </div>

    </div>
    <div class="col-xl-9">
        <div class="row">
            <div class="col-sm-12">

                <div class="card">
                    <div class="card-header">
                        <h5 class="card-header-text">Contacts
                        </h5>
                    </div>
                    <div class="card-block contact-details">
                        <div
                            class="data_table_main table-responsive dt-responsive">
                            <table id="simpletable"
                                   class="table  table-striped table-bordered nowrap">
                                <thead>
                                <tr>
                                    <th>Name</th>
                                    <th>Email</th>
                                    <th>Mobileno.</th>
                                    <th>Favourite</th>
                                    <th>Action</th>
                                </tr>
                                </thead>
                                <tbody>
                                <tr>
                                    <td>Garrett Winters</td>
                                    <td><a href="http://html.codedthemes.com/cdn-cgi/l/email-protection"
                                           class="__cf_email__"
                                           data-cfemail="65040706545756250208040c094b060a08">[email&#160;protected]</a>
                                    </td>
                                    <td>9989988988</td>
                                    <td><i class="fa fa-star"
                                           aria-hidden="true"></i>
                                    </td>
                                    <td class="dropdown">
                                        <button
                                            type="button"
                                            class="btn btn-primary dropdown-toggle"
                                            data-toggle="dropdown"
                                            aria-haspopup="true"
                                            aria-expanded="false"><i
                                                class="fa fa-cog"
                                                aria-hidden="true"></i></button>
                                        <div
                                            class="dropdown-menu dropdown-menu-right b-none contact-menu">
                                            <a class="dropdown-item"
                                               href="#!"><i
                                                    class="icofont icofont-edit"></i>Edit</a>
                                            <a class="dropdown-item"
                                               href="#!"><i
                                                    class="icofont icofont-ui-delete"></i>Delete</a>
                                            <a class="dropdown-item"
                                               href="#!"><i
                                                    class="icofont icofont-eye-alt"></i>View</a>
                                            <a class="dropdown-item"
                                               href="#!"><i
                                                    class="icofont icofont-tasks-alt"></i>Project</a>
                                            <a class="dropdown-item"
                                               href="#!"><i
                                                    class="icofont icofont-ui-note"></i>Notes</a>
                                            <a class="dropdown-item"
                                               href="#!"><i
                                                    class="icofont icofont-eye-alt"></i>Activity</a>
                                            <a class="dropdown-item"
                                               href="#!"><i
                                                    class="icofont icofont-badge"></i>Schedule</a>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td>Garrett Winters</td>
                                    <td><a href="http://html.codedthemes.com/cdn-cgi/l/email-protection"
                                           class="__cf_email__"
                                           data-cfemail="ec8d8e8fdddedfac8b818d8580c28f8381">[email&#160;protected]</a>
                                    </td>
                                    <td>9989988988</td>
                                    <td><i class="fa fa-star-o"
                                           aria-hidden="true"></i>
                                    </td>
                                    <td class="dropdown">
                                        <button
                                            type="button"
                                            class="btn btn-primary dropdown-toggle"
                                            data-toggle="dropdown"
                                            aria-haspopup="true"
                                            aria-expanded="false"><i
                                                class="fa fa-cog"
                                                aria-hidden="true"></i></button>
                                        <div
                                            class="dropdown-menu dropdown-menu-right b-none contact-menu">
                                            <a class="dropdown-item"
                                               href="#!"><i
                                                    class="icofont icofont-edit"></i>Edit</a>
                                            <a class="dropdown-item"
                                               href="#!"><i
                                                    class="icofont icofont-ui-delete"></i>Delete</a>
                                            <a class="dropdown-item"
                                               href="#!"><i
                                                    class="icofont icofont-eye-alt"></i>View</a>
                                            <a class="dropdown-item"
                                               href="#!"><i
                                                    class="icofont icofont-tasks-alt"></i>Project</a>
                                            <a class="dropdown-item"
                                               href="#!"><i
                                                    class="icofont icofont-ui-note"></i>Notes</a>
                                            <a class="dropdown-item"
                                               href="#!"><i
                                                    class="icofont icofont-eye-alt"></i>Activity</a>
                                            <a class="dropdown-item"
                                               href="#!"><i
                                                    class="icofont icofont-badge"></i>Schedule</a>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td>Garrett Winters</td>
                                    <td><a href="http://html.codedthemes.com/cdn-cgi/l/email-protection"
                                           class="__cf_email__"
                                           data-cfemail="2647444517141566414b474f4a0845494b">[email&#160;protected]</a>
                                    </td>
                                    <td>9989988988</td>
                                    <td><i class="fa fa-star"
                                           aria-hidden="true"></i>
                                    </td>
                                    <td class="dropdown">
                                        <button
                                            type="button"
                                            class="btn btn-primary dropdown-toggle"
                                            data-toggle="dropdown"
                                            aria-haspopup="true"
                                            aria-expanded="false"><i
                                                class="fa fa-cog"
                                                aria-hidden="true"></i></button>
                                        <div
                                            class="dropdown-menu dropdown-menu-right b-none contact-menu">
                                            <a class="dropdown-item"
                                               href="#!"><i
                                                    class="icofont icofont-edit"></i>Edit</a>
                                            <a class="dropdown-item"
                                               href="#!"><i
                                                    class="icofont icofont-ui-delete"></i>Delete</a>
                                            <a class="dropdown-item"
                                               href="#!"><i
                                                    class="icofont icofont-eye-alt"></i>View</a>
                                            <a class="dropdown-item"
                                               href="#!"><i
                                                    class="icofont icofont-tasks-alt"></i>Project</a>
                                            <a class="dropdown-item"
                                               href="#!"><i
                                                    class="icofont icofont-ui-note"></i>Notes</a>
                                            <a class="dropdown-item"
                                               href="#!"><i
                                                    class="icofont icofont-eye-alt"></i>Activity</a>
                                            <a class="dropdown-item"
                                               href="#!"><i
                                                    class="icofont icofont-badge"></i>Schedule</a>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td>Garrett Winters</td>
                                    <td><a href="http://html.codedthemes.com/cdn-cgi/l/email-protection"
                                           class="__cf_email__"
                                           data-cfemail="d4b5b6b7e5e6e794b3b9b5bdb8fab7bbb9">[email&#160;protected]</a>
                                    </td>
                                    <td>9989988988</td>
                                    <td><i class="fa fa-star"
                                           aria-hidden="true"></i>
                                    </td>
                                    <td class="dropdown">
                                        <button
                                            type="button"
                                            class="btn btn-primary dropdown-toggle"
                                            data-toggle="dropdown"
                                            aria-haspopup="true"
                                            aria-expanded="false"><i
                                                class="fa fa-cog"
                                                aria-hidden="true"></i></button>
                                        <div
                                            class="dropdown-menu dropdown-menu-right b-none contact-menu">
                                            <a class="dropdown-item"
                                               href="#!"><i
                                                    class="icofont icofont-edit"></i>Edit</a>
                                            <a class="dropdown-item"
                                               href="#!"><i
                                                    class="icofont icofont-ui-delete"></i>Delete</a>
                                            <a class="dropdown-item"
                                               href="#!"><i
                                                    class="icofont icofont-eye-alt"></i>View</a>
                                            <a class="dropdown-item"
                                               href="#!"><i
                                                    class="icofont icofont-tasks-alt"></i>Project</a>
                                            <a class="dropdown-item"
                                               href="#!"><i
                                                    class="icofont icofont-ui-note"></i>Notes</a>
                                            <a class="dropdown-item"
                                               href="#!"><i
                                                    class="icofont icofont-eye-alt"></i>Activity</a>
                                            <a class="dropdown-item"
                                               href="#!"><i
                                                    class="icofont icofont-badge"></i>Schedule</a>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td>Garrett Winters</td>
                                    <td><a href="http://html.codedthemes.com/cdn-cgi/l/email-protection"
                                           class="__cf_email__"
                                           data-cfemail="7d1c1f1e4c4f4e3d1a101c1411531e1210">[email&#160;protected]</a>
                                    </td>
                                    <td>9989988988</td>
                                    <td><i class="fa fa-star-o"
                                           aria-hidden="true"></i>
                                    </td>
                                    <td class="dropdown">
                                        <button
                                            type="button"
                                            class="btn btn-primary dropdown-toggle"
                                            data-toggle="dropdown"
                                            aria-haspopup="true"
                                            aria-expanded="false"><i
                                                class="fa fa-cog"
                                                aria-hidden="true"></i></button>
                                        <div
                                            class="dropdown-menu dropdown-menu-right b-none contact-menu">
                                            <a class="dropdown-item"
                                               href="#!"><i
                                                    class="icofont icofont-edit"></i>Edit</a>
                                            <a class="dropdown-item"
                                               href="#!"><i
                                                    class="icofont icofont-ui-delete"></i>Delete</a>
                                            <a class="dropdown-item"
                                               href="#!"><i
                                                    class="icofont icofont-eye-alt"></i>View</a>
                                            <a class="dropdown-item"
                                               href="#!"><i
                                                    class="icofont icofont-tasks-alt"></i>Project</a>
                                            <a class="dropdown-item"
                                               href="#!"><i
                                                    class="icofont icofont-ui-note"></i>Notes</a>
                                            <a class="dropdown-item"
                                               href="#!"><i
                                                    class="icofont icofont-eye-alt"></i>Activity</a>
                                            <a class="dropdown-item"
                                               href="#!"><i
                                                    class="icofont icofont-badge"></i>Schedule</a>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td>Garrett Winters</td>
                                    <td><a href="http://html.codedthemes.com/cdn-cgi/l/email-protection"
                                           class="__cf_email__"
                                           data-cfemail="a2c3c0c1939091e2c5cfc3cbce8cc1cdcf">[email&#160;protected]</a>
                                    </td>
                                    <td>9989988988</td>
                                    <td><i class="fa fa-star"
                                           aria-hidden="true"></i>
                                    </td>
                                    <td class="dropdown">
                                        <button
                                            type="button"
                                            class="btn btn-primary dropdown-toggle"
                                            data-toggle="dropdown"
                                            aria-haspopup="true"
                                            aria-expanded="false"><i
                                                class="fa fa-cog"
                                                aria-hidden="true"></i></button>
                                        <div
                                            class="dropdown-menu dropdown-menu-right b-none contact-menu">
                                            <a class="dropdown-item"
                                               href="#!"><i
                                                    class="icofont icofont-edit"></i>Edit</a>
                                            <a class="dropdown-item"
                                               href="#!"><i
                                                    class="icofont icofont-ui-delete"></i>Delete</a>
                                            <a class="dropdown-item"
                                               href="#!"><i
                                                    class="icofont icofont-eye-alt"></i>View</a>
                                            <a class="dropdown-item"
                                               href="#!"><i
                                                    class="icofont icofont-tasks-alt"></i>Project</a>
                                            <a class="dropdown-item"
                                               href="#!"><i
                                                    class="icofont icofont-ui-note"></i>Notes</a>
                                            <a class="dropdown-item"
                                               href="#!"><i
                                                    class="icofont icofont-eye-alt"></i>Activity</a>
                                            <a class="dropdown-item"
                                               href="#!"><i
                                                    class="icofont icofont-badge"></i>Schedule</a>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td>Garrett Winters</td>
                                    <td><a href="http://html.codedthemes.com/cdn-cgi/l/email-protection"
                                           class="__cf_email__"
                                           data-cfemail="25444746141716654248444c490b464a48">[email&#160;protected]</a>
                                    </td>
                                    <td>9989988988</td>
                                    <td><i class="fa fa-star"
                                           aria-hidden="true"></i>
                                    </td>
                                    <td class="dropdown">
                                        <button
                                            type="button"
                                            class="btn btn-primary dropdown-toggle"
                                            data-toggle="dropdown"
                                            aria-haspopup="true"
                                            aria-expanded="false"><i
                                                class="fa fa-cog"
                                                aria-hidden="true"></i></button>
                                        <div
                                            class="dropdown-menu dropdown-menu-right b-none contact-menu">
                                            <a class="dropdown-item"
                                               href="#!"><i
                                                    class="icofont icofont-edit"></i>Edit</a>
                                            <a class="dropdown-item"
                                               href="#!"><i
                                                    class="icofont icofont-ui-delete"></i>Delete</a>
                                            <a class="dropdown-item"
                                               href="#!"><i
                                                    class="icofont icofont-eye-alt"></i>View</a>
                                            <a class="dropdown-item"
                                               href="#!"><i
                                                    class="icofont icofont-tasks-alt"></i>Project</a>
                                            <a class="dropdown-item"
                                               href="#!"><i
                                                    class="icofont icofont-ui-note"></i>Notes</a>
                                            <a class="dropdown-item"
                                               href="#!"><i
                                                    class="icofont icofont-eye-alt"></i>Activity</a>
                                            <a class="dropdown-item"
                                               href="#!"><i
                                                    class="icofont icofont-badge"></i>Schedule</a>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td>Garrett Winters</td>
                                    <td><a href="http://html.codedthemes.com/cdn-cgi/l/email-protection"
                                           class="__cf_email__"
                                           data-cfemail="9bfaf9f8aaa9a8dbfcf6faf2f7b5f8f4f6">[email&#160;protected]</a>
                                    </td>
                                    <td>9989988988</td>
                                    <td><i class="fa fa-star"
                                           aria-hidden="true"></i>
                                    </td>
                                    <td class="dropdown">
                                        <button
                                            type="button"
                                            class="btn btn-primary dropdown-toggle"
                                            data-toggle="dropdown"
                                            aria-haspopup="true"
                                            aria-expanded="false"><i
                                                class="fa fa-cog"
                                                aria-hidden="true"></i></button>
                                        <div
                                            class="dropdown-menu dropdown-menu-right b-none contact-menu">
                                            <a class="dropdown-item"
                                               href="#!"><i
                                                    class="icofont icofont-edit"></i>Edit</a>
                                            <a class="dropdown-item"
                                               href="#!"><i
                                                    class="icofont icofont-ui-delete"></i>Delete</a>
                                            <a class="dropdown-item"
                                               href="#!"><i
                                                    class="icofont icofont-eye-alt"></i>View</a>
                                            <a class="dropdown-item"
                                               href="#!"><i
                                                    class="icofont icofont-tasks-alt"></i>Project</a>
                                            <a class="dropdown-item"
                                               href="#!"><i
                                                    class="icofont icofont-ui-note"></i>Notes</a>
                                            <a class="dropdown-item"
                                               href="#!"><i
                                                    class="icofont icofont-eye-alt"></i>Activity</a>
                                            <a class="dropdown-item"
                                               href="#!"><i
                                                    class="icofont icofont-badge"></i>Schedule</a>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td>Garrett Winters</td>
                                    <td><a href="http://html.codedthemes.com/cdn-cgi/l/email-protection"
                                           class="__cf_email__"
                                           data-cfemail="b8d9dadb898a8bf8dfd5d9d1d496dbd7d5">[email&#160;protected]</a>
                                    </td>
                                    <td>9989988988</td>
                                    <td><i class="fa fa-star"
                                           aria-hidden="true"></i>
                                    </td>
                                    <td class="dropdown">
                                        <button
                                            type="button"
                                            class="btn btn-primary dropdown-toggle"
                                            data-toggle="dropdown"
                                            aria-haspopup="true"
                                            aria-expanded="false"><i
                                                class="fa fa-cog"
                                                aria-hidden="true"></i></button>
                                        <div
                                            class="dropdown-menu dropdown-menu-right b-none contact-menu">
                                            <a class="dropdown-item"
                                               href="#!"><i
                                                    class="icofont icofont-edit"></i>Edit</a>
                                            <a class="dropdown-item"
                                               href="#!"><i
                                                    class="icofont icofont-ui-delete"></i>Delete</a>
                                            <a class="dropdown-item"
                                               href="#!"><i
                                                    class="icofont icofont-eye-alt"></i>View</a>
                                            <a class="dropdown-item"
                                               href="#!"><i
                                                    class="icofont icofont-tasks-alt"></i>Project</a>
                                            <a class="dropdown-item"
                                               href="#!"><i
                                                    class="icofont icofont-ui-note"></i>Notes</a>
                                            <a class="dropdown-item"
                                               href="#!"><i
                                                    class="icofont icofont-eye-alt"></i>Activity</a>
                                            <a class="dropdown-item"
                                               href="#!"><i
                                                    class="icofont icofont-badge"></i>Schedule</a>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td>Garrett Winters</td>
                                    <td><a href="http://html.codedthemes.com/cdn-cgi/l/email-protection"
                                           class="__cf_email__"
                                           data-cfemail="95f4f7f6a4a7a6d5f2f8f4fcf9bbf6faf8">[email&#160;protected]</a>
                                    </td>
                                    <td>9989988988</td>
                                    <td><i class="fa fa-star-o"
                                           aria-hidden="true"></i>
                                    </td>
                                    <td class="dropdown">
                                        <button
                                            type="button"
                                            class="btn btn-primary dropdown-toggle"
                                            data-toggle="dropdown"
                                            aria-haspopup="true"
                                            aria-expanded="false"><i
                                                class="fa fa-cog"
                                                aria-hidden="true"></i></button>
                                        <div
                                            class="dropdown-menu dropdown-menu-right b-none contact-menu">
                                            <a class="dropdown-item"
                                               href="#!"><i
                                                    class="icofont icofont-edit"></i>Edit</a>
                                            <a class="dropdown-item"
                                               href="#!"><i
                                                    class="icofont icofont-ui-delete"></i>Delete</a>
                                            <a class="dropdown-item"
                                               href="#!"><i
                                                    class="icofont icofont-eye-alt"></i>View</a>
                                            <a class="dropdown-item"
                                               href="#!"><i
                                                    class="icofont icofont-tasks-alt"></i>Project</a>
                                            <a class="dropdown-item"
                                               href="#!"><i
                                                    class="icofont icofont-ui-note"></i>Notes</a>
                                            <a class="dropdown-item"
                                               href="#!"><i
                                                    class="icofont icofont-eye-alt"></i>Activity</a>
                                            <a class="dropdown-item"
                                               href="#!"><i
                                                    class="icofont icofont-badge"></i>Schedule</a>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td>Garrett Winters</td>
                                    <td><a href="http://html.codedthemes.com/cdn-cgi/l/email-protection"
                                           class="__cf_email__"
                                           data-cfemail="64050607555657240309050d084a070b09">[email&#160;protected]</a>
                                    </td>
                                    <td>9989988988</td>
                                    <td><i class="fa fa-star"
                                           aria-hidden="true"></i>
                                    </td>
                                    <td class="dropdown">
                                        <button
                                            type="button"
                                            class="btn btn-primary dropdown-toggle"
                                            data-toggle="dropdown"
                                            aria-haspopup="true"
                                            aria-expanded="false"><i
                                                class="fa fa-cog"
                                                aria-hidden="true"></i></button>
                                        <div
                                            class="dropdown-menu dropdown-menu-right b-none contact-menu">
                                            <a class="dropdown-item"
                                               href="#!"><i
                                                    class="icofont icofont-edit"></i>Edit</a>
                                            <a class="dropdown-item"
                                               href="#!"><i
                                                    class="icofont icofont-ui-delete"></i>Delete</a>
                                            <a class="dropdown-item"
                                               href="#!"><i
                                                    class="icofont icofont-eye-alt"></i>View</a>
                                            <a class="dropdown-item"
                                               href="#!"><i
                                                    class="icofont icofont-tasks-alt"></i>Project</a>
                                            <a class="dropdown-item"
                                               href="#!"><i
                                                    class="icofont icofont-ui-note"></i>Notes</a>
                                            <a class="dropdown-item"
                                               href="#!"><i
                                                    class="icofont icofont-eye-alt"></i>Activity</a>
                                            <a class="dropdown-item"
                                               href="#!"><i
                                                    class="icofont icofont-badge"></i>Schedule</a>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td>Garrett Winters</td>
                                    <td><a href="http://html.codedthemes.com/cdn-cgi/l/email-protection"
                                           class="__cf_email__"
                                           data-cfemail="c6a7a4a5f7f4f586a1aba7afaae8a5a9ab">[email&#160;protected]</a>
                                    </td>
                                    <td>9989988988</td>
                                    <td><i class="fa fa-star"
                                           aria-hidden="true"></i>
                                    </td>
                                    <td class="dropdown">
                                        <button
                                            type="button"
                                            class="btn btn-primary dropdown-toggle"
                                            data-toggle="dropdown"
                                            aria-haspopup="true"
                                            aria-expanded="false"><i
                                                class="fa fa-cog"
                                                aria-hidden="true"></i></button>
                                        <div
                                            class="dropdown-menu dropdown-menu-right b-none contact-menu">
                                            <a class="dropdown-item"
                                               href="#!"><i
                                                    class="icofont icofont-edit"></i>Edit</a>
                                            <a class="dropdown-item"
                                               href="#!"><i
                                                    class="icofont icofont-ui-delete"></i>Delete</a>
                                            <a class="dropdown-item"
                                               href="#!"><i
                                                    class="icofont icofont-eye-alt"></i>View</a>
                                            <a class="dropdown-item"
                                               href="#!"><i
                                                    class="icofont icofont-tasks-alt"></i>Project</a>
                                            <a class="dropdown-item"
                                               href="#!"><i
                                                    class="icofont icofont-ui-note"></i>Notes</a>
                                            <a class="dropdown-item"
                                               href="#!"><i
                                                    class="icofont icofont-eye-alt"></i>Activity</a>
                                            <a class="dropdown-item"
                                               href="#!"><i
                                                    class="icofont icofont-badge"></i>Schedule</a>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td>Garrett Winters</td>
                                    <td><a href="http://html.codedthemes.com/cdn-cgi/l/email-protection"
                                           class="__cf_email__"
                                           data-cfemail="35545756040706755258545c591b565a58">[email&#160;protected]</a>
                                    </td>
                                    <td>9989988988</td>
                                    <td><i class="fa fa-star"
                                           aria-hidden="true"></i>
                                    </td>
                                    <td class="dropdown">
                                        <button
                                            type="button"
                                            class="btn btn-primary dropdown-toggle"
                                            data-toggle="dropdown"
                                            aria-haspopup="true"
                                            aria-expanded="false"><i
                                                class="fa fa-cog"
                                                aria-hidden="true"></i></button>
                                        <div
                                            class="dropdown-menu dropdown-menu-right b-none contact-menu">
                                            <a class="dropdown-item"
                                               href="#!"><i
                                                    class="icofont icofont-edit"></i>Edit</a>
                                            <a class="dropdown-item"
                                               href="#!"><i
                                                    class="icofont icofont-ui-delete"></i>Delete</a>
                                            <a class="dropdown-item"
                                               href="#!"><i
                                                    class="icofont icofont-eye-alt"></i>View</a>
                                            <a class="dropdown-item"
                                               href="#!"><i
                                                    class="icofont icofont-tasks-alt"></i>Project</a>
                                            <a class="dropdown-item"
                                               href="#!"><i
                                                    class="icofont icofont-ui-note"></i>Notes</a>
                                            <a class="dropdown-item"
                                               href="#!"><i
                                                    class="icofont icofont-eye-alt"></i>Activity</a>
                                            <a class="dropdown-item"
                                               href="#!"><i
                                                    class="icofont icofont-badge"></i>Schedule</a>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td>Garrett Winters</td>
                                    <td><a href="http://html.codedthemes.com/cdn-cgi/l/email-protection"
                                           class="__cf_email__"
                                           data-cfemail="4322212072717003242e222a2f6d202c2e">[email&#160;protected]</a>
                                    </td>
                                    <td>9989988988</td>
                                    <td><i class="fa fa-star"
                                           aria-hidden="true"></i>
                                    </td>
                                    <td class="dropdown">
                                        <button
                                            type="button"
                                            class="btn btn-primary dropdown-toggle"
                                            data-toggle="dropdown"
                                            aria-haspopup="true"
                                            aria-expanded="false"><i
                                                class="fa fa-cog"
                                                aria-hidden="true"></i></button>
                                        <div
                                            class="dropdown-menu dropdown-menu-right b-none contact-menu">
                                            <a class="dropdown-item"
                                               href="#!"><i
                                                    class="icofont icofont-edit"></i>Edit</a>
                                            <a class="dropdown-item"
                                               href="#!"><i
                                                    class="icofont icofont-ui-delete"></i>Delete</a>
                                            <a class="dropdown-item"
                                               href="#!"><i
                                                    class="icofont icofont-eye-alt"></i>View</a>
                                            <a class="dropdown-item"
                                               href="#!"><i
                                                    class="icofont icofont-tasks-alt"></i>Project</a>
                                            <a class="dropdown-item"
                                               href="#!"><i
                                                    class="icofont icofont-ui-note"></i>Notes</a>
                                            <a class="dropdown-item"
                                               href="#!"><i
                                                    class="icofont icofont-eye-alt"></i>Activity</a>
                                            <a class="dropdown-item"
                                               href="#!"><i
                                                    class="icofont icofont-badge"></i>Schedule</a>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td>Garrett Winters</td>
                                    <td><a href="http://html.codedthemes.com/cdn-cgi/l/email-protection"
                                           class="__cf_email__"
                                           data-cfemail="f0919293c1c2c3b0979d91999cde939f9d">[email&#160;protected]</a>
                                    </td>
                                    <td>9989988988</td>
                                    <td><i class="fa fa-star-o"
                                           aria-hidden="true"></i>
                                    </td>
                                    <td class="dropdown">
                                        <button
                                            type="button"
                                            class="btn btn-primary dropdown-toggle"
                                            data-toggle="dropdown"
                                            aria-haspopup="true"
                                            aria-expanded="false"><i
                                                class="fa fa-cog"
                                                aria-hidden="true"></i></button>
                                        <div
                                            class="dropdown-menu dropdown-menu-right b-none contact-menu">
                                            <a class="dropdown-item"
                                               href="#!"><i
                                                    class="icofont icofont-edit"></i>Edit</a>
                                            <a class="dropdown-item"
                                               href="#!"><i
                                                    class="icofont icofont-ui-delete"></i>Delete</a>
                                            <a class="dropdown-item"
                                               href="#!"><i
                                                    class="icofont icofont-eye-alt"></i>View</a>
                                            <a class="dropdown-item"
                                               href="#!"><i
                                                    class="icofont icofont-tasks-alt"></i>Project</a>
                                            <a class="dropdown-item"
                                               href="#!"><i
                                                    class="icofont icofont-ui-note"></i>Notes</a>
                                            <a class="dropdown-item"
                                               href="#!"><i
                                                    class="icofont icofont-eye-alt"></i>Activity</a>
                                            <a class="dropdown-item"
                                               href="#!"><i
                                                    class="icofont icofont-badge"></i>Schedule</a>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td>Garrett Winters</td>
                                    <td><a href="http://html.codedthemes.com/cdn-cgi/l/email-protection"
                                           class="__cf_email__"
                                           data-cfemail="b0d1d2d3818283f0d7ddd1d9dc9ed3dfdd">[email&#160;protected]</a>
                                    </td>
                                    <td>9989988988</td>
                                    <td><i class="fa fa-star"
                                           aria-hidden="true"></i>
                                    </td>
                                    <td class="dropdown">
                                        <button
                                            type="button"
                                            class="btn btn-primary dropdown-toggle"
                                            data-toggle="dropdown"
                                            aria-haspopup="true"
                                            aria-expanded="false"><i
                                                class="fa fa-cog"
                                                aria-hidden="true"></i></button>
                                        <div
                                            class="dropdown-menu dropdown-menu-right b-none contact-menu">
                                            <a class="dropdown-item"
                                               href="#!"><i
                                                    class="icofont icofont-edit"></i>Edit</a>
                                            <a class="dropdown-item"
                                               href="#!"><i
                                                    class="icofont icofont-ui-delete"></i>Delete</a>
                                            <a class="dropdown-item"
                                               href="#!"><i
                                                    class="icofont icofont-eye-alt"></i>View</a>
                                            <a class="dropdown-item"
                                               href="#!"><i
                                                    class="icofont icofont-tasks-alt"></i>Project</a>
                                            <a class="dropdown-item"
                                               href="#!"><i
                                                    class="icofont icofont-ui-note"></i>Notes</a>
                                            <a class="dropdown-item"
                                               href="#!"><i
                                                    class="icofont icofont-eye-alt"></i>Activity</a>
                                            <a class="dropdown-item"
                                               href="#!"><i
                                                    class="icofont icofont-badge"></i>Schedule</a>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td>Garrett Winters</td>
                                    <td><a href="http://html.codedthemes.com/cdn-cgi/l/email-protection"
                                           class="__cf_email__"
                                           data-cfemail="1f7e7d7c2e2d2c5f78727e7673317c7072">[email&#160;protected]</a>
                                    </td>
                                    <td>9989988988</td>
                                    <td><i class="fa fa-star-o"
                                           aria-hidden="true"></i>
                                    </td>
                                    <td class="dropdown">
                                        <button
                                            type="button"
                                            class="btn btn-primary dropdown-toggle"
                                            data-toggle="dropdown"
                                            aria-haspopup="true"
                                            aria-expanded="false"><i
                                                class="fa fa-cog"
                                                aria-hidden="true"></i></button>
                                        <div
                                            class="dropdown-menu dropdown-menu-right b-none contact-menu">
                                            <a class="dropdown-item"
                                               href="#!"><i
                                                    class="icofont icofont-edit"></i>Edit</a>
                                            <a class="dropdown-item"
                                               href="#!"><i
                                                    class="icofont icofont-ui-delete"></i>Delete</a>
                                            <a class="dropdown-item"
                                               href="#!"><i
                                                    class="icofont icofont-eye-alt"></i>View</a>
                                            <a class="dropdown-item"
                                               href="#!"><i
                                                    class="icofont icofont-tasks-alt"></i>Project</a>
                                            <a class="dropdown-item"
                                               href="#!"><i
                                                    class="icofont icofont-ui-note"></i>Notes</a>
                                            <a class="dropdown-item"
                                               href="#!"><i
                                                    class="icofont icofont-eye-alt"></i>Activity</a>
                                            <a class="dropdown-item"
                                               href="#!"><i
                                                    class="icofont icofont-badge"></i>Schedule</a>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td>Garrett Winters</td>
                                    <td><a href="http://html.codedthemes.com/cdn-cgi/l/email-protection"
                                           class="__cf_email__"
                                           data-cfemail="ea8b8889dbd8d9aa8d878b8386c4898587">[email&#160;protected]</a>
                                    </td>
                                    <td>9989988988</td>
                                    <td><i class="fa fa-star"
                                           aria-hidden="true"></i>
                                    </td>
                                    <td class="dropdown">
                                        <button
                                            type="button"
                                            class="btn btn-primary dropdown-toggle"
                                            data-toggle="dropdown"
                                            aria-haspopup="true"
                                            aria-expanded="false"><i
                                                class="fa fa-cog"
                                                aria-hidden="true"></i></button>
                                        <div
                                            class="dropdown-menu dropdown-menu-right b-none contact-menu">
                                            <a class="dropdown-item"
                                               href="#!"><i
                                                    class="icofont icofont-edit"></i>Edit</a>
                                            <a class="dropdown-item"
                                               href="#!"><i
                                                    class="icofont icofont-ui-delete"></i>Delete</a>
                                            <a class="dropdown-item"
                                               href="#!"><i
                                                    class="icofont icofont-eye-alt"></i>View</a>
                                            <a class="dropdown-item"
                                               href="#!"><i
                                                    class="icofont icofont-tasks-alt"></i>Project</a>
                                            <a class="dropdown-item"
                                               href="#!"><i
                                                    class="icofont icofont-ui-note"></i>Notes</a>
                                            <a class="dropdown-item"
                                               href="#!"><i
                                                    class="icofont icofont-eye-alt"></i>Activity</a>
                                            <a class="dropdown-item"
                                               href="#!"><i
                                                    class="icofont icofont-badge"></i>Schedule</a>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td>Garrett Winters</td>
                                    <td><a href="http://html.codedthemes.com/cdn-cgi/l/email-protection"
                                           class="__cf_email__"
                                           data-cfemail="29484b4a181b1a694e44484045074a4644">[email&#160;protected]</a>
                                    </td>
                                    <td>9989988988</td>
                                    <td><i class="fa fa-star"
                                           aria-hidden="true"></i>
                                    </td>
                                    <td class="dropdown">
                                        <button
                                            type="button"
                                            class="btn btn-primary dropdown-toggle"
                                            data-toggle="dropdown"
                                            aria-haspopup="true"
                                            aria-expanded="false"><i
                                                class="fa fa-cog"
                                                aria-hidden="true"></i></button>
                                        <div
                                            class="dropdown-menu dropdown-menu-right b-none contact-menu">
                                            <a class="dropdown-item"
                                               href="#!"><i
                                                    class="icofont icofont-edit"></i>Edit</a>
                                            <a class="dropdown-item"
                                               href="#!"><i
                                                    class="icofont icofont-ui-delete"></i>Delete</a>
                                            <a class="dropdown-item"
                                               href="#!"><i
                                                    class="icofont icofont-eye-alt"></i>View</a>
                                            <a class="dropdown-item"
                                               href="#!"><i
                                                    class="icofont icofont-tasks-alt"></i>Project</a>
                                            <a class="dropdown-item"
                                               href="#!"><i
                                                    class="icofont icofont-ui-note"></i>Notes</a>
                                            <a class="dropdown-item"
                                               href="#!"><i
                                                    class="icofont icofont-eye-alt"></i>Activity</a>
                                            <a class="dropdown-item"
                                               href="#!"><i
                                                    class="icofont icofont-badge"></i>Schedule</a>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td>Garrett Winters</td>
                                    <td><a href="http://html.codedthemes.com/cdn-cgi/l/email-protection"
                                           class="__cf_email__"
                                           data-cfemail="d5b4b7b6e4e7e695b2b8b4bcb9fbb6bab8">[email&#160;protected]</a>
                                    </td>
                                    <td>9989988988</td>
                                    <td><i class="fa fa-star-o"
                                           aria-hidden="true"></i>
                                    </td>
                                    <td class="dropdown">
                                        <button
                                            type="button"
                                            class="btn btn-primary dropdown-toggle"
                                            data-toggle="dropdown"
                                            aria-haspopup="true"
                                            aria-expanded="false"><i
                                                class="fa fa-cog"
                                                aria-hidden="true"></i></button>
                                        <div
                                            class="dropdown-menu dropdown-menu-right b-none contact-menu">
                                            <a class="dropdown-item"
                                               href="#!"><i
                                                    class="icofont icofont-edit"></i>Edit</a>
                                            <a class="dropdown-item"
                                               href="#!"><i
                                                    class="icofont icofont-ui-delete"></i>Delete</a>
                                            <a class="dropdown-item"
                                               href="#!"><i
                                                    class="icofont icofont-eye-alt"></i>View</a>
                                            <a class="dropdown-item"
                                               href="#!"><i
                                                    class="icofont icofont-tasks-alt"></i>Project</a>
                                            <a class="dropdown-item"
                                               href="#!"><i
                                                    class="icofont icofont-ui-note"></i>Notes</a>
                                            <a class="dropdown-item"
                                               href="#!"><i
                                                    class="icofont icofont-eye-alt"></i>Activity</a>
                                            <a class="dropdown-item"
                                               href="#!"><i
                                                    class="icofont icofont-badge"></i>Schedule</a>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td>Garrett Winters</td>
                                    <td><a href="http://html.codedthemes.com/cdn-cgi/l/email-protection"
                                           class="__cf_email__"
                                           data-cfemail="c0a1a2a3f1f2f380a7ada1a9aceea3afad">[email&#160;protected]</a>
                                    </td>
                                    <td>9989988988</td>
                                    <td><i class="fa fa-star"
                                           aria-hidden="true"></i>
                                    </td>
                                    <td class="dropdown">
                                        <button
                                            type="button"
                                            class="btn btn-primary dropdown-toggle"
                                            data-toggle="dropdown"
                                            aria-haspopup="true"
                                            aria-expanded="false"><i
                                                class="fa fa-cog"
                                                aria-hidden="true"></i></button>
                                        <div
                                            class="dropdown-menu dropdown-menu-right b-none contact-menu">
                                            <a class="dropdown-item"
                                               href="#!"><i
                                                    class="icofont icofont-edit"></i>Edit</a>
                                            <a class="dropdown-item"
                                               href="#!"><i
                                                    class="icofont icofont-ui-delete"></i>Delete</a>
                                            <a class="dropdown-item"
                                               href="#!"><i
                                                    class="icofont icofont-eye-alt"></i>View</a>
                                            <a class="dropdown-item"
                                               href="#!"><i
                                                    class="icofont icofont-tasks-alt"></i>Project</a>
                                            <a class="dropdown-item"
                                               href="#!"><i
                                                    class="icofont icofont-ui-note"></i>Notes</a>
                                            <a class="dropdown-item"
                                               href="#!"><i
                                                    class="icofont icofont-eye-alt"></i>Activity</a>
                                            <a class="dropdown-item"
                                               href="#!"><i
                                                    class="icofont icofont-badge"></i>Schedule</a>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td>Garrett Winters</td>
                                    <td><a href="http://html.codedthemes.com/cdn-cgi/l/email-protection"
                                           class="__cf_email__"
                                           data-cfemail="8aebe8e9bbb8b9caede7ebe3e6a4e9e5e7">[email&#160;protected]</a>
                                    </td>
                                    <td>9989988988</td>
                                    <td><i class="fa fa-star"
                                           aria-hidden="true"></i>
                                    </td>
                                    <td class="dropdown">
                                        <button
                                            type="button"
                                            class="btn btn-primary dropdown-toggle"
                                            data-toggle="dropdown"
                                            aria-haspopup="true"
                                            aria-expanded="false"><i
                                                class="fa fa-cog"
                                                aria-hidden="true"></i></button>
                                        <div
                                            class="dropdown-menu dropdown-menu-right b-none contact-menu">
                                            <a class="dropdown-item"
                                               href="#!"><i
                                                    class="icofont icofont-edit"></i>Edit</a>
                                            <a class="dropdown-item"
                                               href="#!"><i
                                                    class="icofont icofont-ui-delete"></i>Delete</a>
                                            <a class="dropdown-item"
                                               href="#!"><i
                                                    class="icofont icofont-eye-alt"></i>View</a>
                                            <a class="dropdown-item"
                                               href="#!"><i
                                                    class="icofont icofont-tasks-alt"></i>Project</a>
                                            <a class="dropdown-item"
                                               href="#!"><i
                                                    class="icofont icofont-ui-note"></i>Notes</a>
                                            <a class="dropdown-item"
                                               href="#!"><i
                                                    class="icofont icofont-eye-alt"></i>Activity</a>
                                            <a class="dropdown-item"
                                               href="#!"><i
                                                    class="icofont icofont-badge"></i>Schedule</a>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td>Garrett Winters</td>
                                    <td><a href="http://html.codedthemes.com/cdn-cgi/l/email-protection"
                                           class="__cf_email__"
                                           data-cfemail="09686b6a383b3a496e64686065276a6664">[email&#160;protected]</a>
                                    </td>
                                    <td>9989988988</td>
                                    <td><i class="fa fa-star-o"
                                           aria-hidden="true"></i>
                                    </td>
                                    <td class="dropdown">
                                        <button
                                            type="button"
                                            class="btn btn-primary dropdown-toggle"
                                            data-toggle="dropdown"
                                            aria-haspopup="true"
                                            aria-expanded="false"><i
                                                class="fa fa-cog"
                                                aria-hidden="true"></i></button>
                                        <div
                                            class="dropdown-menu dropdown-menu-right b-none contact-menu">
                                            <a class="dropdown-item"
                                               href="#!"><i
                                                    class="icofont icofont-edit"></i>Edit</a>
                                            <a class="dropdown-item"
                                               href="#!"><i
                                                    class="icofont icofont-ui-delete"></i>Delete</a>
                                            <a class="dropdown-item"
                                               href="#!"><i
                                                    class="icofont icofont-eye-alt"></i>View</a>
                                            <a class="dropdown-item"
                                               href="#!"><i
                                                    class="icofont icofont-tasks-alt"></i>Project</a>
                                            <a class="dropdown-item"
                                               href="#!"><i
                                                    class="icofont icofont-ui-note"></i>Notes</a>
                                            <a class="dropdown-item"
                                               href="#!"><i
                                                    class="icofont icofont-eye-alt"></i>Activity</a>
                                            <a class="dropdown-item"
                                               href="#!"><i
                                                    class="icofont icofont-badge"></i>Schedule</a>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td>Garrett Winters</td>
                                    <td><a href="http://html.codedthemes.com/cdn-cgi/l/email-protection"
                                           class="__cf_email__"
                                           data-cfemail="0263606133303142656f636b6e2c616d6f">[email&#160;protected]</a>
                                    </td>
                                    <td>9989988988</td>
                                    <td><i class="fa fa-star"
                                           aria-hidden="true"></i>
                                    </td>
                                    <td class="dropdown">
                                        <button
                                            type="button"
                                            class="btn btn-primary dropdown-toggle"
                                            data-toggle="dropdown"
                                            aria-haspopup="true"
                                            aria-expanded="false"><i
                                                class="fa fa-cog"
                                                aria-hidden="true"></i></button>
                                        <div
                                            class="dropdown-menu dropdown-menu-right b-none contact-menu">
                                            <a class="dropdown-item"
                                               href="#!"><i
                                                    class="icofont icofont-edit"></i>Edit</a>
                                            <a class="dropdown-item"
                                               href="#!"><i
                                                    class="icofont icofont-ui-delete"></i>Delete</a>
                                            <a class="dropdown-item"
                                               href="#!"><i
                                                    class="icofont icofont-eye-alt"></i>View</a>
                                            <a class="dropdown-item"
                                               href="#!"><i
                                                    class="icofont icofont-tasks-alt"></i>Project</a>
                                            <a class="dropdown-item"
                                               href="#!"><i
                                                    class="icofont icofont-ui-note"></i>Notes</a>
                                            <a class="dropdown-item"
                                               href="#!"><i
                                                    class="icofont icofont-eye-alt"></i>Activity</a>
                                            <a class="dropdown-item"
                                               href="#!"><i
                                                    class="icofont icofont-badge"></i>Schedule</a>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td>Garrett Winters</td>
                                    <td><a href="http://html.codedthemes.com/cdn-cgi/l/email-protection"
                                           class="__cf_email__"
                                           data-cfemail="1b7a79782a29285b7c767a727735787476">[email&#160;protected]</a>
                                    </td>
                                    <td>9989988988</td>
                                    <td><i class="fa fa-star"
                                           aria-hidden="true"></i>
                                    </td>
                                    <td class="dropdown">
                                        <button
                                            type="button"
                                            class="btn btn-primary dropdown-toggle"
                                            data-toggle="dropdown"
                                            aria-haspopup="true"
                                            aria-expanded="false"><i
                                                class="fa fa-cog"
                                                aria-hidden="true"></i></button>
                                        <div
                                            class="dropdown-menu dropdown-menu-right b-none contact-menu">
                                            <a class="dropdown-item"
                                               href="#!"><i
                                                    class="icofont icofont-edit"></i>Edit</a>
                                            <a class="dropdown-item"
                                               href="#!"><i
                                                    class="icofont icofont-ui-delete"></i>Delete</a>
                                            <a class="dropdown-item"
                                               href="#!"><i
                                                    class="icofont icofont-eye-alt"></i>View</a>
                                            <a class="dropdown-item"
                                               href="#!"><i
                                                    class="icofont icofont-tasks-alt"></i>Project</a>
                                            <a class="dropdown-item"
                                               href="#!"><i
                                                    class="icofont icofont-ui-note"></i>Notes</a>
                                            <a class="dropdown-item"
                                               href="#!"><i
                                                    class="icofont icofont-eye-alt"></i>Activity</a>
                                            <a class="dropdown-item"
                                               href="#!"><i
                                                    class="icofont icofont-badge"></i>Schedule</a>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td>Garrett Winters</td>
                                    <td><a href="http://html.codedthemes.com/cdn-cgi/l/email-protection"
                                           class="__cf_email__"
                                           data-cfemail="97f6f5f4a6a5a4d7f0faf6fefbb9f4f8fa">[email&#160;protected]</a>
                                    </td>
                                    <td>9989988988</td>
                                    <td><i class="fa fa-star"
                                           aria-hidden="true"></i>
                                    </td>
                                    <td class="dropdown">
                                        <button
                                            type="button"
                                            class="btn btn-primary dropdown-toggle"
                                            data-toggle="dropdown"
                                            aria-haspopup="true"
                                            aria-expanded="false"><i
                                                class="fa fa-cog"
                                                aria-hidden="true"></i></button>
                                        <div
                                            class="dropdown-menu dropdown-menu-right b-none contact-menu">
                                            <a class="dropdown-item"
                                               href="#!"><i
                                                    class="icofont icofont-edit"></i>Edit</a>
                                            <a class="dropdown-item"
                                               href="#!"><i
                                                    class="icofont icofont-ui-delete"></i>Delete</a>
                                            <a class="dropdown-item"
                                               href="#!"><i
                                                    class="icofont icofont-eye-alt"></i>View</a>
                                            <a class="dropdown-item"
                                               href="#!"><i
                                                    class="icofont icofont-tasks-alt"></i>Project</a>
                                            <a class="dropdown-item"
                                               href="#!"><i
                                                    class="icofont icofont-ui-note"></i>Notes</a>
                                            <a class="dropdown-item"
                                               href="#!"><i
                                                    class="icofont icofont-eye-alt"></i>Activity</a>
                                            <a class="dropdown-item"
                                               href="#!"><i
                                                    class="icofont icofont-badge"></i>Schedule</a>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td>Garrett Winters</td>
                                    <td><a href="http://html.codedthemes.com/cdn-cgi/l/email-protection"
                                           class="__cf_email__"
                                           data-cfemail="81e0e3e2b0b3b2c1e6ece0e8edafe2eeec">[email&#160;protected]</a>
                                    </td>
                                    <td>9989988988</td>
                                    <td><i class="fa fa-star"
                                           aria-hidden="true"></i>
                                    </td>
                                    <td class="dropdown">
                                        <button
                                            type="button"
                                            class="btn btn-primary dropdown-toggle"
                                            data-toggle="dropdown"
                                            aria-haspopup="true"
                                            aria-expanded="false"><i
                                                class="fa fa-cog"
                                                aria-hidden="true"></i></button>
                                        <div
                                            class="dropdown-menu dropdown-menu-right b-none contact-menu">
                                            <a class="dropdown-item"
                                               href="#!"><i
                                                    class="icofont icofont-edit"></i>Edit</a>
                                            <a class="dropdown-item"
                                               href="#!"><i
                                                    class="icofont icofont-ui-delete"></i>Delete</a>
                                            <a class="dropdown-item"
                                               href="#!"><i
                                                    class="icofont icofont-eye-alt"></i>View</a>
                                            <a class="dropdown-item"
                                               href="#!"><i
                                                    class="icofont icofont-tasks-alt"></i>Project</a>
                                            <a class="dropdown-item"
                                               href="#!"><i
                                                    class="icofont icofont-ui-note"></i>Notes</a>
                                            <a class="dropdown-item"
                                               href="#!"><i
                                                    class="icofont icofont-eye-alt"></i>Activity</a>
                                            <a class="dropdown-item"
                                               href="#!"><i
                                                    class="icofont icofont-badge"></i>Schedule</a>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td>Garrett Winters</td>
                                    <td><a href="http://html.codedthemes.com/cdn-cgi/l/email-protection"
                                           class="__cf_email__"
                                           data-cfemail="d2b3b0b1e3e0e192b5bfb3bbbefcb1bdbf">[email&#160;protected]</a>
                                    </td>
                                    <td>9989988988</td>
                                    <td><i class="fa fa-star"
                                           aria-hidden="true"></i>
                                    </td>
                                    <td class="dropdown">
                                        <button
                                            type="button"
                                            class="btn btn-primary dropdown-toggle"
                                            data-toggle="dropdown"
                                            aria-haspopup="true"
                                            aria-expanded="false"><i
                                                class="fa fa-cog"
                                                aria-hidden="true"></i></button>
                                        <div
                                            class="dropdown-menu dropdown-menu-right b-none contact-menu">
                                            <a class="dropdown-item"
                                               href="#!"><i
                                                    class="icofont icofont-edit"></i>Edit</a>
                                            <a class="dropdown-item"
                                               href="#!"><i
                                                    class="icofont icofont-ui-delete"></i>Delete</a>
                                            <a class="dropdown-item"
                                               href="#!"><i
                                                    class="icofont icofont-eye-alt"></i>View</a>
                                            <a class="dropdown-item"
                                               href="#!"><i
                                                    class="icofont icofont-tasks-alt"></i>Project</a>
                                            <a class="dropdown-item"
                                               href="#!"><i
                                                    class="icofont icofont-ui-note"></i>Notes</a>
                                            <a class="dropdown-item"
                                               href="#!"><i
                                                    class="icofont icofont-eye-alt"></i>Activity</a>
                                            <a class="dropdown-item"
                                               href="#!"><i
                                                    class="icofont icofont-badge"></i>Schedule</a>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td>Garrett Winters</td>
                                    <td><a href="http://html.codedthemes.com/cdn-cgi/l/email-protection"
                                           class="__cf_email__"
                                           data-cfemail="4021222371727300272d21292c6e232f2d">[email&#160;protected]</a>
                                    </td>
                                    <td>9989988988</td>
                                    <td><i class="fa fa-star"
                                           aria-hidden="true"></i>
                                    </td>
                                    <td class="dropdown">
                                        <button
                                            type="button"
                                            class="btn btn-primary dropdown-toggle"
                                            data-toggle="dropdown"
                                            aria-haspopup="true"
                                            aria-expanded="false"><i
                                                class="fa fa-cog"
                                                aria-hidden="true"></i></button>
                                        <div
                                            class="dropdown-menu dropdown-menu-right b-none contact-menu">
                                            <a class="dropdown-item"
                                               href="#!"><i
                                                    class="icofont icofont-edit"></i>Edit</a>
                                            <a class="dropdown-item"
                                               href="#!"><i
                                                    class="icofont icofont-ui-delete"></i>Delete</a>
                                            <a class="dropdown-item"
                                               href="#!"><i
                                                    class="icofont icofont-eye-alt"></i>View</a>
                                            <a class="dropdown-item"
                                               href="#!"><i
                                                    class="icofont icofont-tasks-alt"></i>Project</a>
                                            <a class="dropdown-item"
                                               href="#!"><i
                                                    class="icofont icofont-ui-note"></i>Notes</a>
                                            <a class="dropdown-item"
                                               href="#!"><i
                                                    class="icofont icofont-eye-alt"></i>Activity</a>
                                            <a class="dropdown-item"
                                               href="#!"><i
                                                    class="icofont icofont-badge"></i>Schedule</a>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td>Garrett Winters</td>
                                    <td><a href="http://html.codedthemes.com/cdn-cgi/l/email-protection"
                                           class="__cf_email__"
                                           data-cfemail="d2b3b0b1e3e0e192b5bfb3bbbefcb1bdbf">[email&#160;protected]</a>
                                    </td>
                                    <td>9989988988</td>
                                    <td><i class="fa fa-star"
                                           aria-hidden="true"></i>
                                    </td>
                                    <td class="dropdown">
                                        <button
                                            type="button"
                                            class="btn btn-primary dropdown-toggle"
                                            data-toggle="dropdown"
                                            aria-haspopup="true"
                                            aria-expanded="false"><i
                                                class="fa fa-cog"
                                                aria-hidden="true"></i></button>
                                        <div
                                            class="dropdown-menu dropdown-menu-right b-none contact-menu">
                                            <a class="dropdown-item"
                                               href="#!"><i
                                                    class="icofont icofont-edit"></i>Edit</a>
                                            <a class="dropdown-item"
                                               href="#!"><i
                                                    class="icofont icofont-ui-delete"></i>Delete</a>
                                            <a class="dropdown-item"
                                               href="#!"><i
                                                    class="icofont icofont-eye-alt"></i>View</a>
                                            <a class="dropdown-item"
                                               href="#!"><i
                                                    class="icofont icofont-tasks-alt"></i>Project</a>
                                            <a class="dropdown-item"
                                               href="#!"><i
                                                    class="icofont icofont-ui-note"></i>Notes</a>
                                            <a class="dropdown-item"
                                               href="#!"><i
                                                    class="icofont icofont-eye-alt"></i>Activity</a>
                                            <a class="dropdown-item"
                                               href="#!"><i
                                                    class="icofont icofont-badge"></i>Schedule</a>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td>Garrett Winters</td>
                                    <td><a href="http://html.codedthemes.com/cdn-cgi/l/email-protection"
                                           class="__cf_email__"
                                           data-cfemail="aecfcccd9f9c9deec9c3cfc7c280cdc1c3">[email&#160;protected]</a>
                                    </td>
                                    <td>9989988988</td>
                                    <td><i class="fa fa-star"
                                           aria-hidden="true"></i>
                                    </td>
                                    <td class="dropdown">
                                        <button
                                            type="button"
                                            class="btn btn-primary dropdown-toggle"
                                            data-toggle="dropdown"
                                            aria-haspopup="true"
                                            aria-expanded="false"><i
                                                class="fa fa-cog"
                                                aria-hidden="true"></i></button>
                                        <div
                                            class="dropdown-menu dropdown-menu-right b-none contact-menu">
                                            <a class="dropdown-item"
                                               href="#!"><i
                                                    class="icofont icofont-edit"></i>Edit</a>
                                            <a class="dropdown-item"
                                               href="#!"><i
                                                    class="icofont icofont-ui-delete"></i>Delete</a>
                                            <a class="dropdown-item"
                                               href="#!"><i
                                                    class="icofont icofont-eye-alt"></i>View</a>
                                            <a class="dropdown-item"
                                               href="#!"><i
                                                    class="icofont icofont-tasks-alt"></i>Project</a>
                                            <a class="dropdown-item"
                                               href="#!"><i
                                                    class="icofont icofont-ui-note"></i>Notes</a>
                                            <a class="dropdown-item"
                                               href="#!"><i
                                                    class="icofont icofont-eye-alt"></i>Activity</a>
                                            <a class="dropdown-item"
                                               href="#!"><i
                                                    class="icofont icofont-badge"></i>Schedule</a>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td>Garrett Winters</td>
                                    <td><a href="http://html.codedthemes.com/cdn-cgi/l/email-protection"
                                           class="__cf_email__"
                                           data-cfemail="a5c4c7c6949796e5c2c8c4ccc98bc6cac8">[email&#160;protected]</a>
                                    </td>
                                    <td>9989988988</td>
                                    <td><i class="fa fa-star"
                                           aria-hidden="true"></i>
                                    </td>
                                    <td class="dropdown">
                                        <button
                                            type="button"
                                            class="btn btn-primary dropdown-toggle"
                                            data-toggle="dropdown"
                                            aria-haspopup="true"
                                            aria-expanded="false"><i
                                                class="fa fa-cog"
                                                aria-hidden="true"></i></button>
                                        <div
                                            class="dropdown-menu dropdown-menu-right b-none contact-menu">
                                            <a class="dropdown-item"
                                               href="#!"><i
                                                    class="icofont icofont-edit"></i>Edit</a>
                                            <a class="dropdown-item"
                                               href="#!"><i
                                                    class="icofont icofont-ui-delete"></i>Delete</a>
                                            <a class="dropdown-item"
                                               href="#!"><i
                                                    class="icofont icofont-eye-alt"></i>View</a>
                                            <a class="dropdown-item"
                                               href="#!"><i
                                                    class="icofont icofont-tasks-alt"></i>Project</a>
                                            <a class="dropdown-item"
                                               href="#!"><i
                                                    class="icofont icofont-ui-note"></i>Notes</a>
                                            <a class="dropdown-item"
                                               href="#!"><i
                                                    class="icofont icofont-eye-alt"></i>Activity</a>
                                            <a class="dropdown-item"
                                               href="#!"><i
                                                    class="icofont icofont-badge"></i>Schedule</a>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td>Garrett Winters</td>
                                    <td><a href="http://html.codedthemes.com/cdn-cgi/l/email-protection"
                                           class="__cf_email__"
                                           data-cfemail="bedfdcdd8f8c8dfed9d3dfd7d290ddd1d3">[email&#160;protected]</a>
                                    </td>
                                    <td>9989988988</td>
                                    <td><i class="fa fa-star"
                                           aria-hidden="true"></i>
                                    </td>
                                    <td class="dropdown">
                                        <button
                                            type="button"
                                            class="btn btn-primary dropdown-toggle"
                                            data-toggle="dropdown"
                                            aria-haspopup="true"
                                            aria-expanded="false"><i
                                                class="fa fa-cog"
                                                aria-hidden="true"></i></button>
                                        <div
                                            class="dropdown-menu dropdown-menu-right b-none contact-menu">
                                            <a class="dropdown-item"
                                               href="#!"><i
                                                    class="icofont icofont-edit"></i>Edit</a>
                                            <a class="dropdown-item"
                                               href="#!"><i
                                                    class="icofont icofont-ui-delete"></i>Delete</a>
                                            <a class="dropdown-item"
                                               href="#!"><i
                                                    class="icofont icofont-eye-alt"></i>View</a>
                                            <a class="dropdown-item"
                                               href="#!"><i
                                                    class="icofont icofont-tasks-alt"></i>Project</a>
                                            <a class="dropdown-item"
                                               href="#!"><i
                                                    class="icofont icofont-ui-note"></i>Notes</a>
                                            <a class="dropdown-item"
                                               href="#!"><i
                                                    class="icofont icofont-eye-alt"></i>Activity</a>
                                            <a class="dropdown-item"
                                               href="#!"><i
                                                    class="icofont icofont-badge"></i>Schedule</a>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td>Garrett Winters</td>
                                    <td><a href="http://html.codedthemes.com/cdn-cgi/l/email-protection"
                                           class="__cf_email__"
                                           data-cfemail="d5b4b7b6e4e7e695b2b8b4bcb9fbb6bab8">[email&#160;protected]</a>
                                    </td>
                                    <td>9989988988</td>
                                    <td><i class="fa fa-star"
                                           aria-hidden="true"></i>
                                    </td>
                                    <td class="dropdown">
                                        <button
                                            type="button"
                                            class="btn btn-primary dropdown-toggle"
                                            data-toggle="dropdown"
                                            aria-haspopup="true"
                                            aria-expanded="false"><i
                                                class="fa fa-cog"
                                                aria-hidden="true"></i></button>
                                        <div
                                            class="dropdown-menu dropdown-menu-right b-none contact-menu">
                                            <a class="dropdown-item"
                                               href="#!"><i
                                                    class="icofont icofont-edit"></i>Edit</a>
                                            <a class="dropdown-item"
                                               href="#!"><i
                                                    class="icofont icofont-ui-delete"></i>Delete</a>
                                            <a class="dropdown-item"
                                               href="#!"><i
                                                    class="icofont icofont-eye-alt"></i>View</a>
                                            <a class="dropdown-item"
                                               href="#!"><i
                                                    class="icofont icofont-tasks-alt"></i>Project</a>
                                            <a class="dropdown-item"
                                               href="#!"><i
                                                    class="icofont icofont-ui-note"></i>Notes</a>
                                            <a class="dropdown-item"
                                               href="#!"><i
                                                    class="icofont icofont-eye-alt"></i>Activity</a>
                                            <a class="dropdown-item"
                                               href="#!"><i
                                                    class="icofont icofont-badge"></i>Schedule</a>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td>Garrett Winters</td>
                                    <td><a href="http://html.codedthemes.com/cdn-cgi/l/email-protection"
                                           class="__cf_email__"
                                           data-cfemail="7a1b18194b48493a1d171b131654191517">[email&#160;protected]</a>
                                    </td>
                                    <td>9989988988</td>
                                    <td><i class="fa fa-star"
                                           aria-hidden="true"></i>
                                    </td>
                                    <td class="dropdown">
                                        <button
                                            type="button"
                                            class="btn btn-primary dropdown-toggle"
                                            data-toggle="dropdown"
                                            aria-haspopup="true"
                                            aria-expanded="false"><i
                                                class="fa fa-cog"
                                                aria-hidden="true"></i></button>
                                        <div
                                            class="dropdown-menu dropdown-menu-right b-none contact-menu">
                                            <a class="dropdown-item"
                                               href="#!"><i
                                                    class="icofont icofont-edit"></i>Edit</a>
                                            <a class="dropdown-item"
                                               href="#!"><i
                                                    class="icofont icofont-ui-delete"></i>Delete</a>
                                            <a class="dropdown-item"
                                               href="#!"><i
                                                    class="icofont icofont-eye-alt"></i>View</a>
                                            <a class="dropdown-item"
                                               href="#!"><i
                                                    class="icofont icofont-tasks-alt"></i>Project</a>
                                            <a class="dropdown-item"
                                               href="#!"><i
                                                    class="icofont icofont-ui-note"></i>Notes</a>
                                            <a class="dropdown-item"
                                               href="#!"><i
                                                    class="icofont icofont-eye-alt"></i>Activity</a>
                                            <a class="dropdown-item"
                                               href="#!"><i
                                                    class="icofont icofont-badge"></i>Schedule</a>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td>Garrett Winters</td>
                                    <td><a href="http://html.codedthemes.com/cdn-cgi/l/email-protection"
                                           class="__cf_email__"
                                           data-cfemail="3b5a59580a09087b5c565a525715585456">[email&#160;protected]</a>
                                    </td>
                                    <td>9989988988</td>
                                    <td><i class="fa fa-star"
                                           aria-hidden="true"></i>
                                    </td>
                                    <td class="dropdown">
                                        <button
                                            type="button"
                                            class="btn btn-primary dropdown-toggle"
                                            data-toggle="dropdown"
                                            aria-haspopup="true"
                                            aria-expanded="false"><i
                                                class="fa fa-cog"
                                                aria-hidden="true"></i></button>
                                        <div
                                            class="dropdown-menu dropdown-menu-right b-none contact-menu">
                                            <a class="dropdown-item"
                                               href="#!"><i
                                                    class="icofont icofont-edit"></i>Edit</a>
                                            <a class="dropdown-item"
                                               href="#!"><i
                                                    class="icofont icofont-ui-delete"></i>Delete</a>
                                            <a class="dropdown-item"
                                               href="#!"><i
                                                    class="icofont icofont-eye-alt"></i>View</a>
                                            <a class="dropdown-item"
                                               href="#!"><i
                                                    class="icofont icofont-tasks-alt"></i>Project</a>
                                            <a class="dropdown-item"
                                               href="#!"><i
                                                    class="icofont icofont-ui-note"></i>Notes</a>
                                            <a class="dropdown-item"
                                               href="#!"><i
                                                    class="icofont icofont-eye-alt"></i>Activity</a>
                                            <a class="dropdown-item"
                                               href="#!"><i
                                                    class="icofont icofont-badge"></i>Schedule</a>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td>Garrett Winters</td>
                                    <td><a href="http://html.codedthemes.com/cdn-cgi/l/email-protection"
                                           class="__cf_email__"
                                           data-cfemail="7716151446454437101a161e1b5914181a">[email&#160;protected]</a>
                                    </td>
                                    <td>9989988988</td>
                                    <td><i class="fa fa-star"
                                           aria-hidden="true"></i>
                                    </td>
                                    <td class="dropdown">
                                        <button
                                            type="button"
                                            class="btn btn-primary dropdown-toggle"
                                            data-toggle="dropdown"
                                            aria-haspopup="true"
                                            aria-expanded="false"><i
                                                class="fa fa-cog"
                                                aria-hidden="true"></i></button>
                                        <div
                                            class="dropdown-menu dropdown-menu-right b-none contact-menu">
                                            <a class="dropdown-item"
                                               href="#!"><i
                                                    class="icofont icofont-edit"></i>Edit</a>
                                            <a class="dropdown-item"
                                               href="#!"><i
                                                    class="icofont icofont-ui-delete"></i>Delete</a>
                                            <a class="dropdown-item"
                                               href="#!"><i
                                                    class="icofont icofont-eye-alt"></i>View</a>
                                            <a class="dropdown-item"
                                               href="#!"><i
                                                    class="icofont icofont-tasks-alt"></i>Project</a>
                                            <a class="dropdown-item"
                                               href="#!"><i
                                                    class="icofont icofont-ui-note"></i>Notes</a>
                                            <a class="dropdown-item"
                                               href="#!"><i
                                                    class="icofont icofont-eye-alt"></i>Activity</a>
                                            <a class="dropdown-item"
                                               href="#!"><i
                                                    class="icofont icofont-badge"></i>Schedule</a>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td>Garrett Winters</td>
                                    <td><a href="http://html.codedthemes.com/cdn-cgi/l/email-protection"
                                           class="__cf_email__"
                                           data-cfemail="9ffefdfcaeadacdff8f2fef6f3b1fcf0f2">[email&#160;protected]</a>
                                    </td>
                                    <td>9989988988</td>
                                    <td><i class="fa fa-star"
                                           aria-hidden="true"></i>
                                    </td>
                                    <td class="dropdown">
                                        <button
                                            type="button"
                                            class="btn btn-primary dropdown-toggle"
                                            data-toggle="dropdown"
                                            aria-haspopup="true"
                                            aria-expanded="false"><i
                                                class="fa fa-cog"
                                                aria-hidden="true"></i></button>
                                        <div
                                            class="dropdown-menu dropdown-menu-right b-none contact-menu">
                                            <a class="dropdown-item"
                                               href="#!"><i
                                                    class="icofont icofont-edit"></i>Edit</a>
                                            <a class="dropdown-item"
                                               href="#!"><i
                                                    class="icofont icofont-ui-delete"></i>Delete</a>
                                            <a class="dropdown-item"
                                               href="#!"><i
                                                    class="icofont icofont-eye-alt"></i>View</a>
                                            <a class="dropdown-item"
                                               href="#!"><i
                                                    class="icofont icofont-tasks-alt"></i>Project</a>
                                            <a class="dropdown-item"
                                               href="#!"><i
                                                    class="icofont icofont-ui-note"></i>Notes</a>
                                            <a class="dropdown-item"
                                               href="#!"><i
                                                    class="icofont icofont-eye-alt"></i>Activity</a>
                                            <a class="dropdown-item"
                                               href="#!"><i
                                                    class="icofont icofont-badge"></i>Schedule</a>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td>Garrett Winters</td>
                                    <td><a href="http://html.codedthemes.com/cdn-cgi/l/email-protection"
                                           class="__cf_email__"
                                           data-cfemail="d6b7b4b5e7e4e596b1bbb7bfbaf8b5b9bb">[email&#160;protected]</a>
                                    </td>
                                    <td>9989988988</td>
                                    <td><i class="fa fa-star"
                                           aria-hidden="true"></i>
                                    </td>
                                    <td class="dropdown">
                                        <button
                                            type="button"
                                            class="btn btn-primary dropdown-toggle"
                                            data-toggle="dropdown"
                                            aria-haspopup="true"
                                            aria-expanded="false"><i
                                                class="fa fa-cog"
                                                aria-hidden="true"></i></button>
                                        <div
                                            class="dropdown-menu dropdown-menu-right b-none contact-menu">
                                            <a class="dropdown-item"
                                               href="#!"><i
                                                    class="icofont icofont-edit"></i>Edit</a>
                                            <a class="dropdown-item"
                                               href="#!"><i
                                                    class="icofont icofont-ui-delete"></i>Delete</a>
                                            <a class="dropdown-item"
                                               href="#!"><i
                                                    class="icofont icofont-eye-alt"></i>View</a>
                                            <a class="dropdown-item"
                                               href="#!"><i
                                                    class="icofont icofont-tasks-alt"></i>Project</a>
                                            <a class="dropdown-item"
                                               href="#!"><i
                                                    class="icofont icofont-ui-note"></i>Notes</a>
                                            <a class="dropdown-item"
                                               href="#!"><i
                                                    class="icofont icofont-eye-alt"></i>Activity</a>
                                            <a class="dropdown-item"
                                               href="#!"><i
                                                    class="icofont icofont-badge"></i>Schedule</a>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td>Garrett Winters</td>
                                    <td><a href="http://html.codedthemes.com/cdn-cgi/l/email-protection"
                                           class="__cf_email__"
                                           data-cfemail="2f4e4d4c1e1d1c6f48424e4643014c4042">[email&#160;protected]</a>
                                    </td>
                                    <td>9989988988</td>
                                    <td><i class="fa fa-star"
                                           aria-hidden="true"></i>
                                    </td>
                                    <td class="dropdown">
                                        <button
                                            type="button"
                                            class="btn btn-primary dropdown-toggle"
                                            data-toggle="dropdown"
                                            aria-haspopup="true"
                                            aria-expanded="false"><i
                                                class="fa fa-cog"
                                                aria-hidden="true"></i></button>
                                        <div
                                            class="dropdown-menu dropdown-menu-right b-none contact-menu">
                                            <a class="dropdown-item"
                                               href="#!"><i
                                                    class="icofont icofont-edit"></i>Edit</a>
                                            <a class="dropdown-item"
                                               href="#!"><i
                                                    class="icofont icofont-ui-delete"></i>Delete</a>
                                            <a class="dropdown-item"
                                               href="#!"><i
                                                    class="icofont icofont-eye-alt"></i>View</a>
                                            <a class="dropdown-item"
                                               href="#!"><i
                                                    class="icofont icofont-tasks-alt"></i>Project</a>
                                            <a class="dropdown-item"
                                               href="#!"><i
                                                    class="icofont icofont-ui-note"></i>Notes</a>
                                            <a class="dropdown-item"
                                               href="#!"><i
                                                    class="icofont icofont-eye-alt"></i>Activity</a>
                                            <a class="dropdown-item"
                                               href="#!"><i
                                                    class="icofont icofont-badge"></i>Schedule</a>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td>Garrett Winters</td>
                                    <td><a href="http://html.codedthemes.com/cdn-cgi/l/email-protection"
                                           class="__cf_email__"
                                           data-cfemail="35545756040706755258545c591b565a58">[email&#160;protected]</a>
                                    </td>
                                    <td>9989988988</td>
                                    <td><i class="fa fa-star"
                                           aria-hidden="true"></i>
                                    </td>
                                    <td class="dropdown">
                                        <button
                                            type="button"
                                            class="btn btn-primary dropdown-toggle"
                                            data-toggle="dropdown"
                                            aria-haspopup="true"
                                            aria-expanded="false"><i
                                                class="fa fa-cog"
                                                aria-hidden="true"></i></button>
                                        <div
                                            class="dropdown-menu dropdown-menu-right b-none contact-menu">
                                            <a class="dropdown-item"
                                               href="#!"><i
                                                    class="icofont icofont-edit"></i>Edit</a>
                                            <a class="dropdown-item"
                                               href="#!"><i
                                                    class="icofont icofont-ui-delete"></i>Delete</a>
                                            <a class="dropdown-item"
                                               href="#!"><i
                                                    class="icofont icofont-eye-alt"></i>View</a>
                                            <a class="dropdown-item"
                                               href="#!"><i
                                                    class="icofont icofont-tasks-alt"></i>Project</a>
                                            <a class="dropdown-item"
                                               href="#!"><i
                                                    class="icofont icofont-ui-note"></i>Notes</a>
                                            <a class="dropdown-item"
                                               href="#!"><i
                                                    class="icofont icofont-eye-alt"></i>Activity</a>
                                            <a class="dropdown-item"
                                               href="#!"><i
                                                    class="icofont icofont-badge"></i>Schedule</a>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td>Garrett Winters</td>
                                    <td><a href="http://html.codedthemes.com/cdn-cgi/l/email-protection"
                                           class="__cf_email__"
                                           data-cfemail="3a5b58590b08097a5d575b535614595557">[email&#160;protected]</a>
                                    </td>
                                    <td>9989988988</td>
                                    <td><i class="fa fa-star"
                                           aria-hidden="true"></i>
                                    </td>
                                    <td class="dropdown">
                                        <button
                                            type="button"
                                            class="btn btn-primary dropdown-toggle"
                                            data-toggle="dropdown"
                                            aria-haspopup="true"
                                            aria-expanded="false"><i
                                                class="fa fa-cog"
                                                aria-hidden="true"></i></button>
                                        <div
                                            class="dropdown-menu dropdown-menu-right b-none contact-menu">
                                            <a class="dropdown-item"
                                               href="#!"><i
                                                    class="icofont icofont-edit"></i>Edit</a>
                                            <a class="dropdown-item"
                                               href="#!"><i
                                                    class="icofont icofont-ui-delete"></i>Delete</a>
                                            <a class="dropdown-item"
                                               href="#!"><i
                                                    class="icofont icofont-eye-alt"></i>View</a>
                                            <a class="dropdown-item"
                                               href="#!"><i
                                                    class="icofont icofont-tasks-alt"></i>Project</a>
                                            <a class="dropdown-item"
                                               href="#!"><i
                                                    class="icofont icofont-ui-note"></i>Notes</a>
                                            <a class="dropdown-item"
                                               href="#!"><i
                                                    class="icofont icofont-eye-alt"></i>Activity</a>
                                            <a class="dropdown-item"
                                               href="#!"><i
                                                    class="icofont icofont-badge"></i>Schedule</a>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td>Garrett Winters</td>
                                    <td><a href="http://html.codedthemes.com/cdn-cgi/l/email-protection"
                                           class="__cf_email__"
                                           data-cfemail="f6979495c7c4c5b6919b979f9ad895999b">[email&#160;protected]</a>
                                    </td>
                                    <td>9989988988</td>
                                    <td><i class="fa fa-star"
                                           aria-hidden="true"></i>
                                    </td>
                                    <td class="dropdown">
                                        <button
                                            type="button"
                                            class="btn btn-primary dropdown-toggle"
                                            data-toggle="dropdown"
                                            aria-haspopup="true"
                                            aria-expanded="false"><i
                                                class="fa fa-cog"
                                                aria-hidden="true"></i></button>
                                        <div
                                            class="dropdown-menu dropdown-menu-right b-none contact-menu">
                                            <a class="dropdown-item"
                                               href="#!"><i
                                                    class="icofont icofont-edit"></i>Edit</a>
                                            <a class="dropdown-item"
                                               href="#!"><i
                                                    class="icofont icofont-ui-delete"></i>Delete</a>
                                            <a class="dropdown-item"
                                               href="#!"><i
                                                    class="icofont icofont-eye-alt"></i>View</a>
                                            <a class="dropdown-item"
                                               href="#!"><i
                                                    class="icofont icofont-tasks-alt"></i>Project</a>
                                            <a class="dropdown-item"
                                               href="#!"><i
                                                    class="icofont icofont-ui-note"></i>Notes</a>
                                            <a class="dropdown-item"
                                               href="#!"><i
                                                    class="icofont icofont-eye-alt"></i>Activity</a>
                                            <a class="dropdown-item"
                                               href="#!"><i
                                                    class="icofont icofont-badge"></i>Schedule</a>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td>Garrett Winters</td>
                                    <td><a href="http://html.codedthemes.com/cdn-cgi/l/email-protection"
                                           class="__cf_email__"
                                           data-cfemail="6a0b08095b58592a0d070b030644090507">[email&#160;protected]</a>
                                    </td>
                                    <td>9989988988</td>
                                    <td><i class="fa fa-star"
                                           aria-hidden="true"></i>
                                    </td>
                                    <td class="dropdown">
                                        <button
                                            type="button"
                                            class="btn btn-primary dropdown-toggle"
                                            data-toggle="dropdown"
                                            aria-haspopup="true"
                                            aria-expanded="false"><i
                                                class="fa fa-cog"
                                                aria-hidden="true"></i></button>
                                        <div
                                            class="dropdown-menu dropdown-menu-right b-none contact-menu">
                                            <a class="dropdown-item"
                                               href="#!"><i
                                                    class="icofont icofont-edit"></i>Edit</a>
                                            <a class="dropdown-item"
                                               href="#!"><i
                                                    class="icofont icofont-ui-delete"></i>Delete</a>
                                            <a class="dropdown-item"
                                               href="#!"><i
                                                    class="icofont icofont-eye-alt"></i>View</a>
                                            <a class="dropdown-item"
                                               href="#!"><i
                                                    class="icofont icofont-tasks-alt"></i>Project</a>
                                            <a class="dropdown-item"
                                               href="#!"><i
                                                    class="icofont icofont-ui-note"></i>Notes</a>
                                            <a class="dropdown-item"
                                               href="#!"><i
                                                    class="icofont icofont-eye-alt"></i>Activity</a>
                                            <a class="dropdown-item"
                                               href="#!"><i
                                                    class="icofont icofont-badge"></i>Schedule</a>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td>Garrett Winters</td>
                                    <td><a href="http://html.codedthemes.com/cdn-cgi/l/email-protection"
                                           class="__cf_email__"
                                           data-cfemail="7d1c1f1e4c4f4e3d1a101c1411531e1210">[email&#160;protected]</a>
                                    </td>
                                    <td>9989988988</td>
                                    <td><i class="fa fa-star"
                                           aria-hidden="true"></i>
                                    </td>
                                    <td class="dropdown">
                                        <button
                                            type="button"
                                            class="btn btn-primary dropdown-toggle"
                                            data-toggle="dropdown"
                                            aria-haspopup="true"
                                            aria-expanded="false"><i
                                                class="fa fa-cog"
                                                aria-hidden="true"></i></button>
                                        <div
                                            class="dropdown-menu dropdown-menu-right b-none contact-menu">
                                            <a class="dropdown-item"
                                               href="#!"><i
                                                    class="icofont icofont-edit"></i>Edit</a>
                                            <a class="dropdown-item"
                                               href="#!"><i
                                                    class="icofont icofont-ui-delete"></i>Delete</a>
                                            <a class="dropdown-item"
                                               href="#!"><i
                                                    class="icofont icofont-eye-alt"></i>View</a>
                                            <a class="dropdown-item"
                                               href="#!"><i
                                                    class="icofont icofont-tasks-alt"></i>Project</a>
                                            <a class="dropdown-item"
                                               href="#!"><i
                                                    class="icofont icofont-ui-note"></i>Notes</a>
                                            <a class="dropdown-item"
                                               href="#!"><i
                                                    class="icofont icofont-eye-alt"></i>Activity</a>
                                            <a class="dropdown-item"
                                               href="#!"><i
                                                    class="icofont icofont-badge"></i>Schedule</a>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td>Garrett Winters</td>
                                    <td><a href="http://html.codedthemes.com/cdn-cgi/l/email-protection"
                                           class="__cf_email__"
                                           data-cfemail="e6878485d7d4d5a6818b878f8ac885898b">[email&#160;protected]</a>
                                    </td>
                                    <td>9989988988</td>
                                    <td><i class="fa fa-star"
                                           aria-hidden="true"></i>
                                    </td>
                                    <td class="dropdown">
                                        <button
                                            type="button"
                                            class="btn btn-primary dropdown-toggle"
                                            data-toggle="dropdown"
                                            aria-haspopup="true"
                                            aria-expanded="false"><i
                                                class="fa fa-cog"
                                                aria-hidden="true"></i></button>
                                        <div
                                            class="dropdown-menu dropdown-menu-right b-none contact-menu">
                                            <a class="dropdown-item"
                                               href="#!"><i
                                                    class="icofont icofont-edit"></i>Edit</a>
                                            <a class="dropdown-item"
                                               href="#!"><i
                                                    class="icofont icofont-ui-delete"></i>Delete</a>
                                            <a class="dropdown-item"
                                               href="#!"><i
                                                    class="icofont icofont-eye-alt"></i>View</a>
                                            <a class="dropdown-item"
                                               href="#!"><i
                                                    class="icofont icofont-tasks-alt"></i>Project</a>
                                            <a class="dropdown-item"
                                               href="#!"><i
                                                    class="icofont icofont-ui-note"></i>Notes</a>
                                            <a class="dropdown-item"
                                               href="#!"><i
                                                    class="icofont icofont-eye-alt"></i>Activity</a>
                                            <a class="dropdown-item"
                                               href="#!"><i
                                                    class="icofont icofont-badge"></i>Schedule</a>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td>Garrett Winters</td>
                                    <td><a href="http://html.codedthemes.com/cdn-cgi/l/email-protection"
                                           class="__cf_email__"
                                           data-cfemail="1a7b78792b28295a7d777b737634797577">[email&#160;protected]</a>
                                    </td>
                                    <td>9989988988</td>
                                    <td><i class="fa fa-star"
                                           aria-hidden="true"></i>
                                    </td>
                                    <td class="dropdown">
                                        <button
                                            type="button"
                                            class="btn btn-primary dropdown-toggle"
                                            data-toggle="dropdown"
                                            aria-haspopup="true"
                                            aria-expanded="false"><i
                                                class="fa fa-cog"
                                                aria-hidden="true"></i></button>
                                        <div
                                            class="dropdown-menu dropdown-menu-right b-none contact-menu">
                                            <a class="dropdown-item"
                                               href="#!"><i
                                                    class="icofont icofont-edit"></i>Edit</a>
                                            <a class="dropdown-item"
                                               href="#!"><i
                                                    class="icofont icofont-ui-delete"></i>Delete</a>
                                            <a class="dropdown-item"
                                               href="#!"><i
                                                    class="icofont icofont-eye-alt"></i>View</a>
                                            <a class="dropdown-item"
                                               href="#!"><i
                                                    class="icofont icofont-tasks-alt"></i>Project</a>
                                            <a class="dropdown-item"
                                               href="#!"><i
                                                    class="icofont icofont-ui-note"></i>Notes</a>
                                            <a class="dropdown-item"
                                               href="#!"><i
                                                    class="icofont icofont-eye-alt"></i>Activity</a>
                                            <a class="dropdown-item"
                                               href="#!"><i
                                                    class="icofont icofont-badge"></i>Schedule</a>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td>Garrett Winters</td>
                                    <td><a href="http://html.codedthemes.com/cdn-cgi/l/email-protection"
                                           class="__cf_email__"
                                           data-cfemail="9dfcfffeacafaeddfaf0fcf4f1b3fef2f0">[email&#160;protected]</a>
                                    </td>
                                    <td>9989988988</td>
                                    <td><i class="fa fa-star"
                                           aria-hidden="true"></i>
                                    </td>
                                    <td class="dropdown">
                                        <button
                                            type="button"
                                            class="btn btn-primary dropdown-toggle"
                                            data-toggle="dropdown"
                                            aria-haspopup="true"
                                            aria-expanded="false"><i
                                                class="fa fa-cog"
                                                aria-hidden="true"></i></button>
                                        <div
                                            class="dropdown-menu dropdown-menu-right b-none contact-menu">
                                            <a class="dropdown-item"
                                               href="#!"><i
                                                    class="icofont icofont-edit"></i>Edit</a>
                                            <a class="dropdown-item"
                                               href="#!"><i
                                                    class="icofont icofont-ui-delete"></i>Delete</a>
                                            <a class="dropdown-item"
                                               href="#!"><i
                                                    class="icofont icofont-eye-alt"></i>View</a>
                                            <a class="dropdown-item"
                                               href="#!"><i
                                                    class="icofont icofont-tasks-alt"></i>Project</a>
                                            <a class="dropdown-item"
                                               href="#!"><i
                                                    class="icofont icofont-ui-note"></i>Notes</a>
                                            <a class="dropdown-item"
                                               href="#!"><i
                                                    class="icofont icofont-eye-alt"></i>Activity</a>
                                            <a class="dropdown-item"
                                               href="#!"><i
                                                    class="icofont icofont-badge"></i>Schedule</a>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td>Garrett Winters</td>
                                    <td><a href="http://html.codedthemes.com/cdn-cgi/l/email-protection"
                                           class="__cf_email__"
                                           data-cfemail="d0b1b2b3e1e2e390b7bdb1b9bcfeb3bfbd">[email&#160;protected]</a>
                                    </td>
                                    <td>9989988988</td>
                                    <td><i class="fa fa-star"
                                           aria-hidden="true"></i>
                                    </td>
                                    <td class="dropdown">
                                        <button
                                            type="button"
                                            class="btn btn-primary dropdown-toggle"
                                            data-toggle="dropdown"
                                            aria-haspopup="true"
                                            aria-expanded="false"><i
                                                class="fa fa-cog"
                                                aria-hidden="true"></i></button>
                                        <div
                                            class="dropdown-menu dropdown-menu-right b-none contact-menu">
                                            <a class="dropdown-item"
                                               href="#!"><i
                                                    class="icofont icofont-edit"></i>Edit</a>
                                            <a class="dropdown-item"
                                               href="#!"><i
                                                    class="icofont icofont-ui-delete"></i>Delete</a>
                                            <a class="dropdown-item"
                                               href="#!"><i
                                                    class="icofont icofont-eye-alt"></i>View</a>
                                            <a class="dropdown-item"
                                               href="#!"><i
                                                    class="icofont icofont-tasks-alt"></i>Project</a>
                                            <a class="dropdown-item"
                                               href="#!"><i
                                                    class="icofont icofont-ui-note"></i>Notes</a>
                                            <a class="dropdown-item"
                                               href="#!"><i
                                                    class="icofont icofont-eye-alt"></i>Activity</a>
                                            <a class="dropdown-item"
                                               href="#!"><i
                                                    class="icofont icofont-badge"></i>Schedule</a>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td>Garrett Winters</td>
                                    <td><a href="http://html.codedthemes.com/cdn-cgi/l/email-protection"
                                           class="__cf_email__"
                                           data-cfemail="44252627757677042329252d286a272b29">[email&#160;protected]</a>
                                    </td>
                                    <td>9989988988</td>
                                    <td><i class="fa fa-star"
                                           aria-hidden="true"></i>
                                    </td>
                                    <td class="dropdown">
                                        <button
                                            type="button"
                                            class="btn btn-primary dropdown-toggle"
                                            data-toggle="dropdown"
                                            aria-haspopup="true"
                                            aria-expanded="false"><i
                                                class="fa fa-cog"
                                                aria-hidden="true"></i></button>
                                        <div
                                            class="dropdown-menu dropdown-menu-right b-none contact-menu">
                                            <a class="dropdown-item"
                                               href="#!"><i
                                                    class="icofont icofont-edit"></i>Edit</a>
                                            <a class="dropdown-item"
                                               href="#!"><i
                                                    class="icofont icofont-ui-delete"></i>Delete</a>
                                            <a class="dropdown-item"
                                               href="#!"><i
                                                    class="icofont icofont-eye-alt"></i>View</a>
                                            <a class="dropdown-item"
                                               href="#!"><i
                                                    class="icofont icofont-tasks-alt"></i>Project</a>
                                            <a class="dropdown-item"
                                               href="#!"><i
                                                    class="icofont icofont-ui-note"></i>Notes</a>
                                            <a class="dropdown-item"
                                               href="#!"><i
                                                    class="icofont icofont-eye-alt"></i>Activity</a>
                                            <a class="dropdown-item"
                                               href="#!"><i
                                                    class="icofont icofont-badge"></i>Schedule</a>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td>Garrett Winters</td>
                                    <td><a href="http://html.codedthemes.com/cdn-cgi/l/email-protection"
                                           class="__cf_email__"
                                           data-cfemail="7213101143404132151f131b1e5c111d1f">[email&#160;protected]</a>
                                    </td>
                                    <td>9989988988</td>
                                    <td><i class="fa fa-star"
                                           aria-hidden="true"></i>
                                    </td>
                                    <td class="dropdown">
                                        <button
                                            type="button"
                                            class="btn btn-primary dropdown-toggle"
                                            data-toggle="dropdown"
                                            aria-haspopup="true"
                                            aria-expanded="false"><i
                                                class="fa fa-cog"
                                                aria-hidden="true"></i></button>
                                        <div
                                            class="dropdown-menu dropdown-menu-right b-none contact-menu">
                                            <a class="dropdown-item"
                                               href="#!"><i
                                                    class="icofont icofont-edit"></i>Edit</a>
                                            <a class="dropdown-item"
                                               href="#!"><i
                                                    class="icofont icofont-ui-delete"></i>Delete</a>
                                            <a class="dropdown-item"
                                               href="#!"><i
                                                    class="icofont icofont-eye-alt"></i>View</a>
                                            <a class="dropdown-item"
                                               href="#!"><i
                                                    class="icofont icofont-tasks-alt"></i>Project</a>
                                            <a class="dropdown-item"
                                               href="#!"><i
                                                    class="icofont icofont-ui-note"></i>Notes</a>
                                            <a class="dropdown-item"
                                               href="#!"><i
                                                    class="icofont icofont-eye"></i>Activity</a>
                                            <a class="dropdown-item"
                                               href="#!"><i
                                                    class="icofont icofont-badge"></i>Schedule</a>
                                        </div>
                                    </td>
                                </tr>
                                </tbody>
                                <tfoot>
                                <tr>
                                    <th>Name</th>
                                    <th>Email</th>
                                    <th>Mobileno.</th>
                                    <th>Favourite</th>
                                    <th>Action</th>
                                </tr>
                                </tfoot>
                            </table>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>
