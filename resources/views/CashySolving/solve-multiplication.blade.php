@extends('layout.app')

@section('content')

    <div class="main-body">
        <div class="page-wrapper">

            <div class="page-body">

                <div class="row">
                    <div class="col-md-12 col-xl-4">
                        <div class="card widget-statstic-card borderless-card">
                            <div class="card-header">
                                <div class="card-header-left">
                                    <h5>Points Earned</h5>
                                    <p class="p-t-10 m-b-0 text-muted">Today</p>
                                </div>
                            </div>
                            <div class="card-block">
                                <i class="fa fa-bank st-icon bg-primary"></i>
                                <div class="text-left">
                                    <h3 class="d-inline-block" id="points">
                                        <div class="preloader3 loader-block analyticsLoader" style="height:0; align-items: center; display: block">
                                            <div class="circ1"></div>
                                            <div class="circ2"></div>
                                            <div class="circ3"></div>
                                            <div class="circ4"></div>
                                        </div>
                                    </h3>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6 col-xl-4">
                        <div class="card widget-statstic-card borderless-card">
                            <div class="card-header">
                                <div class="card-header-left">
                                    <h5>Correct Answers</h5>
                                    <p class="p-t-10 m-b-0 text-muted">Today</p>

                                </div>
                            </div>
                            <div class="card-block">
                                <i class="fa fa-users st-icon bg-warning txt-lite-color"></i>
                                <div class="text-left">
                                    <h3 class="d-inline-block" id="correct">
                                        <div class="preloader3 loader-block analyticsLoader" style="height:0; align-items: center; display: block">
                                            <div class="circ1"></div>
                                            <div class="circ2"></div>
                                            <div class="circ3"></div>
                                            <div class="circ4"></div>
                                        </div>
                                    </h3>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6 col-xl-4">
                        <div class="card widget-statstic-card borderless-card">
                            <div class="card-header">
                                <div class="card-header-left">
                                    <h5>Incorrect Answers</h5>
                                    <p class="p-t-10 m-b-0 text-muted">Today</p>
                                </div>
                            </div>
                            <div class="card-block">
                                <i class="fa fa-line-chart st-icon bg-success"></i>
                                <div class="text-left">
                                    <h3 class="d-inline-block" id="incorrect">
                                        <div class="preloader3 loader-block analyticsLoader" style="height:0; align-items: center; display: block">
                                            <div class="circ1"></div>
                                            <div class="circ2"></div>
                                            <div class="circ3"></div>
                                            <div class="circ4"></div>
                                        </div>
                                    </h3>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-3">

                    </div>
                    <div class="col-md-6">
                        <div class="card">
                            <div class="card-header">
                                <h5>
                                    Solve using Multiplication
                                </h5>
                            </div>
                            <div class="card-block">
                                <form action="" method="POST" id="gameForm" data-operation="Multiplication">
                                    <div class="preloader3 loader-block" style="height:0; align-items: center; display: block" id="solvingLoader">
                                        <div class="circ1"></div>
                                        <div class="circ2"></div>
                                        <div class="circ3"></div>
                                        <div class="circ4"></div>
                                    </div>
                                    <div id="game" class="game-banner" style="display: none;">
                                        <div class="form-group" style="margin-bottom: 0">
                                            <span class="problem"><span id="num1"></span> * <span id="num2"></span> </span>
                                            =
                                            <input class="answer" name="answer" />
                                        </div>
                                        <button type="submit" class="btn btn-primary btn-block">Solve</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3">

                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade success" id="success" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-body">
                    <h5> Yey! That's correct!</h5>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="error" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-body">
                    <h5>Sorry, wrong answer</h5>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="calculating" tabindex="-1" role="dialog" data-keyboard="false" data-backdrop="static">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-body">
                    Checking your answer...
                </div>
            </div>
        </div>
    </div>

@endsection

@section('scripts')
    <script src="{{ asset('backend') }}/script/solving.js"></script>

@endsection
