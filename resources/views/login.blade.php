<!DOCTYPE html>
<html lang="en">

<meta http-equiv="content-type" content="text/html;charset=UTF-8" />

<head>
    <title>{{ env('APP_NAME') }}</title>


    <!--[if lt IE 10]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="description"
          content="{{ __('en.meta-description') }}" />
    <meta name="keywords"
          content="{{ __('en.meta-keywords') }}" />
    <meta name="author" content="{{ __('en.meta-author') }}" />
    <meta name="csrf-token" content="{{ csrf_token() }}">


    <link rel="icon" href="http://html.codedthemes.com/gradient-able/files/assets/images/favicon.ico"
          type="image/x-icon">
    <link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600" rel="stylesheet">

    <link rel="stylesheet" type="text/css" href="{{ asset('backend') }}/bower_components/bootstrap/css/bootstrap.min.css">

    <link rel="stylesheet" type="text/css" href="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.css">

    <link rel="stylesheet" type="text/css" href="{{ asset('backend') }}/assets/css/style.css">

    <link rel="stylesheet" type="text/css" href="{{ asset('backend') }}/assets/css/jquery.mCustomScrollbar.css">

</head>

<body class="fix-menu">

<div class="theme-loader">
    <div class="loader-track">
        <div class="loader-bar"></div>
    </div>
</div>

<section class="login p-fixed d-flex text-center bg-primary common-img-bg">

    <div class="container">
        <div class="row">
            <div class="col-sm-12">

                <div class="login-card card-block auth-body mr-auto ml-auto">
                    <form class="md-float-material" method="POST" id="login_form">

                        <div class="text-center">
{{--                            <img src="{{ asset('backend') }}/assets/images/logo.png" alt="logo.png">--}}
                        </div>
                        <div class="auth-box">
                            <div class="row m-b-20">
                                <div class="col-md-12">
                                    <h3 class="text-left txt-primary">Sign In</h3>
                                </div>
                            </div>
                            <hr />
                            <div class="input-group">
                                <input type="text" name="username" value="{{ old('username') }}" class="form-control" placeholder="Your Username">
                                <span class="md-line"></span>
                            </div>
                            <div class="input-group">
                                <input type="password" name="password" class="form-control" placeholder="Password">
                                <span class="md-line"></span>
                            </div>
                            <div class="row m-t-25 text-left">
                                <div class="col-12">
                                    <div class="checkbox-fade fade-in-primary d-">
                                        <label for="checkbox">
                                            <input type="checkbox" name="remember" value="1" id="checkbox">
                                            <span class="cr"><i
                                                    class="cr-icon icofont icofont-ui-check txt-primary"></i></span>
                                            <span class="text-inverse">Remember me</span>
                                        </label>
                                    </div>
                                    <div class="forgot-phone text-right f-right">
                                        <a href="#" class="text-right f-w-600 text-inverse">
                                            Forgot Password?</a>
                                    </div>
                                </div>
                            </div>
                            <div class="row m-t-30">
                                <div class="col-md-12">
                                    <button type="submit"
                                            class="btn btn-primary btn-md btn-block waves-effect text-center m-b-20">Sign
                                        in</button>
                                </div>
                            </div>
                            <hr />
                            <div class="row">
                                <div class="col-md-12 text-black">
                                    <div class="form-register">
                                        <span style="color: black;">
                                            Don't have an account?
                                        </span>
                                    </div>

                                    <a href="{{ url('register') }}" class="text-right f-w-600 text-amazon"><u>Sign up to Cashy Now</u></a>
                                </div>
                            </div>

                        </div>
                    </form>

                </div>

            </div>

        </div>

    </div>

</section>

<script src="{{ asset('backend') }}/bower_components/jquery/js/jquery.min.js"></script>
<script src="{{ asset('backend') }}/bower_components/jquery-ui/js/jquery-ui.min.js"></script>
<script src="{{ asset('backend') }}/bower_components/popper.js/js/popper.min.js"></script>
<script src="{{ asset('backend') }}/bower_components/bootstrap/js/bootstrap.min.js"></script>

<script src="{{ asset('backend') }}/bower_components/jquery-slimscroll/js/jquery.slimscroll.js"></script>

<script src="{{ asset('backend') }}/bower_components/modernizr/js/modernizr.js"></script>
<script src="{{ asset('backend') }}/bower_components/modernizr/js/css-scrollbars.js"></script>

<script src="{{ asset('backend') }}/bower_components/i18next/js/i18next.min.js"></script>
<script src="{{ asset('backend') }}/bower_components/i18next-xhr-backend/js/i18nextXHRbackend.min.js"></script>
<script
    src="{{ asset('backend') }}/bower_components/i18next-browser-languagedetector/js/i18nextBrowserLanguageDetector.min.js"></script>
<script src="{{ asset('backend') }}/bower_components/jquery-i18next/js/jquery-i18next.min.js"></script>
<script src="{{ asset('backend') }}/assets/js/common-pages.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.js"></script>
<script src="{{ asset('backend') }}/script/login.js"></script>
</body>

</html>
