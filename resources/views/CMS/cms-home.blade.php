@extends('layout.app')

@section('content')

    <div class="main-body">
        <div class="page-wrapper">

            <div class="page-header card">
                {!! $breadcrumbs !!}
            </div>

            <div class="page-body">
                <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="card-header">
                                <h5>Home Header CMS</h5>
                            </div>
                            <div class="card-block">
                                <form action="/homeCMSPost" method="POST">
                                    @csrf
                                    <div class="form-group row">
                                        <label class="col-sm-2 col-form-label">Title</label>
                                        <div class="col-sm-10">
                                            <input type="text" class="form-control" name="header_title_1" value="{{ $cms->header_title_1 }}">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-sm-2 col-form-label">Heading</label>
                                        <div class="col-sm-10">
                                            <textarea name="heading" class="summernote">{!! $cms->heading !!}</textarea>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-sm-2 col-form-label">Content</label>
                                        <div class="col-sm-10">
                                            <textarea name="content" class="summernote">{!! $cms->content !!}</textarea>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <button type="submit" class="btn btn-block btn-success"><i class="fa fa-save"></i> Save Changes</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


@endsection

@section('script')

@endsection
