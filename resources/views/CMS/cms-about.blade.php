@extends('layout.app')

@section('content')

    <div class="main-body">
        <div class="page-wrapper">

            <div class="page-header card">
                {!! $breadcrumbs !!}
            </div>

            <div class="page-body">
                <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="card-header">
                                <h5>About CMS</h5>
                            </div>
                            <div class="card-block">
                                <form action="/aboutCMSPost" method="POST">
                                    @csrf
                                    <div class="form-group row">
                                        <label class="col-sm-2 col-form-label">Title</label>
                                        <div class="col-sm-10">
                                            <input type="text" class="form-control" name="about_title" value="{{ $cms->about_title }}">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-sm-2 col-form-label">Content</label>
                                        <div class="col-sm-10">
                                            <textarea name="about_content" class="summernote">{!! $cms->about_content !!}</textarea>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <button type="submit" class="btn btn-block btn-success"><i class="fa fa-save"></i> Save Changes</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12">

                        <div class="card">
                            <div class="card-header">
                                <div style="float: left;">
                                    <h5>About Cashy Content List</h5>
                                </div>
                                <div style="float: right;">
                                    <button class="btn btn-primary" data-target="#create" data-toggle="modal">Create</button>
                                </div>
                            </div>
                            <div class="card-block">
                                <div class="dt-responsive table-responsive">
                                    <table id="simpletable" class="table table-striped table-bordered nowrap">
                                        <thead>
                                        <tr>
                                            <th>About Content</th>
                                            <th>Actions</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @foreach($about_items as $row)
                                            <tr>
                                                <td>{{ $row->item }}</td>
                                                <td class="icon-btn">
                                                    <button class="btn btn-info btn-icon" type="button" data-toggle="modal" data-target="#update{{ $row->id }}"><i class="fa fa-pencil"></i></button>
                                                    <button class="btn btn-danger btn-icon" type="button" data-toggle="modal" data-target="#delete{{ $row->id }}"><i class="fa fa-trash"></i></button>
                                                </td>
                                            </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="create" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <form action="/about-items" method="POST">
                @csrf
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title">Create Record</h4>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="form-group">
                            <label>Item</label>
                            <input type="text" class="form-control" name="item" />
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button"
                                class="btn btn-default waves-effect"
                                data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-success waves-effect waves-light ">Save Record</button>
                    </div>
                </div>
            </form>
        </div>
    </div>

    @foreach($about_items as $row)
        <div class="modal fade" id="update{{ $row->id }}" tabindex="-1" role="dialog">
            <div class="modal-dialog" role="document">
                <form action="/about-items/{{ $row->id }}" method="POST">
                    @csrf
                    @method('PATCH')
                    <div class="modal-content">
                        <div class="modal-header">
                            <h4 class="modal-title">Update Record</h4>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <div class="form-group">
                                <label>Item</label>
                                <input type="text" class="form-control" name="item" value="{{ $row->item }}" />
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button"
                                    class="btn btn-default waves-effect"
                                    data-dismiss="modal">Close</button>
                            <button type="submit" class="btn btn-success waves-effect waves-light ">Save Changes</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    @endforeach

    @foreach($about_items as $row)
        <div class="modal fade" id="delete{{ $row->id }}" tabindex="-1" role="dialog">
            <div class="modal-dialog" role="document">
                <form action="/about-items/{{ $row->id }}" method="POST">
                    @csrf
                    @method('DELETE')
                    <div class="modal-content">
                        <div class="modal-header">
                            <h4 class="modal-title">Delete Record</h4>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                           <h5>Are you sure you want to delete this record?</h5>
                        </div>
                        <div class="modal-footer">
                            <button type="button"
                                    class="btn btn-default waves-effect"
                                    data-dismiss="modal">Close</button>
                            <button type="submit" class="btn btn-danger waves-effect waves-light ">Delete</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    @endforeach

@endsection

@section('script')

@endsection
