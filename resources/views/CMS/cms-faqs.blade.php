@extends('layout.app')

@section('content')

    <div class="main-body">
        <div class="page-wrapper">

            <div class="page-header card">
                {!! $breadcrumbs !!}
            </div>

            <div class="page-body">

                <div class="row">
                    <div class="col-md-12">

                        <div class="card">
                            <div class="card-header">
                                <div style="float: left;">
                                    <h5>Cashy FAQs</h5>
                                </div>
                                <div style="float: right;">
                                    <button class="btn btn-primary" data-target="#create" data-toggle="modal">Create</button>
                                </div>
                            </div>
                            <div class="card-block">
                                <div class="dt-responsive table-responsive">
                                    <table id="simpletable" class="table table-striped table-bordered nowrap">
                                        <thead>
                                        <tr>
                                            <th>Title</th>
                                            <th>Content</th>
                                            <th>Actions</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @foreach($faq_lists as $row)
                                            <tr>
                                                <td>{{ $row->faq_title }}</td>
                                                <td>{{ $row->faq_content }}</td>
                                                <td class="icon-btn">
                                                    <button class="btn btn-info btn-icon" type="button" data-toggle="modal" data-target="#update{{ $row->id }}"><i class="fa fa-pencil"></i></button>
                                                    <button class="btn btn-danger btn-icon" type="button" data-toggle="modal" data-target="#delete{{ $row->id }}"><i class="fa fa-trash"></i></button>
                                                </td>
                                            </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="create" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <form action="/cms-faqs" method="POST">
                @csrf
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title">Create Record</h4>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="form-group">
                            <label>FAQs Title</label>
                            <input type="text" class="form-control" name="faq_title" />
                        </div>
                        <div class="form-group">
                            <label>FAQs Content</label>
                            <input type="text" class="form-control" name="faq_content" />
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button"
                                class="btn btn-default waves-effect"
                                data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-success waves-effect waves-light ">Save Record</button>
                    </div>
                </div>
            </form>
        </div>
    </div>

    @foreach($faq_lists as $row)
        <div class="modal fade" id="update{{ $row->id }}" tabindex="-1" role="dialog">
            <div class="modal-dialog" role="document">
                <form action="/cms-faqs/{{ $row->id }}" method="POST">
                    @csrf
                    @method('PATCH')
                    <div class="modal-content">
                        <div class="modal-header">
                            <h4 class="modal-title">Update Record</h4>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <div class="form-group">
                                <label>Features Title</label>
                                <input type="text" class="form-control" name="faq_title" value="{{ $row->faq_title }}" />
                            </div>
                            <div class="form-group">
                                <label>Features Content</label>
                                <input type="text" class="form-control" name="faq_content" value="{{ $row->faq_content }}" />
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button"
                                    class="btn btn-default waves-effect"
                                    data-dismiss="modal">Close</button>
                            <button type="submit" class="btn btn-success waves-effect waves-light ">Save Changes</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    @endforeach

    @foreach($faq_lists as $row)
        <div class="modal fade" id="delete{{ $row->id }}" tabindex="-1" role="dialog">
            <div class="modal-dialog" role="document">
                <form action="/cms-faqs/{{ $row->id }}" method="POST">
                    @csrf
                    @method('DELETE')
                    <div class="modal-content">
                        <div class="modal-header">
                            <h4 class="modal-title">Delete Record</h4>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <h5>Are you sure you want to delete this record?</h5>
                        </div>
                        <div class="modal-footer">
                            <button type="button"
                                    class="btn btn-default waves-effect"
                                    data-dismiss="modal">Close</button>
                            <button type="submit" class="btn btn-danger waves-effect waves-light ">Delete</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    @endforeach

@endsection

@section('script')

@endsection
