@extends('layout.app')

@section('content')
    <main class="main">
        <!-- Breadcrumb -->
        <ol class="breadcrumb bc-colored bg-theme" id="breadcrumb">
            <li class="breadcrumb-item ">
                <a href="">Home</a>
            </li>
            <li class="breadcrumb-item">
                <a href="#"> Client </a>
            </li>
            <li class="breadcrumb-item active"> List</li>
        </ol>

        <div class="container-fluid">

            <div class="animated fadeIn">

                <div class="row">
                    <div class="col-md-12">
                        <div class="card card-accent-dark">
                            <div class="card-header">
                                <h5 class="text-theme">Bulk Data</h5>
                            </div>

                            <div class="card-body">
                                <div class="form-group">
                                    <a href="{{ url('clientDownload') }}" class="btn btn-success btn-block"><i class="fa fa-download"></i> CSV DATA & FORMAT</a>
                                </div>
                                <div class="form-group">
                                    <form action="/clientUpload" method="POST" enctype="multipart/form-data">
                                        @csrf
                                        <label>Upload</label>
                                        <input type="file" class="form-control" name="import_file" />
                                        <button type="submit" class="btn btn-info btn-block"><i class="fa fa-upload"></i> Upload</button>
                                    </form>

                                </div>
                            </div>
                        </div>

                    </div>
                </div>

                <div class="row">

                    <div class="col-md-12">
                        <div class="card card-accent-theme">

                            <div class="card-body">
                                <div align="right">
                                    <button type="button" class="btn btn-primary" data-toggle="modal"
                                            data-target="#create">Create
                                    </button>
                                </div>
                                <h4 class="text-theme">Client
                                </h4>
                                <br/>
                                <table id="exampleTableTools" class="display table table-hover table-striped"
                                       cellspacing="0" width="100%">
                                    <thead>
                                    <tr>
                                        <th>ID</th>
                                        <th>Client Name</th>
                                        <th>Contact Person</th>
                                        <th>Email</th>
                                        <th>Address</th>
                                        <th>Contact</th>
                                        <th>Action</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($client as $row)
                                        <tr>
                                            <td>{{ $row->id }}</td>
                                            <td>{{ $row->clientName }}</td>
                                            <td>{{ $row->contactName }}</td>
                                            <td>{{ $row->email }}</td>
                                            <td>{{ $row->address }}</td>
                                            <td>{{ $row->contact }}</td>
                                            <td class="text-nowrap">
                                                <a href="#" data-toggle="modal" data-target="#edit{{ $row->id }}">
                                                    <i class="fa fa-pencil text-inverse m-r-10"></i>
                                                </a>
                                                <a href="#" data-toggle="modal" data-target="#delete{{ $row->id }}">
                                                    <i class="fa fa-close text-danger"></i>
                                                </a>
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                            <!-- end card-body -->
                        </div>
                        <!-- end card -->
                    </div>
                    <!-- end col -->

                </div>
                <!-- end row -->
            </div>
            <!-- end animated fadeIn -->
        </div>
        <!-- end container-fluid -->
    </main>
    <!-- end main -->


    <!-- start model -->
    <div class="modal fade bs-live-example-modal-long" id="create" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-md" role="document">
            <div class="modal-content">
                <form action="/client" method="POST" enctype="multipart/form-data">
                    @csrf
                    <div class="modal-header">
                        <h6 class="modal-title">New Client Record</h6>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">×</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="recipient-name" class="col-form-label">Client Name:</label>
                                    <input type="text" name="clientName" class="form-control" required value="{{ old('clientName') }}" />
                                </div>
                                <div class="form-group">
                                    <label for="recipient-name" class="col-form-label">Email:</label>
                                    <input type="text" name="email" class="form-control" value="{{ old('email') }}" required/>
                                </div>
                                <div class="form-group">
                                    <label for="recipient-name" class="col-form-label">Address:</label>
                                    <textarea name="address" class="form-control">{{ old('address') }}</textarea>
                                </div>
                                <div class="form-group">
                                    <label for="recipient-name" class="col-form-label">Contact:</label>
                                    <input type="text" name="contact" class="form-control" required value="{{ old('contact') }}"/>
                                </div>
                            </div>
                            <div class="col-12">
                                <div class="form-group">
                                    <h6>Contact Person</h6>
                                </div>
                                <div class="form-group">
                                    <label for="recipient-name" class="col-form-label"> Name:</label>
                                    <input type="text" name="contactName" class="form-control" value="{{ old('contactName') }}" required/>
                                </div>
                                <div class="form-group">
                                    <label for="recipient-name" class="col-form-label"> Position:</label>
                                    <input type="text" name="contactPosition" class="form-control" value="{{ old('contactPosition') }}" required/>
                                </div>
                                <div class="form-group">
                                    <label for="recipient-name" class="col-form-label"> Number:</label>
                                    <input type="number" name="contactNumber" class="form-control" value="{{ old('contactNumber') }}" required/>
                                </div>
                                <div class="form-group">
                                    <label for="recipient-name" class="col-form-label"> Email:</label>
                                    <input type="email" name="contactEmail" class="form-control" value="{{ old('contactEmail') }}" required/>
                                </div>
                            </div>
                            <div class="col-12">
                                <div class="form-group">
                                    <h5>Client Login</h5>
                                </div>
                                <div class="row">
                                    <div class="form-group col-6">
                                        <label>Username</label>
                                        <input type="text" name="username" class="form-control" required />
                                    </div>
                                    <div class="form-group col-6">
                                        <label>Password</label>
                                        <input type="password" name="password" class="form-control" required />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-info">Save Record</button>
                    </div>
                </form>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>

    @foreach($client as $row)
        <div class="modal fade bs-live-example-modal-long" id="edit{{ $row->id }}" tabindex="-1" role="dialog"
             aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <form action="client/{{ $row->id }}" method="POST" enctype="multipart/form-data">
                        @csrf
                        @method('PATCH')
                        <div class="modal-header">
                            <h6 class="modal-title">Edit Client Record</h6>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">×</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="recipient-name" class="col-form-label">Client Name:</label>
                                        <input type="text" name="clientName" class="form-control" value="{{ $row->clientName }}" />
                                    </div>
                                    <div class="form-group">
                                        <label for="recipient-name" class="col-form-label">Email:</label>
                                        <input type="text" name="email" class="form-control" value="{{ $row->email }}"/>
                                    </div>
                                    <div class="form-group">
                                        <label for="recipient-name" class="col-form-label">Address:</label>
                                        <input type="text" name="address" class="form-control" value="{{ $row->address }}"/>
                                    </div>
                                    <div class="form-group">
                                        <label for="recipient-name" class="col-form-label">Contact:</label>
                                        <input type="text" name="contact" class="form-control" value="{{ $row->contact }}"/>
                                    </div>
                                </div>
                                <div class="col-12">
                                    <div class="form-group">
                                        <h6>Contact Person</h6>
                                    </div>
                                    <div class="form-group">
                                        <label for="recipient-name" class="col-form-label"> Name:</label>
                                        <input type="text" name="contactName" class="form-control" value="{{ $row->contactName }}"/>
                                    </div>
                                    <div class="form-group">
                                        <label for="recipient-name" class="col-form-label"> Position:</label>
                                        <input type="text" name="contactPosition" class="form-control" value="{{ $row->contactPosition }}"/>
                                    </div>
                                    <div class="form-group">
                                        <label for="recipient-name" class="col-form-label"> Number:</label>
                                        <input type="number" name="contactNumber" class="form-control" value="{{ $row->contactNumber }}"/>
                                    </div>
                                    <div class="form-group">
                                        <label for="recipient-name" class="col-form-label"> Email:</label>
                                        <input type="email" name="contactEmail" class="form-control" value="{{ $row->contactEmail }}"/>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                            <button type="submit" class="btn btn-primary">Save Record</button>
                        </div>
                    </form>
                </div>
                <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>

        <div class="modal fade bs-live-example-modal-long" id="delete{{ $row->id }}" tabindex="-1" role="dialog"
             aria-hidden="true">
            <div class="modal-dialog modal-sm" role="document">
                <div class="modal-content">
                    <form action="client/{{ $row->id }}" method="POST" enctype="multipart/form-data">
                        <div class="modal-header">
                            <h6 class="modal-title">Delete Client Record</h6>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">×</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            @csrf
                            @method('DELETE')
                            <p>Are you sure you want to delete this record?</p>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                            <button type="submit" class="btn btn-warning">Delete Record</button>
                        </div>
                    </form>
                </div>

                <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>
    @endforeach
    <!-- /.modal -->

@endsection

@section('script')

    <script type="text/javascript">
        $('#brand').select2({
            width: "100%",
            placeholder: "Select or Add Record",
            allowClear: true,
            tags: true
        });
        $('#brandCategory').select2({
            width: "100%",
            placeholder: "Select or Add Record",
            allowClear: true,
            tags: true
        });

    </script>




@endsection
