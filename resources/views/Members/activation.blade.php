@extends('layout.app')

@section('content')

    <div class="main-body">
        <div class="page-wrapper">

            <div class="page-header card">
                {!! $breadcrumbs !!}
            </div>

            <div class="page-body">


                <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="card-header">
                                <h5>Generate Activation Code</h5>
                            </div>
                            <div class="card-block">
                                <form action="/activation" method="POST" id="xin-form">
                                    @csrf
                                    <div class="form-group">
                                        <label>Activation Code <button style="border: 0; background: transparent" id="generate" type="button"> <i class="fa fa-refresh"></i> <span id="progress"></span> </button></label>
                                        <input type="text" class="form-control" name="activation_code" id="activation" required />
                                    </div>
                                    <div class="form-group">
                                        <label>Members</label>
                                        <select class="form-control" name="assigned_to" required>
                                            @foreach($members as $member)
                                                <option value="{{ $member->member_id }}">{{ $member->fullname }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <button class="btn btn-primary btn-block" id="activation_button" type="submit"><i class="fa fa-save"></i> Assign</button>
                                    </div>
                                </form>

                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12">

                        <div class="card">
                            <div class="card-header">
                                <div style="float: left;">
                                    <h5>Activation Codes</h5>
                                </div>
                            </div>
                            <div class="card-block">
                                <div class="dt-responsive table-responsive">
                                    <table id="activationTable" class="table table-striped table-bordered nowrap">
                                        <thead>
                                        <tr>
                                            <th>Activation Code</th>
                                            <th>Assigned Activator</th>
                                            <th>Status</th>
                                        </tr>
                                        </thead>
                                        <tbody>

                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('scripts')
    <script>

        $(document).ready(function() {

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });


            let table = $('#activationTable').dataTable({
                processing: true,
                serverSide: true,
                ajax: "{{ url("/fetchActivationCodes") }}",
                columns: [
                    {
                        data: 'activation_code',
                        name: 'activation_code'
                    },
                    {
                        data: "fullname",
                        name: "fullname"
                    },
                    {
                        data: "status",
                        name: "status",
                        "render": function (data, type, row, meta) {
                            switch (row['status']) {
                                case "0":
                                    return "<label class='label label-primary'>Unused</label>";

                                case "1":
                                    return "<label class='label label-danger'>Used</label>"
                            }

                        }
                    },
                ]
            });

            $("#generate").on("click", function (e) {

                $("#progress").html("Generating...");

                $.get('/generateActivation', function (data) {
                    $("#activation").val(data);
                    $("#progress").html('');
                })
            });

            $("#xin-form").on('submit', function(e) {
                e.preventDefault();
                $("#activation_button").html('Assigning...');
                $.ajax({
                    type: "POST",
                    url: $(this).prop('action'),
                    data: $("#xin-form").serialize(),
                    success: function (data) {
                        table.fnDraw(false);
                        $("#xin-form")[0].reset();
                        toastr.success("Code Generated Successfully.")
                        $("#activation_button").html('<i class="fa fa-save"></i> Assign');
                    },
                    error: function (data) {
                        // console.log("");
                        toastr.error("Activation Code Already Used");
                    }
                });
            });


        });
    </script>
@endsection
