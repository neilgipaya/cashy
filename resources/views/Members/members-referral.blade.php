@extends('layout.app')

@section('content')

    <div class="main-body">
        <div class="page-wrapper">

            <div class="page-header card">
                {!! $breadcrumbs !!}
            </div>

            <div class="page-body">

                <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="card-header">
                                <h5>My Referral Code</h5>
                            </div>
                            <div class="card-body">
                                <pre>Copy this referral code link and spread to different people. You'll get a points for every successful Referral</pre>
                                <div class="form-group row">
                                    <div class="col-md-6">
                                        <input type="text" id="referral_code" class="form-control" value="https://cashyshop.com/register/?r={{ $referral_code === null ? "" : $referral_code }}" readonly />
                                    </div>
                                    <div class="col-md-6 icon-btn">
                                        <button class="btn btn-primary" type="button" onclick="copy()">Copy Referral Code</button>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12">

                        <div class="card">
                            <div class="card-header">
                                <div style="float: left;">
                                    <h5>My Referrals</h5>
                                </div>
                            </div>
                            <div class="card-block">
                                <div class="dt-responsive table-responsive">
                                    <table id="simpletable" class="table table-striped table-bordered nowrap">
                                        <thead>
                                        <tr>
                                            <th>Profile</th>
                                            <th>Name</th>
                                            <th>Email</th>
                                            <th>Gender</th>
                                            <th>Contact No</th>
                                            <th>Activation Code</th>
                                            <th>Activation Status</th>
                                            @can('activate-member')
                                                <th>Action</th>
                                            @endcan
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @foreach($referral_lists as $row)
                                            <tr>
                                                <td></td>
                                                <td>{{ $row->fullname }}</td>
                                                <td>{{ $row->email }}</td>
                                                <td>{{ $row->gender }}</td>
                                                <td>{{ $row->contact_no }}</td>
                                                <td>{{ empty($row->activation_code)? "NOT ACTIVATED" : $row->activation_code }}</td>
                                                <td>
                                                    @if(!empty($row->activation_code))
                                                        <label class='label label-primary'>Activated</label>
                                                    @else
                                                        <label class='label label-danger'>Not Activated</label>
                                                    @endif
                                                </td>
                                                @can('activate-member')
                                                    <td class="icon-btn">
                                                        @if(empty($row->activation_code))
                                                            <button class="btn btn-icon btn-info " title="Activate" type="button" data-toggle="modal" data-target="#activate{{ $row->member_id }}"><i class="fa fa-toggle-on"></i></button>
                                                        @endif
                                                    </td>
                                                @endcan
                                            </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    @foreach($referral_lists as $row)
        <div class="modal fade" id="activate{{ $row->member_id }}" tabindex="-1" role="dialog">
            <div class="modal-dialog" role="document">
                <form action="/activate-member/{{ $row->user_id }}" method="POST">
                    @csrf
                    @method('PATCH')
                    <div class="modal-content">
                        <div class="modal-header">
                            <h4 class="modal-title">Activate Member</h4>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <div class="form-group">
                                <label>Available Activation Code</label>
                                <select class="form-control" name="activation_code" required>
                                    @foreach($activation_codes as $activation_code)
                                        @if($activation_code->status != '1')
                                            <option value="{{ $activation_code->activation_code }}">{{ $activation_code->activation_code }}</option>
                                        @endif
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button"
                                    class="btn btn-default waves-effect"
                                    data-dismiss="modal">Close</button>
                            <button type="submit" class="btn btn-danger waves-effect waves-light ">Activate Member</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    @endforeach

    @foreach($referral_lists as $row)
        <div class="modal fade" id="delete{{ $row->member_id }}" tabindex="-1" role="dialog">
            <div class="modal-dialog" role="document">
                <form action="/members/{{ $row->member_id }}" method="POST">
                    @csrf
                    @method('PATCH')
                    <div class="modal-content">
                        <div class="modal-header">
                            <h4 class="modal-title">Deactivate User</h4>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <input type="hidden" name="status" value="Inactive" />
                            <h5>Are you sure you want to deactivate this member?</h5>
                        </div>
                        <div class="modal-footer">
                            <button type="button"
                                    class="btn btn-default waves-effect"
                                    data-dismiss="modal">Close</button>
                            <button type="submit" class="btn btn-danger waves-effect waves-light ">Deactivate</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    @endforeach
@endsection

@section('scripts')
    <script>
        function copy() {
            /* Get the text field */
            let copyText = document.getElementById("referral_code");

            /* Select the text field */
            copyText.select();
            copyText.setSelectionRange(0, 99999); /*For mobile devices*/

            /* Copy the text inside the text field */
            document.execCommand("copy");

            /* Alert the copied text */
            toastr.success("Referral Copied");
        }
    </script>
@endsection
