@extends('layout.app')

@section('content')

    <div class="main-body">
        <div class="page-wrapper">

            <div class="page-header card">
                {!! $breadcrumbs !!}
            </div>

            <div class="page-body">
                <div class="row">
                    <div class="col-md-8 offset-2">
                        <div class="card">
                            <div class="card-header">
                                <h5>Register New Member</h5>
                            </div>
                            <div class="card-block">
                                <form action="/members" method="POST" enctype="multipart/form-data">
                                    @csrf
                                    <div class="form-group row">
                                        <div class="col-md-6">
                                            <label>Username</label>
                                            <input type="text" class="form-control" name="username" required value="{{ old('username') }}" />
                                        </div>
                                        <div class="col-md-6">
                                            <label>Password</label>
                                            <input type="password" class="form-control" name="password" required />
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <div class="col-md-12">
                                            <label>Email Address</label>
                                            <input type="email" class="form-control" name="email" required value="{{ old('email') }}" />
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <div class="col-md-12">
                                            <label class="col-form-label">Profile</label>
                                            <input type="file" name="profile" id="filer_input">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <div class="col-md-4">
                                            <label>First Name</label>
                                            <input type="text" class="form-control" name="firstname" value="{{ old('firstname') }}" required />
                                        </div>
                                        <div class="col-md-3">
                                            <label>Middle Name</label>
                                            <input type="text" class="form-control" name="middlename" value="{{ old('middlename') }}"  />
                                        </div>
                                        <div class="col-md-3">
                                            <label>Last Name</label>
                                            <input type="text" class="form-control" name="lastname" value="{{ old('lastname') }}" required />
                                        </div>
                                        <div class="col-md-2">
                                            <label>Suffix</label>
                                            <input type="text" class="form-control" name="suffix" value="{{ old('suffix') }}" />
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <div class="col-md-12">
                                            <label>Address</label>
                                            <textarea name="address" class="form-control" rows="2" required>{{ old('address') }}</textarea>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <div class="col-md-6">
                                            <label>Gender</label>
                                            <select name="gender" class="form-control" required>
                                                <option value="Male">Male</option>
                                                <option value="Female">Female</option>
                                                <option value="Transgender">Transgender</option>
                                            </select>
                                        </div>
                                        <div class="col-md-6">
                                            <label>Contact No</label>
                                            <input type="tel" class="form-control" name="contact_no" required value="{{ old('contact_no') }}" />
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <div class="col-md-12">
                                            <label>Activation Code</label>
                                            <input type="text" class="form-control" name="activation_code" value="{{ old('activation_code') }}" />
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <div class="col-md-12">
                                            <label>Member Referral Code</label>
                                            <input type="text" name="parent_member" class="form-control" value="{{ old('parent_member') }}" />
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <button type="submit" class="btn btn-block btn-success"><i class="fa fa-save"></i> Register Member</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


@endsection

@section('script')

@endsection
