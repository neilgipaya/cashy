@extends('layout.app')

@section('content')

    <div class="main-body">
        <div class="page-wrapper">

            <div class="page-header card">
                {!! $breadcrumbs !!}
            </div>

            <div class="page-body">

                <div class="row">
                    <div class="col-md-12">

                        <div class="card">
                            <div class="card-header">
                                <div style="float: left;">
                                    <h5>Cashy Members</h5>
                                </div>
{{--                                <div style="float: right;">--}}
{{--                                    <a href="{{ url('members/create') }}" class="btn btn-primary">Register New</a>--}}
{{--                                </div>--}}
                            </div>
                            <div class="card-block">
                                <div class="dt-responsive table-responsive">
                                    <table id="simpletable" class="table table-striped table-bordered nowrap">
                                        <thead>
                                        <tr>
                                            <th>Profile</th>
                                            <th>Username</th>
                                            <th>Name</th>
                                            <th>Email</th>
                                            <th>Gender</th>
                                            <th>Contact No</th>
                                            <th>Activation Code</th>
                                            <th>Activation Status</th>
                                            <th>Action</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @foreach($members as $row)
                                            <tr>
                                                <td></td>
                                                <td>{{ $row->username }}</td>
                                                <td>{{ $row->fullname }}</td>
                                                <td>{{ $row->email }}</td>
                                                <td>{{ $row->gender }}</td>
                                                <td>{{ $row->contact_no }}</td>
                                                <td>{{ empty($row->activation_code)? "NOT ACTIVATED" : $row->activation_code }}</td>
                                                <td>
                                                    @if(!empty($row->activation_code))
                                                        <label class='label label-primary'>Activated</label>
                                                    @else
                                                        <label class='label label-danger'>Not Activated</label>
                                                    @endif
                                                </td>
                                                <td class="dropdown">
                                                    <button
                                                        type="button"
                                                        class="btn btn-primary btn-sm dropdown-toggle "
                                                        data-toggle="dropdown"
                                                        aria-haspopup="true"
                                                        aria-expanded="false"><i
                                                            class="fa fa-cog"
                                                            aria-hidden="true"></i></button>
                                                    <div
                                                        class="dropdown-menu dropdown-menu-right b-none contact-menu">
{{--                                                        <a class="dropdown-item"--}}
{{--                                                           href="#!"><i--}}
{{--                                                                class="icofont icofont-edit"></i>Edit</a>--}}
                                                        <a class="dropdown-item"
                                                           href="{{ url('members/' . $row->member_id) }}"><i
                                                                class="icofont icofont-eye-alt"></i>View</a>
                                                        <a class="dropdown-item"
                                                           href="#" data-toggle="modal" data-target="#activate{{ $row->member_id }}"><i
                                                                class="icofont icofont-tasks-alt"></i>Activate</a>
                                                        <a class="dropdown-item"
                                                           href="#" data-toggle="modal" data-target="#delete{{ $row->member_id }}"><i
                                                                class="icofont icofont-ui-delete"></i>Deactivate</a>
{{--                                                        <a class="dropdown-item"--}}
{{--                                                           href="#!"><i--}}
{{--                                                                class="icofont icofont-ui-note"></i>Notes</a>--}}
{{--                                                        <a class="dropdown-item"--}}
{{--                                                           href="#!"><i--}}
{{--                                                                class="icofont icofont-eye-alt"></i>Activity</a>--}}
{{--                                                        <a class="dropdown-item"--}}
{{--                                                           href="#!"><i--}}
{{--                                                                class="icofont icofont-badge"></i>Schedule</a>--}}
                                                    </div>
                                                </td>
                                            </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('scripts')
    @foreach($members as $row)
        <div class="modal fade" id="delete{{ $row->member_id }}" tabindex="-1" role="dialog">
            <div class="modal-dialog" role="document">
                <form action="/members/{{ $row->member_id }}" method="POST">
                    @csrf
                    @method('PATCH')
                    <div class="modal-content">
                        <div class="modal-header">
                            <h4 class="modal-title">Deactivate User</h4>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <input type="hidden" name="status" value="Inactive" />
                            <h5>Are you sure you want to deactivate this member?</h5>
                        </div>
                        <div class="modal-footer">
                            <button type="button"
                                    class="btn btn-default waves-effect"
                                    data-dismiss="modal">Close</button>
                            <button type="submit" class="btn btn-danger waves-effect waves-light ">Deactivate</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    @endforeach

    @foreach($members as $row)
        <div class="modal fade" id="activate{{ $row->member_id }}" tabindex="-1" role="dialog">
            <div class="modal-dialog" role="document">
                <form action="/activate-member/{{ $row->member_id }}" method="POST">
                    @csrf
                    @method('PATCH')
                    <div class="modal-content">
                        <div class="modal-header">
                            <h4 class="modal-title">Activate User</h4>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <div class="form-group">
                                <label>Available Activation Code</label>
                                <select class="form-control" name="activation_code">
                                    @foreach($activation_codes as $activation_code)
                                        <option value="{{{ $activation_code->activation_code }}}">{{ $activation_code->activation_code }} - {{ $activation_code->fullname }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button"
                                    class="btn btn-default waves-effect"
                                    data-dismiss="modal">Close</button>
                            <button type="submit" class="btn btn-danger waves-effect waves-light ">Activate</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    @endforeach
@endsection
