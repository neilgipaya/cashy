<!DOCTYPE html>
<html lang="en">

<meta http-equiv="content-type" content="text/html;charset=UTF-8" />

<head>
    <title>{{ env('APP_NAME') }}</title>


    <!--[if lt IE 10]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="description"
          content="{{ __('en.meta-description') }}" />
    <meta name="keywords"
          content="{{ __('en.meta-keywords') }}" />
    <meta name="author" content="{{ __('en.meta-author') }}" />
    <meta name="csrf-token" content="{{ csrf_token() }}">


    <link rel="icon" href="http://html.codedthemes.com/gradient-able/files/assets/images/favicon.ico"
          type="image/x-icon">
    <link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600" rel="stylesheet">

    <link rel="stylesheet" type="text/css" href="{{ asset('backend') }}/bower_components/bootstrap/css/bootstrap.min.css">

    <link rel="stylesheet" type="text/css" href="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.css">

    <link rel="stylesheet" type="text/css" href="{{ asset('backend') }}/assets/css/style.css">

    <link rel="stylesheet" type="text/css" href="{{ asset('backend') }}/assets/css/jquery.mCustomScrollbar.css">

</head>

<body class="fix-menu">

<div class="theme-loader">
    <div class="loader-track">
        <div class="loader-bar"></div>
    </div>
</div>

<section class="login p-fixed d-flex text-center bg-primary common-img-bg">

    <div class="container">
        <div class="row">
            <div class="col-sm-12">

                <div class="signup-card card-block auth-body mr-auto ml-auto">
                    <form action="/registerMember" method="POST" class="md-float-material">
                        @csrf
                        <div class="text-center">
{{--                            <img src="../files/assets/images/logo.png" alt="logo.png">--}}
                        </div>
                        <div class="auth-box">
                            <div class="row m-b-20">
                                <div class="col-md-12">
                                    <h3 class="text-center txt-primary">Sign up. It is fast and easy.</h3>
                                </div>
                            </div>
                            <hr />
                            <div class="input-group">
                                <input type="text" class="form-control" name="username" placeholder="Username" required value="{{ old('username') }}" />
                            </div>
                            <div class="input-group">
                                <input type="password" class="form-control" name="password" required placeholder="Password" />
                            </div>
                            <div class="input-group">
                                <input type="email" class="form-control" name="email" required value="{{ old('email') }}" placeholder="Email Address" />
                            </div>
                            <div class="input-group">
                                <input type="text" class="form-control" name="fullname" value="{{ old('name') }}" required placeholder="Full Name" />
                            </div>
                            <div class="input-group">
                                <input type="text" required name="parent_member" class="form-control" value="{{ empty($referral_code) ? old('parent_member') : $referral_code }}" placeholder="Member Referral Code" />
                            </div>
                            <div class="row m-t-30">
                                <div class="col-md-12">
                                    <button type="submit"
                                            class="btn btn-primary btn-md btn-block waves-effect text-center m-b-20">Sign
                                        up now</button>
                                </div>
                            </div>
                            <hr />
                            <div class="row">
                                <div class="col-md-12 text-black">
                                    <div class="form-register">
                                        <span style="color: black;">
                                            Already have an account?
                                        </span>
                                    </div>

                                    <a href="{{ url('login') }}" class="text-right f-w-600 text-amazon"><u>Login to Cashy now</u></a>
                                </div>
                            </div>
                        </div>
                    </form>

                </div>


            </div>

        </div>

    </div>

</section>

<script src="{{ asset('backend') }}/bower_components/jquery/js/jquery.min.js"></script>
<script src="{{ asset('backend') }}/bower_components/jquery-ui/js/jquery-ui.min.js"></script>
<script src="{{ asset('backend') }}/bower_components/popper.js/js/popper.min.js"></script>
<script src="{{ asset('backend') }}/bower_components/bootstrap/js/bootstrap.min.js"></script>

<script src="{{ asset('backend') }}/bower_components/jquery-slimscroll/js/jquery.slimscroll.js"></script>

<script src="{{ asset('backend') }}/bower_components/modernizr/js/modernizr.js"></script>
<script src="{{ asset('backend') }}/bower_components/modernizr/js/css-scrollbars.js"></script>

<script src="{{ asset('backend') }}/bower_components/i18next/js/i18next.min.js"></script>
<script src="{{ asset('backend') }}/bower_components/i18next-xhr-backend/js/i18nextXHRbackend.min.js"></script>
<script
    src="{{ asset('backend') }}/bower_components/i18next-browser-languagedetector/js/i18nextBrowserLanguageDetector.min.js"></script>
<script src="{{ asset('backend') }}/bower_components/jquery-i18next/js/jquery-i18next.min.js"></script>
<script src="{{ asset('backend') }}/assets/js/common-pages.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.js"></script>
<script src="{{ asset('backend') }}/script/login.js"></script>

<script>
    toastr.options = {
        "closeButton": true,
        "debug": false,
        "newestOnTop": true,
        "progressBar": true,
        "positionClass": "toast-top-right",
        "preventDuplicates": true,
        "onclick": null,
        "showDuration": "1000",
        "hideDuration": "1000",
        "timeOut": "5000",
        "extendedTimeOut": "1000",
        "showEasing": "swing",
        "hideEasing": "linear",
        "showMethod": "fadeIn",
        "hideMethod": "fadeOut"
    };


        @if(Session::has('message'))
    var type = "{{ Session::get('alert-type', 'info') }}";

    switch(type){

        case 'info':
            toastr.info("{{ Session::get('message') }}");
            break;

        case 'warning':
            toastr.warning("{{ Session::get('message') }}");
            break;

        case 'success':
            toastr.success("{{ Session::get('message') }}");
            break;

        case 'error':
            toastr.error("{{ Session::get('message') }}");
            break;
    }
    @endif

</script>
@if ($errors->any())
    @foreach ($errors->all() as $error)
        <script>
            toastr.warning('{{ $error }}');
        </script>
    @endforeach
@endif
</body>

</html>
