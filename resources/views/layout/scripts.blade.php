<script src="{{ asset('backend') }}/bower_components/jquery/js/jquery.min.js"></script>
<script src="{{ asset('backend') }}/bower_components/jquery-ui/js/jquery-ui.min.js"></script>
<script src="{{ asset('backend') }}/bower_components/popper.js/js/popper.min.js"></script>
<script src="{{ asset('backend') }}/bower_components/bootstrap/js/bootstrap.min.js"></script>
<script src="{{ asset('backend') }}/assets/pages/widget/excanvas.js"></script>

<script src="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.js"></script>

<script src="{{ asset('backend') }}/bower_components/jquery-slimscroll/js/jquery.slimscroll.js"></script>

<script src="{{ asset('backend') }}/bower_components/modernizr/js/modernizr.js"></script>

<script src="{{ asset('backend') }}/assets/js/SmoothScroll.js"></script>
<script src="{{ asset('backend') }}/assets/js/jquery.mCustomScrollbar.concat.min.js"></script>

<script src="{{ asset('backend') }}/assets/pages/jquery.filer/js/jquery.filer.min.js"></script>
<script src="{{ asset('backend') }}/assets/pages/filer/custom-filer.js"></script>
<script src="{{ asset('backend') }}/assets/pages/filer/jquery.fileuploads.init.js"></script>


<script src="{{ asset('backend') }}/assets/pages/advance-elements/moment-with-locales.min.js"></script>
<script src="{{ asset('backend') }}/bower_components/bootstrap-datepicker/js/bootstrap-datepicker.min.js"></script>
<script src="{{ asset('backend') }}/assets/pages/advance-elements/bootstrap-datetimepicker.min.js"></script>

<script src="{{ asset('backend') }}/bower_components/bootstrap-daterangepicker/js/daterangepicker.js"></script>

<script src="{{ asset('backend') }}/bower_components/datedropper/js/datedropper.min.js"></script>

<script src="{{ asset('backend') }}/bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="{{ asset('backend') }}/bower_components/datatables.net-buttons/js/dataTables.buttons.min.js"></script>
<script src="{{ asset('backend') }}/assets/pages/data-table/js/jszip.min.js"></script>
<script src="{{ asset('backend') }}/assets/pages/data-table/js/pdfmake.min.js"></script>
<script src="{{ asset('backend') }}/assets/pages/data-table/js/vfs_fonts.js"></script>
<script src="{{ asset('backend') }}/bower_components/datatables.net-buttons/js/buttons.print.min.js"></script>
<script src="{{ asset('backend') }}/bower_components/datatables.net-buttons/js/buttons.html5.min.js"></script>
<script src="{{ asset('backend') }}/bower_components/datatables.net-bs4/js/dataTables.bootstrap4.min.js"></script>
<script src="{{ asset('backend') }}/bower_components/datatables.net-responsive/js/dataTables.responsive.min.js"></script>
<script src="{{ asset('backend') }}/bower_components/datatables.net-responsive-bs4/js/responsive.bootstrap4.min.js"></script>

<script src="{{ asset('backend') }}/assets/pages/data-table/js/data-table-custom.js"></script>

<script src="{{ asset('backend') }}/bower_components/sweetalert/js/sweetalert.min.js"></script>
{{--<script src="{{ asset('backend') }}/assets/js/modal.js"></script>--}}

<script src="{{ asset('backend') }}/assets/js/pcoded.min.js"></script>
<script src="{{ asset('backend') }}/assets/js/vertical/vertical-layout.min.js"></script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.12/summernote-bs4.js"></script>

<script src="{{ asset('backend') }}/assets/pages/chart/echarts/js/echarts-all.js"></script>
{{--<script src="{{ asset('backend') }}/assets/pages/user-profile.js"></script>--}}

<script src="{{ asset('backend') }}/assets/js/script.js"></script>

<script src="{{ asset('backend') }}/script/summernote_config.js"></script>

<script src="{{ asset('backend') }}/script/global.js"></script>

<script>
    // $('.select2').select2();
</script>
