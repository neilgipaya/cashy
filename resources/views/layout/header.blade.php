
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
<meta http-equiv="X-UA-Compatible" content="IE=edge" />
<meta name="description" content="@lang('en.meta-description')" />
<meta name="keywords" content="@lang('en.meta-keywords')" />
<meta name="author" content="@lang('en.meta-author')" />

<link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600" rel="stylesheet">

<link rel="stylesheet" type="text/css" href="{{ asset('backend') }}/bower_components/bootstrap/css/bootstrap.min.css">

<link rel="stylesheet" type="text/css" href="{{ asset('backend') }}/assets/icon/themify-icons/themify-icons.css">

<link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">

<link rel="stylesheet" type="text/css" href="{{ asset('backend') }}/bower_components/datatables.net-bs4/css/dataTables.bootstrap4.min.css">
<link rel="stylesheet" type="text/css" href="{{ asset('backend') }}/assets/pages/data-table/css/buttons.dataTables.min.css">
<link rel="stylesheet" type="text/css" href="{{ asset('backend') }}/bower_components/datatables.net-responsive-bs4/css/responsive.bootstrap4.min.css">

<link href="{{ asset('backend') }}/assets/pages/jquery.filer/css/jquery.filer.css" type="text/css" rel="stylesheet" />
<link href="{{ asset('backend') }}/assets/pages/jquery.filer/css/themes/jquery.filer-dragdropbox-theme.css" type="text/css"
      rel="stylesheet" />

<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.11/css/select2.min.css" rel="stylesheet" />

<link rel="stylesheet" type="text/css" href="{{ asset('backend') }}/bower_components/sweetalert/css/sweetalert.css">

<link rel="stylesheet" type="text/css" href="{{ asset('backend') }}/assets/css/component.css">

<link rel="stylesheet" type="text/css" href="{{ asset('backend') }}/assets/css/jquery.mCustomScrollbar.css">

<link rel="stylesheet" href="{{ asset('backend') }}/assets/pages/chart/radial/css/radial.css" type="text/css" media="all">

<link rel="stylesheet" type="text/css" href="{{ asset('backend') }}/assets/css/style.css">

