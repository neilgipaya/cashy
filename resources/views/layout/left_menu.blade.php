<nav class="pcoded-navbar">
    <div class="sidebar_toggle"><a href="#"><i class="icon-close icons"></i></a></div>
    <div class="pcoded-inner-navbar main-menu">
        <div class="pcoded-navigation-label">Navigation</div>
        <ul class="pcoded-item pcoded-left-item">
            @if(auth()->user()->can('admin-dashboard') or  auth()->user()->can('member-dashboard'))
                <li class="{{ Request::segment(1) == 'dashboard'? 'active': '' }}">
                    <a href="{{ url('dashboard') }}">
                        <span class="pcoded-micon"><i class="ti-home"></i><b>D</b></span>
                        <span class="pcoded-mtext">Dashboard</span>
                    </a>
                </li>
            @endif
        </ul>

        @if(Auth::user()->activation_status == 'Activated')
            @can('solve')
            <div class="pcoded-navigation-label">Cashy Solving</div>
            <ul class="pcoded-item pcoded-left-item">
                <li class="pcoded-hasmenu {{ Request::segment(1) == 'solving'? 'active pcoded-trigger': '' }}">
                    <a href="javascript:void(0)">
                        <span class="pcoded-micon"><i class="ti-layout-grid2-alt"></i><b>S</b></span>
                        <span class="pcoded-mtext">Solving</span>
                        <span class="pcoded-mcaret"></span>
                    </a>
                    <ul class="pcoded-submenu">
                        <li class="{{ Request::segment(2) == 'solve-addition'? 'active': '' }}">
                            <a href="{{ url('solving/solve-addition') }}">
                                <span class="pcoded-micon"><i class="ti-angle-right"></i></span>
                                <span class="pcoded-mtext">Solve Addition</span>
                                <span class="pcoded-mcaret"></span>
                            </a>
                        </li>
                        <li class="{{ Request::segment(2) == 'solve-subtraction'? 'active': '' }}">
                            <a href="{{ url('solving/solve-subtraction') }}">
                                <span class="pcoded-micon"><i class="ti-angle-right"></i></span>
                                <span class="pcoded-mtext">Solve Subtraction</span>
                                <span class="pcoded-mcaret"></span>
                            </a>
                        </li>
                        <li class="{{ Request::segment(2) == 'solve-multiplication'? 'active': '' }}">
                            <a href="{{ url('solving/solve-multiplication') }}">
                                <span class="pcoded-micon"><i class="ti-angle-right"></i></span>
                                <span class="pcoded-mtext">Solve Multiplication</span>
                                <span class="pcoded-mcaret"></span>
                            </a>
                        </li>
                        <li class="{{ Request::segment(2) == 'solve-division'? 'active': '' }}">
                            <a href="{{ url('solving/solve-division') }}">
                                <span class="pcoded-micon"><i class="ti-angle-right"></i></span>
                                <span class="pcoded-mtext">Solve Division</span>
                                <span class="pcoded-mcaret"></span>
                            </a>
                        </li>
                    </ul>
                </li>
            </ul>
        @endcan
        @endif


        @can('members-list')
            <div class="pcoded-navigation-label">Members Panel</div>
        @endcan
        <ul class="pcoded-item pcoded-left-item">
            @can('members-list')
            <li class="{{ Request::segment(1) == 'members'? 'active': '' }}">
                <a href="{{ url('members') }}">
                    <span class="pcoded-micon"><i class="ti-user"></i><b>CM</b></span>
                    <span class="pcoded-mtext">Cashy Members</span>
                </a>
            </li>
            @endcan
            @can('activation-list')
            <li class="{{ Request::segment(1) == 'activation'? 'active': '' }}">
                <a href="{{ url('activation') }}">
                    <span class="pcoded-micon"><i class="ti-line-dashed"></i><b>AC</b></span>
                    <span class="pcoded-mtext">Activation Code</span>
                </a>
            </li>
            @endcan
            @if(Auth::user()->activation_status == 'Activated')
                @can('referral-list')
                    <li class="{{ Request::segment(1) == 'referral'? 'active': '' }}">
                        <a href="{{ url('referral') }}">
                            <span class="pcoded-micon"><i class="fa fa-user-circle"></i><b>R</b></span>
                            <span class="pcoded-mtext">Referral List</span>
                        </a>
                    </li>
                @endcan
            @endif
        </ul>
        @can('cms')
            <div class="pcoded-navigation-label">Content Management</div>
            <ul class="pcoded-item pcoded-left-item">
                <li class="pcoded-hasmenu {{ Request::segment(1) == 'cms'? 'active pcoded-trigger': '' }}">
                    <a href="javascript:void(0)">
                        <span class="pcoded-micon"><i class="ti-layout-grid2-alt"></i><b>C</b></span>
                        <span class="pcoded-mtext">CMS</span>
                        <span class="pcoded-mcaret"></span>
                    </a>
                    <ul class="pcoded-submenu">
                        <li class="{{ Request::segment(2) == 'homeCMS'? 'active': '' }}">
                            <a href="{{ url('cms/homeCMS') }}">
                                <span class="pcoded-micon"><i class="ti-angle-right"></i></span>
                                <span class="pcoded-mtext">Home Header</span>
                                <span class="pcoded-mcaret"></span>
                            </a>
                        </li>
                        <li class="{{ Request::segment(2) == 'aboutCMS'? 'active': '' }}">
                            <a href="{{ url('cms/aboutCMS') }}">
                                <span class="pcoded-micon"><i class="ti-angle-right"></i></span>
                                <span class="pcoded-mtext">About Cashy</span>
                                <span class="pcoded-mcaret"></span>
                            </a>
                        </li>
                        <li class="{{ Request::segment(2) == 'featuresCMS'? 'active': '' }}">
                            <a href="{{ url('cms/featuresCMS') }}">
                                <span class="pcoded-micon"><i class="ti-angle-right"></i></span>
                                <span class="pcoded-mtext">Cashy Features</span>
                                <span class="pcoded-mcaret"></span>
                            </a>
                        </li>
                        <li class="{{ Request::segment(2) == 'faqsCMS'? 'active': '' }}">
                            <a href="{{ url('cms/faqsCMS') }}">
                                <span class="pcoded-micon"><i class="ti-angle-right"></i></span>
                                <span class="pcoded-mtext">Cashy FAQs</span>
                                <span class="pcoded-mcaret"></span>
                            </a>
                        </li>
                    </ul>
                </li>
            </ul>
        @endcan
    </div>
</nav>
