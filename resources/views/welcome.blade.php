<!DOCTYPE html>
<html lang="zxx">


<head>
    <meta charset="UTF-8">
    <meta name="description" content="">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Title -->
    <title>{{ env('APP_NAME') }}</title>

    <!-- Favicon -->
    <link rel="icon" href="{{ asset('favicon.ico') }}">

    <!-- Core Stylesheet -->
    <link rel="stylesheet" href="{{ asset('frontend') }}/css/style.css">

    <!-- Responsive Stylesheet -->
    <link rel="stylesheet" href="{{ asset('frontend') }}/css/responsive.css">
    <script async src="https://pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
    <script>
        (adsbygoogle = window.adsbygoogle || []).push({
            google_ad_client: "ca-pub-6534314793368116",
            enable_page_level_ads: true
        });
    </script>
</head>

<body class="light-version">
<!-- Preloader -->
<div id="preloader">
    <div class="preload-content">
        <div id="dream-load"></div>
    </div>
</div>

<!-- ##### Header Area Start ##### -->
<header class="header-area fadeInDown" data-wow-delay="0.2s">
    <div class="classy-nav-container light breakpoint-off">
        <div class="container">
            <!-- Classy Menu -->
            <nav class="classy-navbar light justify-content-between" id="dreamNav">

                <!-- Logo -->
                <a class="nav-brand light" href="#"><img src="{{ asset('frontend') }}/img/core-img/logo-cashy-dark.png" alt="logo" style="width: 120px"> </a>

                <!-- Navbar Toggler -->
                <div class="classy-navbar-toggler demo">
                    <span class="navbarToggler"><span></span><span></span><span></span></span>
                </div>

                <!-- Menu -->
                <div class="classy-menu">

                    <!-- close btn -->
                    <div class="classycloseIcon">
                        <div class="cross-wrap"><span class="top"></span><span class="bottom"></span></div>
                    </div>

                    <!-- Nav Start -->
                    <div class="classynav">
                        <ul id="nav">
                            <li><a href="#home">Home</a></li>
                            <li><a href="#about">About</a></li>
                            <li><a href="#features">Services</a></li>
                            <li><a href="#faq">FAQ</a></li>
                            <li><a href="#contact">Contact</a></li>
                            <li><a href="{{ url('cashy-shop')  }}">Open Shop</a></li>
                        </ul>

                        @if(Auth::check())

                            <a href="{{ url('dashboard') }}" class="btn login-btn ml-50">
                                Cashy Dashboard
                            </a>
                        @else
                            <!-- Button -->
                            <a href="{{ url('login') }}" class="btn login-btn ml-50">
                                Login / Register
                            </a>
                        @endif
                    </div>
                    <!-- Nav End -->
                </div>
            </nav>
        </div>
    </div>
</header>
<!-- ##### Header Area End ##### -->

<!-- ##### Welcome Area Start ##### -->
<section class="welcome_area clearfix dzsparallaxer auto-init ico fullwidth" data-options='{direction: "normal"}' id="home">
    <div class="divimage dzsparallaxer--target" style="width: 101%; height: 130%; background-image: url({{ asset('frontend') }}/img/bg-img/header3.png)"></div>

    <!-- Hero Content -->
    <div class="hero-content transparent">
        <!-- blip -->
        <div class="dream-blip blip1"></div>
        <div class="dream-blip blip2"></div>
        <div class="dream-blip blip3"></div>
        <div class="dream-blip blip4"></div>

        <div class="container h-100">
            <div class="row h-100 align-items-center">
                <!-- Welcome Content -->
                <div class="col-12 col-lg-6 col-md-12">
                    <div class="welcome-content">
                        <div class="promo-section">
                            <h3 class="special-head dark">{{ $cms_header->header_title_1 }}</h3>
                        </div>
                        <h1 class="fadeInUp" data-wow-delay="0.2s">
                            {{ $cms_header->heading }}
                        </h1>
                        <p class="w-text fadeInUp" data-wow-delay="0.3s">
                            {{ $cms_header->content }}
                        </p>
                        <div class="dream-btn-group fadeInUp" data-wow-delay="0.4s">
                            <a href="#" class="btn more-btn mr-3">@lang('en.home_open_shop')</a>
                            <a href="#about" class="btn more-btn"> @lang('en.home_learn_more')</a>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="main-ilustration main-ilustration-8  mt-30 fadeInUp" style="" data-wow-delay="0.5s">

                    </div>
                </div>

            </div>
        </div>
    </div>
</section>
<!-- ##### Welcome Area End ##### -->


<div class="clearfix"></div>



<!-- ##### About Us Area Start ##### -->
<section class="special section-padding-100-70 clearfix" id="about">

    <div class="container">
        <div class="row align-items-center">
            <div class="col-12 col-lg-6 offset-lg-0 col-md-12 no-padding-left">
                <div class="welcome-meter fadeInUp mb-30" data-wow-delay="0.7s">

                    <div class="video-container">
                        <iframe width="560" height="315" src="https://www.youtube.com/embed/0Q-1jRXKy70" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                    </div>
                </div>
            </div>

            <div class="col-12 col-lg-6 offset-lg-0">
                <div class="who-we-contant">
                    <div class="dream-dots text-left fadeInUp" data-wow-delay="0.2s" style="visibility: visible; animation-delay: 0.2s; animation-name: fadeInUp;">
                        <span class="gradient-text blue">@lang('en.home_about_cashy')</span>
                    </div>
                    <h4 class="fadeInUp" data-wow-delay="0.3s">
                       {{ $about->about_title }}
                    </h4>
                    <p class="fadeInUp" data-wow-delay="0.4s">
                        {{ $about->about_content }}
                    </p>
                    <div class="list-wrap align-items-center mb-20">
                        <div class="row">
                            @foreach($about_lists as $about_list)
                            <div class="col-md-6">
                                <div class="side-feature-list-item">
                                    <i class="fa fa-check-square-o check-mark-icon-font" aria-hidden="true"></i>
                                    <div class="foot-c-info">{{ $about_list->item }}</div>
                                </div>
                            </div>
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
</section>
<!-- ##### About Us Area End ##### -->

<!-- ##### About Us Area Start ##### -->
<section class="about-us-area hero-bg section-padding-100-70 clearfix">
    <div class="container">
        <div class="row align-items-center">

            <div class="col-12 col-lg-6 col-sm-12">
                <div class="who-we-contant">
                    <div class="dream-dots text-left fadeInUp" data-wow-delay="0.2s" style="visibility: visible; animation-delay: 0.2s; animation-name: fadeInUp;">
                        <span class="gradient-text cyan">@lang('en.home_contact_us')</span>
                    </div>
                    <h4 class="w-text bold fadeInUp" data-wow-delay="0.3s">Get in touch with us</h4>
                    <p class="g-text fadeInUp" data-wow-delay="0.5s">Let us know if there are something you want to ask with us and we will answer your question accordingly.</p>
                    <a href="#contact" class="btn more-btn mt-15">contact us</a>

                </div>
            </div>
        </div>
    </div>
</section>


<!-- ##### Our Services Area Start ##### -->
<section class="section-padding-100-70 relative clearfix" id="features">
    <div class="container">

        <div class="section-heading text-center">
            <!-- Dream Dots -->
            <div class="dream-dots justify-content-center fadeInUp" data-wow-delay="0.2s">
                <span class="gradient-text blue">@lang('en.home_features')</span>
            </div>
            <h2 class="fadeInUp" data-wow-delay="0.3s">{{ $features->features_title }}</h2>
            <p class="fadeInUp" data-wow-delay="0.4s">{{ $features->features_content }}</p>
        </div>


        <div class="row">
            @foreach($feature_lists as $feature_list)
                <div class="col-12 col-md-6 col-lg-4">
                    <!-- Content -->
                    <div class="service_single_content text-left mb-100 fadeInUp" data-wow-delay="0.2s">
                        <!-- Icon -->
                        <h6>{{ $feature_list->features_title }}</h6>
                        <p>{{ $feature_list->features_content }}</p>
                    </div>
                </div>
            @endforeach
        </div>
    </div>
</section>



<!-- ##### FAQ & Timeline Area Start ##### -->
<div class="faq-timeline-area section-padding-100-85" id="faq">
    <div class="container">
        <div class="section-heading text-center">
            <!-- Dream Dots -->
            <div class="dream-dots justify-content-center fadeInUp" data-wow-delay="0.2s">
                <span class="gradient-text blue">@lang('en.home_frequently_asked')</span>
            </div>
            <h2 class="fadeInUp" data-wow-delay="0.3s">  </h2>
            <p class="fadeInUp" data-wow-delay="0.4s"></p>
        </div>
        <div class="row align-items-center">
            <div class="col-12 col-lg-6 offset-lg-0 col-md-8 offset-md-2 col-sm-12">
                <img src="{{ asset('frontend') }}/img/core-img/digital-3.png" alt="" class="center-block img-responsive">
            </div>
            <div class="col-12 col-lg-6 col-md-12">
                <div class="dream-faq-area mt-s">
                    <dl style="margin-bottom:0">
                        <!-- Single FAQ Area -->
                        @foreach($faqs as $faq)
                            <dt class="v2 wave fadeInUp" data-wow-delay="0.2s">{{ $faq->faq_title }}</dt>
                            <dd class="fadeInUp" data-wow-delay="0.3s">
                                <p>{{ $faq->faq_content }}</p>
                            </dd>
                        @endforeach
                    </dl>
                </div>
            </div>

        </div>
    </div>
</div>
<!-- ##### FAQ & Timeline Area End ##### -->

<!-- ##### Footer Area Start ##### -->
<footer class="footer-area bg-img" style="background-image: url({{ asset('frontend') }}/img/core-img/pattern.png);">

    <!-- ##### Contact Area Start ##### -->
    <div class="contact_us_area section-padding-100-0" id="contact">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="section-heading text-center">
                        <!-- Dream Dots -->
                        <div class="dream-dots justify-content-center fadeInUp" data-wow-delay="0.2s">
                            <span class="gradient-text blue"></span>
                        </div>
                        <h2 class="fadeInUp" data-wow-delay="0.3s">Contact With Us</h2>
                        <p class="fadeInUp" data-wow-delay="0.4s">
                            Let us know if you have any questions. We're always here 24/7 to assist you.
                        </p>
                    </div>
                </div>
            </div>

            <!-- Contact Form -->
            <div class="row justify-content-center">
                <div class="col-12 col-md-10 col-lg-8">
                    <div class="contact_form">
                        <form action="#" method="post" id="main_contact_form" novalidate>
                            <div class="row">
                                <div class="col-12">
                                    <div id="success_fail_info"></div>
                                </div>

                                <div class="col-12 col-md-6">
                                    <div class="group fadeInUp" data-wow-delay="0.2s">
                                        <input type="text" name="name" id="name" required>
                                        <span class="highlight"></span>
                                        <span class="bar"></span>
                                        <label>Name</label>
                                    </div>
                                </div>
                                <div class="col-12 col-md-6">
                                    <div class="group fadeInUp" data-wow-delay="0.3s">
                                        <input type="text" name="email" id="email" required>
                                        <span class="highlight"></span>
                                        <span class="bar"></span>
                                        <label>Email</label>
                                    </div>
                                </div>
                                <div class="col-12">
                                    <div class="group fadeInUp" data-wow-delay="0.4s">
                                        <input type="text" name="subject" id="subject" required>
                                        <span class="highlight"></span>
                                        <span class="bar"></span>
                                        <label>Subject</label>
                                    </div>
                                </div>
                                <div class="col-12">
                                    <div class="group fadeInUp" data-wow-delay="0.5s">
                                        <textarea name="message" id="message" required></textarea>
                                        <span class="highlight"></span>
                                        <span class="bar"></span>
                                        <label>Message</label>
                                    </div>
                                </div>
                                <div class="col-12 text-center fadeInUp" data-wow-delay="0.6s">
                                    <button type="submit" class="btn more-btn">Send Message</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- ##### Contact Area End ##### -->

    <div class="footer-content-area ">
        <div class="container">
            <div class="row ">
                <div class="col-12 col-lg-4 col-md-6">
                    <div class="footer-copywrite-info">
                        <!-- Copywrite -->
                        <div class="copywrite_text fadeInUp" data-wow-delay="0.2s">
                            <div class="footer-logo">
                                <a href="#">
{{--                                    <img src="{{ asset('frontend') }}/img/core-img/logo-cashy-dark.png" alt="logo">--}}
                                    Cashyshop 2019  </a>
                            </div>
                            <p></p>
                        </div>
                        <!-- Social Icon -->
{{--                        <div class="footer-social-info fadeInUp" data-wow-delay="0.4s">--}}
{{--                            <a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a>--}}
{{--                            <a href="#"> <i class="fa fa-twitter" aria-hidden="true"></i></a>--}}
{{--                            <a href="#"><i class="fa fa-instagram" aria-hidden="true"></i></a>--}}
{{--                        </div>--}}
                    </div>
                </div>


{{--                <div class="col-12 col-lg-2 col-md-6 ">--}}
{{--                    <!-- Content Info -->--}}
{{--                    <div class="contact_info_area d-sm-flex justify-content-between">--}}
{{--                        <div class="contact_info mt-s text-center fadeInUp" data-wow-delay="0.2s">--}}
{{--                            <h5>NAVIGATE</h5>--}}
{{--                            <a href="#"><p>Advertisers</p></a>--}}
{{--                            <a href="#"><p>Developers</p></a>--}}
{{--                            <a href="#"><p>Resources</p></a>--}}
{{--                            <a href="#"><p>Company</p></a>--}}
{{--                            <a href="#"><p>Connect</p></a>--}}
{{--                        </div>--}}
{{--                    </div>--}}
{{--                </div>--}}


{{--                <div class="col-12 col-lg-3 col-md-6 ">--}}
{{--                    <div class="contact_info_area d-sm-flex justify-content-between">--}}
{{--                        <!-- Content Info -->--}}
{{--                        <div class="contact_info mt-s text-center fadeInUp" data-wow-delay="0.4s">--}}
{{--                            <h5>@lang('en.home_contact_us')</h5>--}}
{{--                            <p>Mailing Address: </p>--}}
{{--                            <p></p>--}}
{{--                            <p></p>--}}
{{--                            <p></p>--}}
{{--                        </div>--}}
{{--                    </div>--}}
{{--                </div>--}}
            </div>
        </div>
    </div>

</footer>

<!-- jQuery js -->
<script src="{{ asset('frontend') }}/js/jquery.min.js"></script>
<!-- Popper js -->
<script src="{{ asset('frontend') }}/js/popper.min.js"></script>
<!-- Bootstrap js -->
<script src="{{ asset('frontend') }}/js/bootstrap.min.js"></script>
<!-- All Plugins js -->
<script src="{{ asset('frontend') }}/js/plugins.js"></script>
<!-- Parallax js -->
<script src="{{ asset('frontend') }}/js/dzsparallaxer.js"></script>

<script src="{{ asset('frontend') }}/js/jquery.syotimer.min.js"></script>

<!-- script js -->
<script src="{{ asset('frontend') }}/js/script.js"></script>

</body>


</html>
