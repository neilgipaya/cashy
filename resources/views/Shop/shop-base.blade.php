<!doctype html>
<html lang="en">


<!-- Mirrored from droitthemes.com/html/saasland/shop-grid.html by HTTrack Website Copier/3.x [XR&CO'2014], Fri, 04 Oct 2019 04:22:49 GMT -->
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="shortcut icon" href="img/favicon.png" type="image/x-icon">
    <title>Cashyshop.com</title>
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="{{ asset('shop') }}/css/bootstrap.min.css">
    <!--icon font css-->
    <link rel="stylesheet" href="{{ asset('shop') }}/vendors/themify-icon/themify-icons.css">
    <link rel="stylesheet" href="{{ asset('shop') }}/vendors/elagent/style.css">
    <link rel="stylesheet" href="{{ asset('shop') }}/vendors/flaticon/flaticon.css">
    <link rel="stylesheet" href="{{ asset('shop') }}/vendors/animation/animate.css">
    <link rel="stylesheet" href="{{ asset('shop') }}/vendors/owl-carousel/assets/owl.carousel.min.css">
    <link rel="stylesheet" href="{{ asset('shop') }}/vendors/magnify-pop/magnific-popup.css">
    <link rel="stylesheet" href="{{ asset('shop') }}/vendors/nice-select/nice-select.css">
    <link rel="stylesheet" href="{{ asset('shop') }}/vendors/scroll/jquery.mCustomScrollbar.min.css">
    <link rel="stylesheet" href="{{ asset('shop') }}/css/style.css">
    <link rel="stylesheet" href="{{ asset('shop') }}/css/responsive.css">
</head>

<body>
<div class="body_wrapper">
    <header class="header_area">
        <nav class="navbar navbar-expand-lg menu_one menu_four">
            <div class="container">
                <a class="navbar-brand sticky_logo" href="#"><img src="{{ asset('frontend/img/core-img/logo-cashy.png') }}"  style="width: 60px" alt="logo"><img src="{{ asset('frontend/img/core-img/logo-cashy-dark.png') }}" style="width: 60px" alt=""></a>
                <button class="navbar-toggler collapsed" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                        <span class="menu_toggle">
                            <span class="hamburger">
                                <span></span>
                                <span></span>
                                <span></span>
                            </span>
                            <span class="hamburger-cross">
                                <span></span>
                                <span></span>
                            </span>
                        </span>
                </button>

                <div class="collapse navbar-collapse justify-content-end" id="navbarSupportedContent">
                    <ul class="navbar-nav menu w_menu pl_100">
                        <li class="nav-item dropdown submenu mega_menu mega_menu_two">
                            <a class="nav-link dropdown-toggle" href="{{ url('/') }}">
                                Exit Shop
                            </a>
                        </li>

                    </ul>
                    <a class="btn_get btn_hover" href="#get-app">Login in / Sign up</a>
                </div>
            </div>
        </nav>
    </header>

    <section class="breadcrumb_area">
        <img class="breadcrumb_shap" src="{{ asset('shop') }}/img/breadcrumb/banner_bg.png" alt="">
        <div class="container">
            <div class="breadcrumb_content text-center">
                <h1 class="f_p f_700 f_size_50 w_color l_height50 mb_20">
                    @lang('en.Lorem_title')
                </h1>
                <p class="f_400 w_color f_size_16 l_height26">
                    @lang('en.Lorem_content')
                </p>
            </div>
        </div>
    </section>


    @yield('content')

    <footer class="footer_area footer_area_four f_bg">
        <div class="footer_bottom">
            <div class="container">
                <div class="row align-items-center">
                    <div class="col-lg-4 col-md-5 col-sm-6">
                        <p class="mb-0 f_400">Copyright © 2019 Cashyshop.com</p>
                    </div>
                </div>
            </div>
        </div>
    </footer>
</div>
<!-- Optional JavaScript -->
<!-- jQuery first, then Popper.js, then Bootstrap JS -->
<script src="{{ asset('shop') }}/js/jquery-3.2.1.min.js"></script>
<script src="{{ asset('shop') }}/js/propper.js"></script>
<script src="{{ asset('shop') }}/js/bootstrap.min.js"></script>
<script src="{{ asset('shop') }}/vendors/wow/wow.min.js"></script>
<script src="{{ asset('shop') }}/vendors/sckroller/jquery.parallax-scroll.js"></script>
<script src="{{ asset('shop') }}/vendors/owl-carousel/owl.carousel.min.js"></script>
<script src="{{ asset('shop') }}/vendors/imagesloaded/imagesloaded.pkgd.min.js"></script>
<script src="{{ asset('shop') }}/vendors/isotope/isotope-min.js"></script>
<script src="{{ asset('shop') }}/vendors/magnify-pop/jquery.magnific-popup.min.js"></script>
<script src="{{ asset('shop') }}/vendors/nice-select/jquery.nice-select.min.js"></script>
<script src="{{ asset('shop') }}/vendors/scroll/jquery.mCustomScrollbar.concat.min.js"></script>
<script src="{{ asset('shop') }}/js/plugins.js"></script>
<script src="{{ asset('shop') }}/js/main.js"></script>
</body>


</html>
