@extends('layout.app')

@section('content')

    <div class="main-body">
        <div class="page-wrapper">

            <div class="page-body">
                <div class="row">
                    <div class="col-md-12">
                        <h5>Account Activated.. Stay tuned for dashboard update</h5>
                    </div>
                </div>
            </div>

        </div>
    </div>


@endsection

@section('script')

    <script src="{{ asset('backend') }}/bower_components/chart.js/js/Chart.js"></script>
    <script src="{{ asset('backend') }}/assets/pages/widget/amchart/amcharts.js"></script>
    <script src="{{ asset('backend') }}/assets/pages/widget/amchart/serial.js"></script>
    <script src="{{ asset('backend') }}/assets/pages/widget/amchart/light.js"></script>

    <script src="{{ asset('backend') }}/assets/pages/dashboard/custom-dashboard.js"></script>


    <script>
        console.log('Dashboard');
    </script>
@endsection
