@extends('layout.app')

@section('content')

    <div class="main-body">
        <div class="page-wrapper">

            <div class="page-body">
                <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="card-header">
                                <h5>Account not yet activated</h5>
                            </div>
                            <div class="card-body">
                                <pre>Please Ask the person who invited you to activate your account</pre>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>


@endsection

@section('script')

    <script src="{{ asset('backend') }}/bower_components/chart.js/js/Chart.js"></script>
    <script src="{{ asset('backend') }}/assets/pages/widget/amchart/amcharts.js"></script>
    <script src="{{ asset('backend') }}/assets/pages/widget/amchart/serial.js"></script>
    <script src="{{ asset('backend') }}/assets/pages/widget/amchart/light.js"></script>

    <script src="{{ asset('backend') }}/assets/pages/dashboard/custom-dashboard.js"></script>


    <script>
        console.log('Dashboard');
    </script>
@endsection
