@extends('layout.app')

@section('content')
    <main class="main">
        <!-- Breadcrumb -->
        <ol class="breadcrumb bc-colored bg-theme" id="breadcrumb">
            <li class="breadcrumb-item ">
                <a href="">Home</a>
            </li>
            <li class="breadcrumb-item">
                <a href="#"> Cashy Eloading </a>
            </li>
            <li class="breadcrumb-item active"> API Console</li>
        </ol>

        <div class="container-fluid">

            <div class="animated fadeIn">

                <div class="row">
                    <div class="col-md-12">
                        <div class="card card-accent-dark">
                            <div class="card-header bg-danger">
                                <h5 class="text-white">API Testing</h5>
                            </div>

                            <div class="card-body">
                                <form id="console-form" action="{{ url('cashy-rewards') }}" method="POST">
                                    <input type="hidden" name="_token" id="token" value="{{ csrf_token() }}" />
                                    <input type="hidden" name="cashy_token" value="{{ env('CASHY_REWARDS_TOKEN') }}" />
                                    <div class="form-group">
                                        <label>Mobile Number</label>
                                        <input type="text" class="form-control" name="mobile_number" required />
                                    </div>
                                    <div class="form-group">
                                        <label>UID <button style="border: 0; background: transparent" id="generate" type="button"> <i class="fa fa-refresh"></i> </button></label>
                                        <input type="text" class="form-control" name="uid" id="uid" required />
                                    </div>
                                    <div class="form-group">
                                        <label>SKU</label>
                                        <select name="promo" class="form-control select2" data-plugin="select2" style="width: 100%" required>
                                            <option value="CASHY15">CASHY 15</option>
                                            <option value="CASHY30">CASHY 30</option>
                                            <option value="CASHY50">CASHY 50</option>
                                            <option value="CASHY60">CASHY 60</option>
                                            <option value="CASHY100">CASHY 100</option>
                                            <option value="CASHY115">CASHY 115</option>
                                            <option value="CASHY200">CASHY 200</option>
                                            <option value="CASHY300">CASHY 300</option>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <button type="submit" class="btn btn-block btn-success"><i class="fa fa-save"></i> Send Transaction</button>
                                    </div>
                                </form>
                            </div>
                        </div>

                    </div>
                </div>

                <div class="row">

                    <div class="col-md-12">
                        <div class="card card-accent-theme">

                            <div class="card-body">

                                <h4 class="text-theme">Transactions &nbsp; <button style="border: 0; background: transparent" id="reloadTable" type="button"> <i class="fa fa-refresh"></i> </button>
                                </h4>
                                <br/>
                                <table class="display table table-hover table-striped" cellspacing="0" width="100%" id="loadTable" data-url="{{ url('/getLoadTransactions') }}">
                                    <thead>
                                    <tr>
                                        <th>ID</th>
                                        <th>Transaction Code</th>
                                        <th>Mobile Number</th>
                                        <th>Promo Code</th>
                                        <th>API Response</th>
                                        <th>Transaction Date & Time</th>
                                    </tr>
                                    </thead>
                                    <tbody>

                                    </tbody>
                                </table>
                            </div>
                            <!-- end card-body -->
                        </div>
                        <!-- end card -->
                    </div>
                    <!-- end col -->

                </div>
                <!-- end row -->
            </div>
            <!-- end animated fadeIn -->
        </div>
        <!-- end container-fluid -->
    </main>
    <!-- end main -->


@endsection

@section('script')

    <script type="text/javascript">
        $(function () {


            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            let table = $("#loadTable");
            let dataUrl = table.data('url');

            console.log(dataUrl);
            let ajaxTable = table.DataTable({
                processing: true,
                serverSide: true,
                ajax: dataUrl,
                ordering: false,
                columns: [
                    {data: 'id', name: 'id', 'visible':false},
                    {data: 'transaction_code', name: 'transaction_code'},
                    {data: 'mobile_number', name: 'mobile_number'},
                    {data: 'promo_code', name: 'promo_code'},
                    {data: 'api_response', name: 'api_response'},
                    {data: 'transaction_date_time', name: 'transaction_date_time'},
                ]
            });

            $('#console-form').on('submit', function (e) {
                e.preventDefault();
                let form = $("#console-form");

                form.modal('hide');
                let data = form.serialize();
                let action = form.attr('action');
                let method = form.attr('method');

                $.ajax({
                    type: method,
                    url: action,
                    data: data,
                    success: function (data) {
                        toastr.success("Load Top-up Successful");
                        let oTable = $("#loadTable").dataTable();
                        oTable.fnDraw(false);

                    },
                    error: function (xhr) {
                        let errorMessage = xhr.responseJSON;
                        toastr.error(errorMessage.content);

                    }
                });
            });


            $("#reloadTable").on("click", function (e) {
                e.preventDefault();
                console.log(1);
                ajaxTable.ajax.reload();
            });


            $("#generate").on("click", function (e) {
                e.preventDefault();

                $.get('/getUID', function (data) {
                    console.log(data);
                    $("#uid").val(data.UID);
                })
            })

        });

    </script>


@endsection
