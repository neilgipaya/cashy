<?php

return [

    'meta-description' => 'Welcome to Cashy Shop',
    'meta-keywords' => 'Cashy, Shop, Solving, Addition, Subtraction, Multiplication, Division, Cashyhan na!',
    'meta-author' => 'Aviarthard PH',
    'welcome' => 'Welcome to Cashy App',
    'home_open_shop' => 'Open Shop',
    'home_learn_more' => 'Learn More',
    'home_about_cashy' => 'About Cashy',
    'home_contact_us' => 'Contact Cashy for More Info',
    'home_our_services' => 'Our Services',
    'home_features' => 'Our Features',
    'home_frequently_asked' => 'Frequently Asked Questions',
    'Lorem_title' => 'Lorem ipsum dolor ismet',
    'Lorem_content' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque aliquam pellentesque finibus. Mauris pharetra augue mauris, at lacinia erat congue at. Ut porttitor, augue vel aliquet iaculis, metus turpis congue dolor, id fermentum dolor turpis eu leo. ',


];
