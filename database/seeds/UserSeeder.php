<?php

use Illuminate\Database\Seeder;
use App\User;
use Illuminate\Support\Facades\Hash;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //

        $user = User::create([
            'fullname' => 'Kio Hoviera',
            'email' => 'neilgipaya@gmail.com',
            'username' => 'khoviera',
            'password' => Hash::make('iceangel1011')
        ]);

        \App\Members::create([

            'user_id' => $user->id,
        ]);
    }
}
