<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'CashyShopController@index')->name('welcome');

Route::get('cashy-shop', 'CashyShopController@shop');


//load controller
Route::post('cashy-rewards', 'LoadController@rewards');
Route::get('/getUID', 'LoadController@generateUID');

Route::get('product-details/{product_id}', 'CashyShopController@productDetails');

//for force logout maintenance
Route::get('force-logout', function (){
   Auth::logout();
});


Route::group(['middleware'=>'guest'], function () {


    Route::get('/login', 'AuthenticationController@showLogin');

    Route::post('/authenticate', 'AuthenticationController@login');

    Route::get('/register', 'AuthenticationController@showRegister');

    Route::post('/registerMember', 'AuthenticationController@registerMember');

});


Route::group(['middleware'=>'auth'], function () {


    //dashboardforMember
    Route::get('dashboard/member', 'DashboardController@memberDashboard');

    Route::get('/getLoadTransactions', 'LoadTransactionsController@transactions');
    Route::get('/user-dashboard', 'DashboardController@userDashboard');


    //apiRoutes
    Route::post('/cashySolve', 'SolvingController@solveNumber');
    Route::get('/generateNumber/{operation}', 'SolvingController@generateNumber');


    //solvingRoutes
    Route::get('fetchProblem/{formula}', 'CashySolvingController@fetchProblem');
    Route::get('/fetchSolvingDetails/{operation}', 'CashySolvingController@fetchSolvingDetails');
    Route::get('/solving/solve-addition', 'CashySolvingController@solveAddition');
    Route::get('/solving/solve-subtraction', 'CashySolvingController@solveSubtraction');
    Route::get('/solving/solve-division', 'CashySolvingController@solveDivision');
    Route::get('/solving/solve-multiplication', 'CashySolvingController@solveMultiplication');

    //ajax routes
    Route::get('/getAnalytics', 'CashySolvingController@getAnalytics');
    Route::get('/getTotalSolvingHistory', 'CashySolvingController@getTotalSolvingHistory');


    //cms routes
    Route::get('cms/homeCMS', 'CMSController@homeCMS');
    Route::post('homeCMSPost', 'CMSController@homeCMSPost');
    Route::get('cms/aboutCMS', 'CMSController@aboutCMS');
    Route::post('aboutCMSPost', 'CMSController@aboutCMSPost');
    Route::get('cms/featuresCMS', 'CMSController@featuresCMS');
    Route::post('featuresCMSPost', 'CMSController@featuresCMSPost');
    Route::get('cms/faqsCMS', 'CMSController@faqsCMS');
    Route::post('faqsCMSPost', 'CMSController@faqsCMSPost');
    Route::resource('about-items', 'AboutItemsController');
    Route::resource('feature-list', 'CMSFeatureListController');
    Route::resource('cms-faqs', 'CMSFaqsController');

    //resource controller
    Route::resource('dashboard', 'DashboardController');
    Route::resource('load-transactions', 'LoadTransactionsController');
    Route::resource('cashySolving', 'CashySolvingController');

    //Members Controller
    Route::get('generateActivation', 'ActivationCodeController@generateCode');
    Route::get('fetchActivationCodes', 'ActivationCodeController@getActivationCode');
    Route::patch('change-profile/{id}', 'MembersController@changeProfilePicture');
    Route::patch('activate-member/{id}', 'MembersController@activateMember');
    Route::resource('referral', 'ReferralController');

    Route::post('activate', 'MembersController@activate');
    Route::resource('activation', 'ActivationCodeController');
    Route::resource('members', 'MembersController');
    Route::resource('roles', 'RolesController');

    //Logout
    Route::get('/logout', function (){

        Auth::logout();

        return redirect('/');
    });
});

